package br.edu.fa7.gerpro7.util.conversor;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;

/**
 * Classe que realiza a conversao entre ComplexidadeUC para String e vice-versa.
 * 
 * @author Marcelo
 * 
 */
public class ComplexidadeUCConverter implements Converter {

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {
		String[] campos = arg2.split(":");
		if (campos.length == 3) {
			ComplexidadeUC complexidadeUC = new ComplexidadeUC();
			complexidadeUC.setIdComplexidadeUc(Integer.parseInt(campos[0]));
			complexidadeUC.setNome(campos[1]);
			complexidadeUC.setDescricao(campos[2]);
			return complexidadeUC;
		} else {

			return null;
		}
	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {

		if (arg2 != null) {
			ComplexidadeUC complexidadeUC = (ComplexidadeUC) arg2;
			String retorno = complexidadeUC.getIdComplexidadeUc() + ":"
					+ complexidadeUC.getNome() + ":"
					+ complexidadeUC.getDescricao();
			return retorno;
		}
		return "Selecione";

	}

}
