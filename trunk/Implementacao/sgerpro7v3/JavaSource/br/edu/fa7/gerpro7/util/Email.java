/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.fa7.gerpro7.util;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.InternetAddress;

public class Email {

    public static void sendSimpleMail(String email, String mensagem)
            throws AddressException, javax.mail.MessagingException, Exception {

        try {

            Properties props = new Properties();

            //definição do mailserver
            props.put("mail.smtp.host", "200.243.115.189");

            // seta os atributos e propriedas necessárias do objeto message para que o email seja enviado.
            Session mailSession = Session.getDefaultInstance(props, null);
            Message message = new MimeMessage(mailSession);

            // De e Para
            message.setFrom(new InternetAddress("malcantara@fa7.edu.br"));
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));


            // Assunto
            message.setSubject("Gerpro7v2");
            
            //conteúdo da mensagem 
            message.setText(mensagem + "\n\n" + "http://200.243.115.189:8080/gerpro7v2");
            
            //Envio da mensagem
            Transport.send(message);

        } catch (AddressException e) {
            throw new Exception("Endereço de e-mail do remetente é inválido.");

        } catch (javax.mail.MessagingException e) {
           throw new Exception("sistema de correio indisponível.");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}