package br.edu.fa7.gerpro7.manterstatusprojeto;


import java.util.ArrayList;

import java.util.List;

import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.StatusProjeto;
import br.edu.fa7.gerpro7.mantercasouso.EditarCasosUso;
import br.edu.fa7.gerpro7.util.FacesBean;

public class ListaStatusProjetoMB extends FacesBean{

	private List<StatusProjeto> lista;

	private StatusProjeto statusProjetoSelecionado = null;
	
	public StatusProjeto getStatusProjetoSelecionado() {
		return statusProjetoSelecionado;
	}

	public void setStatusProjetoSelecionado(StatusProjeto statusProjetoSelecionado) {
		this.statusProjetoSelecionado = statusProjetoSelecionado;
	}
	public List<StatusProjeto> getLista() {
		return lista;
	}

	public void setLista(List<StatusProjeto> lista) {
		this.lista = lista;
	}
	
	public ListaStatusProjetoMB() {

	}

	public String prepararNovo() {
		try {
            StatusProjeto statusProjetoNovo = new StatusProjeto();
            
            EditarStatusProjetoMB editarStatusProjeto = (EditarStatusProjetoMB) getBean("editarStatusProjeto");
			editarStatusProjeto.setStatusProjetoEditado(statusProjetoNovo);
            return "editarStatusProjeto";
            
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}

	}

	public String prepararEditar() {
		try {
			System.out.println(statusProjetoSelecionado.getNomeProjeto());

			EditarStatusProjetoMB editarstatusProjeto = (EditarStatusProjetoMB) getBean("editarStatusProjeto");
			
			editarstatusProjeto.setStatusProjetoEditado(statusProjetoSelecionado);
			
			return "editarStatusProjeto";
			
		} catch (Exception e) {
			return null;
		}

	}
	public String prepararExcluir() {
		try {
			System.out.println(statusProjetoSelecionado.getNomeProjeto());
			return "excluirStatusProjeto";
		} catch (Exception e) {
			return null;
		}

	}

	
}
