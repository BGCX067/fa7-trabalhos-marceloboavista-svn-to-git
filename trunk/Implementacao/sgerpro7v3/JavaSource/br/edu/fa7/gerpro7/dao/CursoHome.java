package br.edu.fa7.gerpro7.dao;

// Generated 19/04/2009 16:28:34 by Hibernate Tools 3.2.4.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;

import br.edu.fa7.gerpro7.entidades.Curso;
import br.edu.fa7.gerpro7.util.DAOFactory;

/**
 * Home object for domain model class Curso.
 * 
 * @see br.edu.fa7.gerpro7.entidades.Curso
 * @author Hibernate Tools
 */
public class CursoHome {
	
	
	private static CursoHome instance = null;
	
	private CursoHome() {
	}
	
	public static CursoHome getInstance(){
		if(instance == null){
			instance = new CursoHome();
		}
		return instance;	
	}
	

	private static final Log log = LogFactory.getLog(CursoHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return DAOFactory.getSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Curso transientInstance) {
		log.debug("persisting Curso instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			// log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Curso instance) {
		// log.debug("attaching dirty Curso instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			// log.debug("attach successful");
		} catch (RuntimeException re) {
			// log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Curso instance) {
		// log.debug("attaching clean Curso instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			// log.debug("attach successful");
		} catch (RuntimeException re) {
			// log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Curso persistentInstance) {
		// log.debug("deleting Curso instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			// log.debug("delete successful");
		} catch (RuntimeException re) {
			// log.error("delete failed", re);
			throw re;
		}
	}

	public Curso merge(Curso detachedInstance) {
		// log.debug("merging Curso instance");
		try {
			Curso result = (Curso) sessionFactory.getCurrentSession().merge(
					detachedInstance);
			// log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			// log.error("merge failed", re);
			throw re;
		}
	}

	public Curso findById(java.lang.Integer id) {
		// log.debug("getting Curso instance with id: " + id);
		try {
			Curso instance = (Curso) sessionFactory.getCurrentSession().get(
					"br.edu.fa7.gerpro7.Curso", id);
			if (instance == null) {
				// log.debug("get successful, no instance found");
			} else {
				// log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			// log.error("get failed", re);
			throw re;
		}
	}

	public List<Curso> findByExample(Curso instance) {
		// log.debug("finding Curso instance by example");
		try {
			List<Curso> results = (List<Curso>) sessionFactory
					.getCurrentSession().createCriteria(
							Curso.class.getName()).add(create(instance))
					.list();
			// log.debug("find by example successful, result size: "
			// + results.size());
			return results;
		} catch (RuntimeException re) {
			// /log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Curso> findAll() {
		 log.debug("finding Curso instance by example");
		try {
			List<Curso> results = (List<Curso>) sessionFactory
					.getCurrentSession().createCriteria(
							Curso.class.getName())
					.list();
			 log.debug("find by example successful, result size: "
			 + results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
