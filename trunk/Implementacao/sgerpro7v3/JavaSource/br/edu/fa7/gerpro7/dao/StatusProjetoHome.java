package br.edu.fa7.gerpro7.dao;

// Generated 19/04/2009 16:28:34 by Hibernate Tools 3.2.4.GA

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;

import br.edu.fa7.gerpro7.entidades.StatusProjeto;
import br.edu.fa7.gerpro7.entidades.StatusSistema;
import br.edu.fa7.gerpro7.util.DAOFactory;
import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Statusprojeto.
 * @see br.edu.fa7.gerpro7.entidades.StatusProjeto
 * @author Hibernate Tools
 */
public class StatusProjetoHome {

	public static StatusProjetoHome instance = null;
	
	private StatusProjetoHome() {
		
	}
	public static StatusProjetoHome getInstance() {
		if (instance ==null)
			instance = new StatusProjetoHome();
		return instance;
	}
	private static final Log log = LogFactory.getLog(StatusProjetoHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) DAOFactory.getSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(StatusProjeto transientInstance) {
		log.debug("persisting Statusprojeto instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(StatusProjeto instance) {
		log.debug("attaching dirty Statusprojeto instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(StatusProjeto instance) {
		log.debug("attaching clean Statusprojeto instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(StatusProjeto persistentInstance) {
		log.debug("deleting Statusprojeto instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public StatusProjeto merge(StatusProjeto detachedInstance) {
		log.debug("merging Statusprojeto instance");
		try {
			StatusProjeto result = (StatusProjeto) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public StatusProjeto findById(java.lang.Integer id) {
		log.debug("getting Statusprojeto instance with id: " + id);
		try {
			StatusProjeto instance = (StatusProjeto) sessionFactory
					.getCurrentSession().get(
							"br.edu.fa7.gerpro7.Statusprojeto", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}
	
	public List<StatusProjeto> findByExample(StatusProjeto instance) {
		log.debug("finding Statussistema instance by example");
		try {
			List<StatusProjeto> results = (List<StatusProjeto>) sessionFactory
					.getCurrentSession().createCriteria(
							StatusSistema.class.getName()).add(
							create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<StatusProjeto> findAll() {
		log.debug("finding all StatusProjeto");
		try {
			List<StatusProjeto> results = (List<StatusProjeto>) sessionFactory
					.getCurrentSession().createQuery("from StatusProjeto").list();
			
			log.debug("find all successful, result size: "
					+ results.size());
			
			return results;
			
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
