package br.edu.fa7.gerpro7.util;

import java.util.List;

import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.entidades.Curso;
import br.edu.fa7.gerpro7.entidades.Funcionalidades;
import br.edu.fa7.gerpro7.entidades.Perfil;
import br.edu.fa7.gerpro7.entidades.StatusSistema;
import br.edu.fa7.gerpro7.entidades.StatusProjeto;
import br.edu.fa7.gerpro7.mantercasouso.ListaCasosUsoMB;
import br.edu.fa7.gerpro7.mantercasouso.ManterCasoUsoControlador;
import br.edu.fa7.gerpro7.mantercomplexidadeuc.ListarComplexidadeUCMB;
import br.edu.fa7.gerpro7.mantercomplexidadeuc.ManterComplexidadeUCControlador;
import br.edu.fa7.gerpro7.mantercursos.ListarCursosMB;
import br.edu.fa7.gerpro7.mantercursos.ManterCursosControlador;
import br.edu.fa7.gerpro7.manterfuncionalidades.ListaFuncionalidadeMB;
import br.edu.fa7.gerpro7.manterfuncionalidades.ManterFuncionalidadesControlador;
import br.edu.fa7.gerpro7.manterperfil.ListarPerfisMB;
import br.edu.fa7.gerpro7.manterperfil.ManterPerfilControlador;
import br.edu.fa7.gerpro7.manterstatussistema.ListaStatusSistemaMB;
import br.edu.fa7.gerpro7.manterstatussistema.ManterStatusSistemaControlador;
import br.edu.fa7.gerpro7.manterstatusprojeto.ListaStatusProjetoMB;
import br.edu.fa7.gerpro7.manterstatusprojeto.ManterStatusProjetoControlador;

public class Principal extends FacesBean {

	/**
	 * Metodo que inicia o caso de uso Manter Cursos
	 * 
	 * @return
	 */
	public String inciarCursos() {
		try {
			
			ManterCursosControlador controlador = ManterCursosControlador.getInstance();
			List<Curso> lista = controlador.obterTodosCursos();
			ListarCursosMB listarCursos  = (ListarCursosMB) getBean("listarCursosMB");
			listarCursos.setLista(lista);
	        	
			return "listaCursos";
			
			
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}

	/**
	 * Metodo para iniciar Manter Perfis
	 * @return
	 */
	public String inciarManterPerfil(){
		try {
			ManterPerfilControlador controler = ManterPerfilControlador.getInstance();
			List<Perfil> lista = controler.obterTodosPerfis();
			ListarPerfisMB listaPerfil = (ListarPerfisMB) getBean("listarPerfisMB");
			listaPerfil.setLista(lista);
			return "listarPerfis";
		} catch (Exception e) {
			return null;
		}
	}

	/**
	 * Metodo que inicia o caso de uso Manter Caso de Uso
	 * 
	 * @return
	 */
	public String iniciarManterCasoUso() {
		try {
			ManterCasoUsoControlador controlador = ManterCasoUsoControlador
					.getInstance();

			List<CasoUso> lista = controlador.obterTodosCasosUso();

			ListaCasosUsoMB casosUsoMB = (ListaCasosUsoMB) getBean("listaCasosUsoMB");
			casosUsoMB.setLista(lista);

			return "listaCasosUso";

		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}

	public String iniciarManterFuncionalidades() {
		try {
			ManterFuncionalidadesControlador controlador = ManterFuncionalidadesControlador.getInstance();
			List<Funcionalidades> lista = controlador.obterFuncionalidades();
			ListaFuncionalidadeMB funcionalidadeMB = (ListaFuncionalidadeMB) getBean("listaFuncionalidadeMB");
			funcionalidadeMB.setLista(lista);
			
			return "listaFuncionalidade";

		} catch (Exception e) {
			return null;
		}
	}

	
	
	/*
	 * M�todo que inicia o caso de uso Manter Complexidade de Caso de Uso
	 */
	public String iniciarMarterComplexidadeUC() {
		try {
			ManterComplexidadeUCControlador controlador = ManterComplexidadeUCControlador
					.getInstance();
			List<ComplexidadeUC> lista = controlador
					.obterTodasComplexidadesUC();
			ListarComplexidadeUCMB complexidadeUCMB = (ListarComplexidadeUCMB) getBean("listaComplexidadeUCMB");
			complexidadeUCMB.setLista(lista);

			return "listarComplexidadeUC";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}

	}

	
	public String iniciarManterStatusSistema(){
		try {
			ManterStatusSistemaControlador controlador = ManterStatusSistemaControlador.getInstance();
			
			List<StatusSistema> lista = controlador.obterTodosStatusSistema();
			
			ListaStatusSistemaMB statusSistemaMB = (ListaStatusSistemaMB) 
			                                        getBean("listaStatusSistema"); 
			statusSistemaMB.setLista(lista);
			
			return "listarStatusSistema";
			
			
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}

	public String iniciarManterStatusProjeto(){
		try {
			ManterStatusProjetoControlador controlador = ManterStatusProjetoControlador.getInstance();
			
			List<StatusProjeto> lista = controlador.obterTodosStatusProjeto();
			
			ListaStatusProjetoMB statusProjetoMB = (ListaStatusProjetoMB) 
			                                        getBean("listaStatusProjeto"); 
			statusProjetoMB.setLista(lista);
			
			return "listarStatusProjeto";
			
			
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}

}
