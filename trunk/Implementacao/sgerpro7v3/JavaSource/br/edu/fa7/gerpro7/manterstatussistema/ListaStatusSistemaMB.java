package br.edu.fa7.gerpro7.manterstatussistema;

import java.util.ArrayList;

import java.util.List;

import br.edu.fa7.gerpro7.entidades.StatusSistema;

import br.edu.fa7.gerpro7.util.FacesBean;

public class ListaStatusSistemaMB extends FacesBean{

	private List<StatusSistema> lista = new ArrayList<StatusSistema>();;

	private StatusSistema statusSistemaSelecionado = null;
	
	public StatusSistema getStatusSistemaSelecionado() {
		return statusSistemaSelecionado;
	}

	public void setStatusSistemaSelecionado(StatusSistema statusSistemaSelecionado) {
		this.statusSistemaSelecionado = statusSistemaSelecionado;
	}
	public List<StatusSistema> getLista() {
		return lista;
	}

	public void setLista(List<StatusSistema> lista) {
		this.lista = lista;
	}
	
	public ListaStatusSistemaMB() {

	}
	
	public String prepararNovo() {
		try {
            StatusSistema statusSistemaNovo = new StatusSistema();
            
            EditarStatusSistemaMB editarStatusSistema = (EditarStatusSistemaMB) getBean("editarStatusSistema");
			editarStatusSistema.setStatusSistemaEditado(statusSistemaNovo);
            return "editarStatusSistema";
            
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}

	}

	public String prepararEditar() {
		try {
			System.out.println(statusSistemaSelecionado.getNomeSistema());

			EditarStatusSistemaMB editarstatusSistema = (EditarStatusSistemaMB) getBean("editarStatusSistema");
			
			editarstatusSistema.setStatusSistemaEditado(statusSistemaSelecionado);
			
			return "editarStatusSistema";
			
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}
	public String prepararExcluir() {
		try {
			
//			System.out.println(statusSistemaSelecionado.getNomeSistema());
			
			ExcluirStatusSistemaMB excluirStatusSistema = (ExcluirStatusSistemaMB) getBean("excluirStatusSistema");
			
			excluirStatusSistema.setStatusSistemaEditado(statusSistemaSelecionado); 
			
			return "excluirStatusSistema";
			
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}

	}

	
}
