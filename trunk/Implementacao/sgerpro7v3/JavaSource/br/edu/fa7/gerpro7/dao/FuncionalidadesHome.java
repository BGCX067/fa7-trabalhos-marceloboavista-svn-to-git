package br.edu.fa7.gerpro7.dao;

// Generated 19/04/2009 16:28:34 by Hibernate Tools 3.2.4.GA

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;

import br.edu.fa7.gerpro7.entidades.Funcionalidades;
import br.edu.fa7.gerpro7.manterfuncionalidades.ManterFuncionalidadesControlador;
import br.edu.fa7.gerpro7.util.DAOFactory;
import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Funcionalidades.
 * @see br.edu.fa7.gerpro7.entidades.Funcionalidades
 * @author Hibernate Tools
 */
public class FuncionalidadesHome {

	private static final Log log = LogFactory.getLog(FuncionalidadesHome.class);
 
    private static FuncionalidadesHome instance = null;
		
	private FuncionalidadesHome() {
		
	}

	public static FuncionalidadesHome getInstance() {
		if (instance == null) {
			instance = new FuncionalidadesHome();
		}
		return instance;
	}


	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) DAOFactory.getSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(Funcionalidades transientInstance) {
		log.debug("persisting Funcionalidades instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(Funcionalidades instance) {
		log.debug("attaching dirty Funcionalidades instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(Funcionalidades instance) {
		log.debug("attaching clean Funcionalidades instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(Funcionalidades persistentInstance) {
		log.debug("deleting Funcionalidades instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public Funcionalidades merge(Funcionalidades detachedInstance) {
		log.debug("merging Funcionalidades instance");
		try {
			Funcionalidades result = (Funcionalidades) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public Funcionalidades findById(java.lang.Integer id) {
		log.debug("getting Funcionalidades instance with id: " + id);
		try {
			Funcionalidades instance = (Funcionalidades) sessionFactory
					.getCurrentSession().get(
							"br.edu.fa7.gerpro7.Funcionalidades", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<Funcionalidades> findByExample(Funcionalidades instance) {
		log.debug("finding Funcionalidades instance by example");
		try {
			List<Funcionalidades> results = (List<Funcionalidades>) sessionFactory
					.getCurrentSession().createCriteria(
							Funcionalidades.class.getName()).add(
							create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<Funcionalidades> findAll() {
		log.debug("finding Funcionalidades instance by example");
		try {
			List<Funcionalidades> results = (List<Funcionalidades>) sessionFactory
					.getCurrentSession().createCriteria(
							Funcionalidades.class.getName()).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
