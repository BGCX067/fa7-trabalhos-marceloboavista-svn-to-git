package br.edu.fa7.gerpro7.dao;

// Generated 19/04/2009 16:28:34 by Hibernate Tools 3.2.4.GA

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;

import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.util.DAOFactory;
import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Complexidadeuc.
 * 
 * @see br.edu.fa7.gerpro7.entidades.ComplexidadeUC
 * @author Hibernate Tools
 */
public class ComplexidadeUCHome {

	private static ComplexidadeUCHome instance = null;

	private ComplexidadeUCHome() {

	}

	public static ComplexidadeUCHome getInstance() {
		if (instance == null)
			instance = new ComplexidadeUCHome();
		return instance;
	}

	private static final Log log = LogFactory.getLog(ComplexidadeUCHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) DAOFactory.getSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(ComplexidadeUC transientInstance) {
		log.debug("persisting Complexidadeuc instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(ComplexidadeUC instance) {
		log.debug("attaching dirty Complexidadeuc instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(ComplexidadeUC instance) {
		log.debug("attaching clean Complexidadeuc instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(ComplexidadeUC persistentInstance) {
		log.debug("deleting Complexidadeuc instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public ComplexidadeUC merge(ComplexidadeUC detachedInstance) {
		log.debug("merging Complexidadeuc instance");
		try {
			ComplexidadeUC result = (ComplexidadeUC) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public ComplexidadeUC findById(java.lang.Integer id) {
		log.debug("getting Complexidadeuc instance with id: " + id);
		try {
			ComplexidadeUC instance = (ComplexidadeUC) sessionFactory
					.getCurrentSession().get(
							"br.edu.fa7.gerpro7.Complexidadeuc", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<ComplexidadeUC> findByExample(ComplexidadeUC instance) {
		log.debug("finding Complexidadeuc instance by example");
		try {
			List<ComplexidadeUC> results = (List<ComplexidadeUC>) sessionFactory
					.getCurrentSession().createCriteria(
							ComplexidadeUC.class.getName()).add(
							create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<ComplexidadeUC> findAll() {
		log.debug("finding all complexidade de caso de uso");
		try {
			List<ComplexidadeUC> results = (List<ComplexidadeUC>) sessionFactory
					.getCurrentSession().createQuery("from ComplexidadeUC")
					.list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
