package br.edu.fa7.gerpro7.manterstatussistema;


import java.util.List;

import br.edu.fa7.gerpro7.entidades.StatusSistema;

import br.edu.fa7.gerpro7.util.FacesBean;

public class EditarStatusSistemaMB extends FacesBean {
	
	private StatusSistema statusSistemaEditado = new StatusSistema();

	
	/**
	 * @return the statusSistemaEditado
	 */
	public StatusSistema getStatusSistemaEditado() {
		return statusSistemaEditado;
	}

	/**
	 * @param statusSistemaEditado the statusSistemaEditado to set
	 */
	public void setStatusSistemaEditado(StatusSistema statusSistemaEditado) {
		this.statusSistemaEditado = statusSistemaEditado;
	}

	public EditarStatusSistemaMB() {
 		
//		ComplexidadeUC complexidadeUC = new ComplexidadeUC();
//		complexidadeUC.setIdComplexidadeUc(1);
//		complexidadeUC.setNome("Simples");
//
//		statusSistemaEditado.setComplexidadeUC(ComplexidadeUC);  
	}
	
	public String salvar(){
		try {
			ManterStatusSistemaControlador statusSistemaControlador = ManterStatusSistemaControlador
			     .getInstance();
			statusSistemaControlador.salvar(statusSistemaEditado);
			info("Status Sistema Salvo Com Sucesso");
			List<StatusSistema> lista = ManterStatusSistemaControlador.getInstance().obterTodosStatusSistema();
			ListaStatusSistemaMB statusSistemaMB = (ListaStatusSistemaMB) getBean("listaStatusSistema");
			statusSistemaMB.setLista(lista);
			return "listarStatusSistema";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		} 
	}

	public String cancelar(){
		try {
			return "listarStatusSistema";
		} catch (Exception e) {
			return null;
		}
	}
}
