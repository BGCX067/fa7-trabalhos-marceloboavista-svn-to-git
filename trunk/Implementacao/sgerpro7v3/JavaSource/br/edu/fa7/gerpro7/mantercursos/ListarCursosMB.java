package br.edu.fa7.gerpro7.mantercursos;


import java.util.ArrayList;
import java.util.List;

import br.edu.fa7.gerpro7.entidades.Curso;
import br.edu.fa7.gerpro7.util.FacesBean;


public class ListarCursosMB extends FacesBean{
	//Lista de cursos.
	private List<Curso> lista = new ArrayList<Curso>();
	//Curso selecionado na grid.
	private Curso cursoSelecionado = null;

    //get, set e construtore.
	public List<Curso> getLista() {
		return lista;
	}

	public void setLista(List<Curso> lista) {
		this.lista = lista;
	}
	
	public Curso getCursoSelecionado() {
		return cursoSelecionado;
	}

	public void setCursoSelecionado(Curso cursoSelecionado) {
		this.cursoSelecionado = cursoSelecionado;
	}

	public ListarCursosMB(){
		Curso curso = new Curso();
		curso.setIdCurso(1);
		curso.setNome("Curso 1");
		lista.add(curso);
		
		Curso curso2 = new Curso();
		curso2.setIdCurso(2);
		curso2.setNome("Curso 2");
		lista.add(curso2);
			
	}
	
	
	public String prepararNovo(){
		try {	
			
		Curso cursoNovo = new Curso();
		EditarCursosMB editarCursosMB = (EditarCursosMB) getBean("editarCursosMB");
		editarCursosMB.setCursoEditado(cursoNovo);
		
			return "editarCursos";
		} catch (Exception e) {
			error(e.getMessage());
			return null;		
		}
			
	}
	
	public String prepararEditar(){
		try {
			System.out.println(cursoSelecionado.getNome());
			EditarCursosMB editarCursosMB = (EditarCursosMB) getBean("editarCursosMB");
			editarCursosMB.setCursoEditado(cursoSelecionado);
			return "editarCursos";
		} catch (Exception e) {
			error(e.getMessage());
			return null;		
		}
			
	}
	
	public String prepararExcluir(){
		try {
			System.out.println(cursoSelecionado.getNome());
			ExcluirCursosMB excluirCursosMB = (ExcluirCursosMB) getBean("excluirCursosMB");
			excluirCursosMB.setCursoExcluido(cursoSelecionado);
			
			return "excluirCursos";
		} catch (Exception e) {
			error(e.getMessage());
			return null;		
		}
			
	}
	
	
	
	
	

}
