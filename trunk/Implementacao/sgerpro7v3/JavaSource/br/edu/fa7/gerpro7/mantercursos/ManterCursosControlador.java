package br.edu.fa7.gerpro7.mantercursos;

import java.util.List;

import br.edu.fa7.gerpro7.dao.CursoHome;
import br.edu.fa7.gerpro7.entidades.Curso;
import br.edu.fa7.gerpro7.util.DAOFactory;

public class ManterCursosControlador {

	private static ManterCursosControlador instance = null;

	private ManterCursosControlador() {
	}

	public static ManterCursosControlador getInstance() {
		if (instance == null) {
			instance = new ManterCursosControlador();
		}
		return instance;
	}

	public List<Curso> obterTodosCursos() {
		// TODO Auto-generated method stub
		CursoHome dao = CursoHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		List<Curso> lista = dao.findAll();
		DAOFactory.getSession().getTransaction().commit();
		return lista;
	}

	public void salvar(Curso cursoEditado) throws Exception {
		CursoHome dao = CursoHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		validaUnicidadeNome(cursoEditado);
		if (cursoEditado.getIdCurso() == null) {
			dao.persist(cursoEditado);
		} else {
			dao.merge(cursoEditado);
		}
		DAOFactory.getSession().getTransaction().commit();

		// TODO Auto-generated method stub

	}

	private void validaUnicidadeNome(Curso cursoEditado) throws Exception {
		// TODO Auto-generated method stub
		CursoHome dao = CursoHome.getInstance();
		Curso curso = new Curso();
		curso.setNome(cursoEditado.getNome());
		List<Curso> listacurso = dao.findByExample(curso);

		for (Curso curso2 : listacurso) {
			if (!curso2.getIdCurso().equals(cursoEditado.getIdCurso()))
				throw new Exception("Nome do curso j� existe cadastrado");
		}

	}

	public void excluir(Curso cursoExcluido) throws Exception {

		try {
			CursoHome dao = CursoHome.getInstance();
			DAOFactory.getSession().getTransaction().begin();
			dao.delete(cursoExcluido);
			DAOFactory.getSession().getTransaction().commit();
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			DAOFactory.getSession().getTransaction().rollback();
			throw new Exception("Entidade em uso, n�o pode ser removida: "
					+ e.getMessage());
		}

	}

}
