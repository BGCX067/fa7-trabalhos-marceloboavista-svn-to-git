package br.edu.fa7.gerpro7.mantercomplexidadeuc;

import java.util.List;
import java.util.ArrayList;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.mantercasouso.EditarCasosUso;
import br.edu.fa7.gerpro7.mantercasouso.ExcluirCasosUso;
import br.edu.fa7.gerpro7.mantercursos.ExcluirCursosMB;
import br.edu.fa7.gerpro7.util.FacesBean;

public class ListarComplexidadeUCMB extends FacesBean {
	public List<ComplexidadeUC> getLista() {
		return lista;
	}

	public void setLista(List<ComplexidadeUC> lista) {
		this.lista = lista;
	}

	private List<ComplexidadeUC> lista = new ArrayList<ComplexidadeUC>();

	/**
	 * Representa a linha da grid que ser� manipulada
	 */
	
	private ComplexidadeUC complexidadeUCSelecionado = null;
	
	public ComplexidadeUC getComplexidadeUCSelecionado() {
		return complexidadeUCSelecionado;
	}

	public void setComplexidadeUCSelecionado(
			ComplexidadeUC complexidadeUCSelecionado) {
		this.complexidadeUCSelecionado = complexidadeUCSelecionado;
	}

	public ListarComplexidadeUCMB() {
		//ComplexidadeUC complexidadeUC = new ComplexidadeUC();

		//complexidadeUC.setIdComplexidadeUc(1);
		//complexidadeUC.setNome("Simples");

		//lista.add(complexidadeUC);

		//ComplexidadeUC complexidadeUC2 = new ComplexidadeUC();

		//complexidadeUC2.setIdComplexidadeUc(2);
		//complexidadeUC2.setNome("Complexo");

		//lista.add(complexidadeUC2);
	}

	public String prepareNew() {
		try {
			ComplexidadeUC complexidadeNovo = new ComplexidadeUC();
			EditarComplexidadeUCMB editarComplexidadeUC =(EditarComplexidadeUCMB) getBean("editarComplexidadeUCMB");
			editarComplexidadeUC.setComplexidadeUCEdit(complexidadeNovo);
			
			return "editarComplexidadeUC";
		} catch (Exception e) {
			return null;
		}
	}

	public String prepareEdit() {
		try {
			//System.out.println(complexidadeUCSelecionado.getNome());
			EditarComplexidadeUCMB editarComplexidadeUCMB = (EditarComplexidadeUCMB) getBean("editarComplexidadeUC");
			editarComplexidadeUCMB.setComplexidadeUCEdit(complexidadeUCSelecionado);

			return "editarComplexidadeUC";
		} catch (Exception e) {
			return null;
		}
	}

	public String prepararExcluir() {
		try {
			// System.out.println(casoUsoSelecionado.getNome());

			ExcluirComplexidadeUCMB excluirComplexidadeUCMB = (ExcluirComplexidadeUCMB) getBean("excluirComplexidadeUCMB");
			excluirComplexidadeUCMB.setComplexidadeUCEditado(complexidadeUCSelecionado);

			return "excluirComplexidadeUC";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}

	}
}
