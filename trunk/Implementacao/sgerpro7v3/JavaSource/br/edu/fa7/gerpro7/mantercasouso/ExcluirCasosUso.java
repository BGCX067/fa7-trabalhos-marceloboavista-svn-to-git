package br.edu.fa7.gerpro7.mantercasouso;

import java.util.List;

import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.util.FacesBean;

public class ExcluirCasosUso extends FacesBean {

	private CasoUso casoUsoEditado = new CasoUso();

	public CasoUso getCasoUsoEditado() {
		return casoUsoEditado;
	}

	public void setCasoUsoEditado(CasoUso casoUsoEditado) {
		this.casoUsoEditado = casoUsoEditado;
	}

	/**
	 * M�todo que exclui um caso de uso
	 * @return
	 */
	public String excluir() {
		try {
			ManterCasoUsoControlador casoUsoControlador = ManterCasoUsoControlador.getInstance();
			casoUsoControlador.excluir(casoUsoEditado);
			info("Caso de uso excluido com sucesso");
			//Atualiza a listagem de resultados
			List<CasoUso> lista = ManterCasoUsoControlador.getInstance()
					.obterTodosCasosUso();

			ListaCasosUsoMB casosUsoMB = (ListaCasosUsoMB) getBean("listaCasosUsoMB");
			casosUsoMB.setLista(lista);
			return "listaCasosUso";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}
}
