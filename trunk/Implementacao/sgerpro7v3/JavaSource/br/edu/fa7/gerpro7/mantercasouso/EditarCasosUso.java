package br.edu.fa7.gerpro7.mantercasouso;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;

import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.util.FacesBean;

public class EditarCasosUso extends FacesBean {

	/**
	 * Propriedade que representa o caso uso que esta sendo editado
	 */
	private CasoUso casoUsoEditado = new CasoUso();

	private List<SelectItem> listaComplexidadeUC = new ArrayList<SelectItem>();

	public List<SelectItem> getListaComplexidadeUC() {
		return listaComplexidadeUC;
	}

	public void setListaComplexidadeUC(List<SelectItem> listaComplexidadeUC) {
		this.listaComplexidadeUC = listaComplexidadeUC;
	}

	/**
	 * Obtem o objeto CasoUso a ser editado
	 * 
	 * @return
	 */
	public CasoUso getCasoUsoEditado() {
		return casoUsoEditado;
	}

	/**
	 * Seta o atributo
	 * 
	 * @param casoUsoEditado
	 */
	public void setCasoUsoEditado(CasoUso casoUsoEditado) {
		this.casoUsoEditado = casoUsoEditado;
	}

	public EditarCasosUso() {
		// ComplexidadeUC complexidadeUC = new ComplexidadeUC();
		// complexidadeUC.setIdComplexidadeUc(1);
		// complexidadeUC.setNome("Simples");
		//
		// casoUsoEditado.setComplexidadeuc(complexidadeUC);

	}

	/***
	 * M�todo utilizado para salvar um caso de uso
	 * 
	 * @return
	 */
	public String salvar() {
		try {
			ManterCasoUsoControlador casoUsoControlador = ManterCasoUsoControlador
					.getInstance();
			casoUsoControlador.salvar(casoUsoEditado);
			info("Caso de Uso salvo com sucesso");
			//Atualiza a listagem de resultados
			List<CasoUso> lista = ManterCasoUsoControlador.getInstance()
					.obterTodosCasosUso();

			ListaCasosUsoMB casosUsoMB = (ListaCasosUsoMB) getBean("listaCasosUsoMB");
			casosUsoMB.setLista(lista);
			//Redireciona para a tela de listagem
			return "listaCasosUso";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}

	/**
	 * M�todo utilizado para cancelar a edi��o
	 * 
	 * @return
	 */
	public String cancelar() {
		try {
			return "listaCasosUso";
		} catch (Exception e) {
			return null;
		}
	}

	public void prepararListaComplexidade(List<ComplexidadeUC> lista) {

		listaComplexidadeUC = new ArrayList<SelectItem>();
		listaComplexidadeUC.clear();

		listaComplexidadeUC.add(new SelectItem(null, "Selecione"));

		for (ComplexidadeUC complexidadeUC : lista) {
			listaComplexidadeUC.add(new SelectItem(complexidadeUC,
					complexidadeUC.getNome()));
		}

	}

}
