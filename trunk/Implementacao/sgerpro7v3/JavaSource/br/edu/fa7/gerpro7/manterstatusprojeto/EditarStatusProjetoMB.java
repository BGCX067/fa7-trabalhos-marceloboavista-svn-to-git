package br.edu.fa7.gerpro7.manterstatusprojeto;

import java.util.ArrayList;
import java.util.List;

import br.edu.fa7.gerpro7.entidades.StatusProjeto;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.util.FacesBean;

public class EditarStatusProjetoMB extends FacesBean {
	
	private StatusProjeto statusProjetoEditado = new StatusProjeto();

	
	/**
	 * @return the statusProjetoEditado
	 */
	public StatusProjeto getStatusProjetoEditado() {
		return statusProjetoEditado;
	}

	/**
	 * @param statusProjetoEditado the statusProjeto to set
	 */
	public void setStatusProjetoEditado(StatusProjeto statusProjetoEditado) {
		this.statusProjetoEditado = statusProjetoEditado;
	}

	public EditarStatusProjetoMB() {
 		
//		ComplexidadeUC complexidadeUC = new ComplexidadeUC();
//		complexidadeUC.setIdComplexidadeUc(1);
//		complexidadeUC.setNome("Simples");
//
//		statusProjetoEditado.setComplexidadeUC(ComplexidadeUC);  
	}
	
	public String salvar(){
		try {
			ManterStatusProjetoControlador statusProjetoControlador = ManterStatusProjetoControlador
			     .getInstance();
			statusProjetoControlador.salvar(statusProjetoEditado);
			info("Status do Projeto Salvo Com Sucesso");
			List<StatusProjeto> lista = ManterStatusProjetoControlador.getInstance().obterTodosStatusProjeto();
			ListaStatusProjetoMB statusProjetoMB = (ListaStatusProjetoMB) getBean("listaStatusProjeto");
			statusProjetoMB.setLista(lista);
			return "listaStatusProjeto";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		} 
	}

	public String cancelar(){
		try {
			return "listastatusProjeto";
		} catch (Exception e) {
			return null;
		}
	}
}
