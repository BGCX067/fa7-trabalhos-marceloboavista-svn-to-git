package br.edu.fa7.gerpro7.entidades;

// Generated 19/04/2009 16:28:33 by Hibernate Tools 3.2.4.GA

import java.util.HashSet;
import java.util.Set;


/**
 * Perfil generated by hbm2java
 */
public class Perfil implements java.io.Serializable {

	private Integer idPerfil;
	private String nome;
	private String descricao;
	private Set<Usuario> usuarios = new HashSet<Usuario>(0);
	private Set<Funcionalidades> funcionalidadeses = new HashSet<Funcionalidades>(0);

	public Perfil() {
	}

	public Perfil(String nome, Set<Usuario> usuarios,
			Set<Funcionalidades> funcionalidadeses) {
		this.nome = nome;
		this.usuarios = usuarios;
		this.funcionalidadeses = funcionalidadeses;
	}

	public Integer getIdPerfil() {
		return this.idPerfil;
	}

	public void setIdPerfil(Integer idPerfil) {
		this.idPerfil = idPerfil;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Set<Usuario> getUsuarios() {
		return this.usuarios;
	}

	public void setUsuarios(Set<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Set<Funcionalidades> getFuncionalidadeses() {
		return this.funcionalidadeses;
	}

	public void setFuncionalidadeses(Set<Funcionalidades> funcionalidadeses) {
		this.funcionalidadeses = funcionalidadeses;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	
	
}
