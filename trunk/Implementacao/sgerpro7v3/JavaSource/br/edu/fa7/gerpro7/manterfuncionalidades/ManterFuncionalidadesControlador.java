package br.edu.fa7.gerpro7.manterfuncionalidades;

import java.util.List;

import br.edu.fa7.gerpro7.dao.FuncionalidadesHome;
import br.edu.fa7.gerpro7.entidades.Funcionalidades;
import br.edu.fa7.gerpro7.util.DAOFactory;


public class ManterFuncionalidadesControlador {
	private static ManterFuncionalidadesControlador instance = null;
	
	
	private ManterFuncionalidadesControlador() {
	}

	public static ManterFuncionalidadesControlador getInstance() {
		if (instance == null) {
			instance = new ManterFuncionalidadesControlador();
		}
		return instance;
	}

	public List<Funcionalidades> obterFuncionalidades() {
		DAOFactory.getSession().getTransaction().begin();
		List<Funcionalidades> lista = FuncionalidadesHome.getInstance().findAll();
		DAOFactory.getSession().getTransaction().commit();
		return lista;
		
	}

	/**
	 * Salvar Funcionalidade pode ser no Novo ou em Altera��o.
	 * @param funcionalidadesEditado
	 * @throws Exception 
	 */
	public void salvar(Funcionalidades funcionalidadesEditado) throws Exception {
		FuncionalidadesHome dao = FuncionalidadesHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		validaUnicidadeNome(funcionalidadesEditado);
		if (funcionalidadesEditado.getIdFuncionalidades()== null)
			dao.persist(funcionalidadesEditado);
		else dao.merge(funcionalidadesEditado);
		DAOFactory.getSession().getTransaction().commit();
	}

	private void validaUnicidadeNome(Funcionalidades funcionalidadesEditado) throws Exception {
		FuncionalidadesHome dao = FuncionalidadesHome.getInstance();
		Funcionalidades funcionalidades = new Funcionalidades();
		funcionalidades.setNomeFuncionalidade(funcionalidadesEditado.getNomeFuncionalidade());
		List<Funcionalidades> listaFuncionalidades = dao.findByExample(funcionalidades);
		
		for (Funcionalidades funcionalidades2 : listaFuncionalidades){
			if(!funcionalidades2.getIdFuncionalidades().equals(funcionalidadesEditado.getIdFuncionalidades()))
			throw new Exception("Nome da Funcionalidade j� existe");
		}
	
	}

}
