package br.edu.fa7.gerpro7.manterfuncionalidades;

import java.util.ArrayList;
import java.util.List;

import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.entidades.Funcionalidades;
import br.edu.fa7.gerpro7.mantercasouso.EditarCasosUso;
import br.edu.fa7.gerpro7.mantercomplexidadeuc.EditarComplexidadeUCMB;
import br.edu.fa7.gerpro7.util.FacesBean;

public class ListaFuncionalidadeMB extends FacesBean {
  /*
   *Lista Funcionalidades a serem exibidas na Grid
   */
   
	private List<Funcionalidades> lista = new ArrayList<Funcionalidades>();

	/* 
	 * Funcionalidade Selecionada na Grid
	 */
	private Funcionalidades funcionalidadesSelecionada = null;
	
	
	public Funcionalidades getFuncionalidadesSelecionada() {
		return funcionalidadesSelecionada;
	}

	public void setFuncionalidadesSelecionada(
			Funcionalidades funcionalidadesSelecionada) {
		this.funcionalidadesSelecionada = funcionalidadesSelecionada;
	}

	public List<Funcionalidades> getLista() {
		return lista;
	}

	public void setLista(List<Funcionalidades> lista) {
		this.lista = lista;
	}
	
   public ListaFuncionalidadeMB(){
	   Funcionalidades funcionalidades = new Funcionalidades();
	   funcionalidades.setIdFuncionalidades(1);
	   funcionalidades.setNomeFuncionalidade("Funcionalidade1");
	   lista.add(funcionalidades);
	   Funcionalidades funcionalidades2 = new Funcionalidades();
	   funcionalidades2.setIdFuncionalidades(2);
	   funcionalidades2.setNomeFuncionalidade("Funcionalidade2");
	   lista.add(funcionalidades2);
   }


 
   public String prepararNovo(){
	   try {
			Funcionalidades funcionalidadeNova = new Funcionalidades();
		  	EditarFuncionalidadesMB editarFuncionalidade = (EditarFuncionalidadesMB) getBean("editarFuncionalidadesMB");
			editarFuncionalidade.setFuncionalidadesEditado(funcionalidadeNova); 
						
			return "editarFuncionalidade";
	} catch (Exception e) {
		error(e.getMessage());
		return null;
   }
   
   }
   
   public String prepararEditar(){
	   try {
		   System.out.println(funcionalidadesSelecionada.getNomeFuncionalidade());
		  
		   EditarFuncionalidadesMB editarFuncionalidadesMB = (EditarFuncionalidadesMB) getBean("editarFuncionalidadesMB");
		   editarFuncionalidadesMB.setFuncionalidadesEditado(funcionalidadesSelecionada);
		    
		   return "editarFuncionalidade";
	} catch (Exception e) {
		error(e.getMessage());
		return null;
   }
  
   }
   public String prepararExcluir(){
	   try {
		   System.out.println(funcionalidadesSelecionada.getNomeFuncionalidade());
		   return "excluirFuncionalidade";
	} catch (Exception e) {
		return null;
   }
  
   }
   
}
   

