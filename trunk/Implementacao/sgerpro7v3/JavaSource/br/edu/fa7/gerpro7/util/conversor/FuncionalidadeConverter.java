package br.edu.fa7.gerpro7.util.conversor;

import java.util.HashMap;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;

/**
 * Classe que realiza a conversao entre ComplexidadeUC para String e vice-versa.
 * 
 * @author Marcelo
 * 
 */
public class FuncionalidadeConverter implements Converter {

	public static HashMap<String, Object> mapaObjectos = new HashMap<String, Object>();

	@Override
	public Object getAsObject(FacesContext arg0, UIComponent arg1, String arg2) {

		return mapaObjectos.get(arg2);

	}

	@Override
	public String getAsString(FacesContext arg0, UIComponent arg1, Object arg2) {

		String token = arg2.hashCode() + "";
		mapaObjectos.put(token, arg2);
		return token;

	}

}
