package br.edu.fa7.gerpro7.manterstatusprojeto;

import java.util.List;

import br.edu.fa7.gerpro7.dao.StatusProjetoHome;
import br.edu.fa7.gerpro7.entidades.StatusProjeto;
import br.edu.fa7.gerpro7.util.DAOFactory;

public class ManterStatusProjetoControlador {
	
	static ManterStatusProjetoControlador instance = null; 
	
	private ManterStatusProjetoControlador() {
		}
	
	public static ManterStatusProjetoControlador getInstance(){
		if (instance == null) {
			instance = new ManterStatusProjetoControlador();
			}
		return instance;
	}
 
	public List<StatusProjeto> obterTodosStatusProjeto() {
		
		StatusProjetoHome dao = StatusProjetoHome.getInstance();
		
		DAOFactory.getSession().getTransaction().begin();
		
		List<StatusProjeto> lista = dao.findAll();
		
		DAOFactory.getSession().getTransaction().commit();
		
		return lista;
	}

	public void salvar(StatusProjeto statusProjetoEditado)throws Exception {
		StatusProjetoHome dao= StatusProjetoHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		validaUnicidadeNome(statusProjetoEditado);
		if (statusProjetoEditado.getIdStatusProjeto()== null)
			dao.persist(statusProjetoEditado);
		else
			dao.merge(statusProjetoEditado);
		DAOFactory.getSession().getTransaction().commit();
		
			
	}
	
	private void validaUnicidadeNome(StatusProjeto statusProjetoEditado) throws Exception {
		StatusProjetoHome dao = StatusProjetoHome.getInstance();
		StatusProjeto statusProjeto = new StatusProjeto();
		statusProjeto.setNomeProjeto(statusProjetoEditado.getNomeProjeto());
		List<StatusProjeto> listaStatusProjeto = dao.findByExample(statusProjeto);
		
		for (StatusProjeto statusProjeto2 : listaStatusProjeto) {
			if(!statusProjeto2.getIdStatusProjeto().equals(statusProjeto.getIdStatusProjeto()))
				throw new Exception("Nome do Status do Projeto ja cadastrado");
		}
					
	}
}
