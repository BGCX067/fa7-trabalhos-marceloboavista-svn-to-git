package br.edu.fa7.gerpro7.manterstatussistema;

import java.util.List;

import br.edu.fa7.gerpro7.entidades.StatusSistema;
import br.edu.fa7.gerpro7.manterstatussistema.ListaStatusSistemaMB;
import br.edu.fa7.gerpro7.manterstatussistema.ManterStatusSistemaControlador;
import br.edu.fa7.gerpro7.util.FacesBean;

public class ExcluirStatusSistemaMB extends FacesBean {

	private StatusSistema statusSistemaEditado = new StatusSistema();

	public StatusSistema getStatusSistemaEditado() {
		return statusSistemaEditado;
	}

	public void setStatusSistemaEditado(StatusSistema statusSistemaEditado) {
		this.statusSistemaEditado = statusSistemaEditado;
	}

	public String excluir() {
		try {
			ManterStatusSistemaControlador StatusSistemaControlador = ManterStatusSistemaControlador.getInstance();
			StatusSistemaControlador.excluir(statusSistemaEditado);
			info("Status do Sistema foi excluido com sucesso");
			
			List<StatusSistema> lista = ManterStatusSistemaControlador.getInstance()
					.obterTodosStatusSistema();

			ListaStatusSistemaMB statusSistemaMB = (ListaStatusSistemaMB) getBean("listaStatusSistemaMB");
			statusSistemaMB.setLista(lista);
			return "listaStatusSistema";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}
}