package br.edu.fa7.gerpro7.dao;

// Generated 19/04/2009 16:28:34 by Hibernate Tools 3.2.4.GA

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;

import br.edu.fa7.gerpro7.entidades.StatusSistema;
import br.edu.fa7.gerpro7.util.DAOFactory;
import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Statussistema.
 * @see br.edu.fa7.gerpro7.entidades.StatusSistema
 * @author Hibernate Tools
 */
public class StatusSistemaHome {
	
	public static StatusSistemaHome instance = null;
	
	private StatusSistemaHome() {
		
	}

	public static StatusSistemaHome getInstance() {
		if (instance ==null)
			instance = new StatusSistemaHome();
		return instance;
	}
	private static final Log log = LogFactory.getLog(StatusSistemaHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) DAOFactory.getSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(StatusSistema transientInstance) {
		log.debug("persisting Statussistema instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(StatusSistema instance) {
		log.debug("attaching dirty Statussistema instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(StatusSistema instance) {
		log.debug("attaching clean Statussistema instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(StatusSistema persistentInstance) {
		log.debug("deleting Statussistema instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public StatusSistema merge(StatusSistema detachedInstance) {
		log.debug("merging Statussistema instance");
		try {
			StatusSistema result = (StatusSistema) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public StatusSistema findById(java.lang.Integer id) {
		log.debug("getting Statussistema instance with id: " + id);
		try {
			StatusSistema instance = (StatusSistema) sessionFactory
					.getCurrentSession().get(
							StatusSistema.class.getName(), id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}


	public List<StatusSistema> findByExample(StatusSistema instance) {
		log.debug("finding Statussistema instance by example");
		try {
			List<StatusSistema> results = (List<StatusSistema>) sessionFactory
					.getCurrentSession().createCriteria(
							StatusSistema.class.getName()).add(
							create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
	
	public List<StatusSistema> findAll() {
		log.debug("finding all StatusSistema");
		try {
			List<StatusSistema> results = (List<StatusSistema>) sessionFactory
					.getCurrentSession().createQuery("from StatusSistema").list();
			
			log.debug("find all successful, result size: "
					+ results.size());
			
			return results;
			
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
