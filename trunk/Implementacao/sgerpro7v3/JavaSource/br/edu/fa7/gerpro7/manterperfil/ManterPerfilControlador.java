package br.edu.fa7.gerpro7.manterperfil;

import java.util.List;

import br.edu.fa7.gerpro7.dao.CasousoHome;
import br.edu.fa7.gerpro7.dao.FuncionalidadesHome;
import br.edu.fa7.gerpro7.dao.PerfilHome;
import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.Funcionalidades;
import br.edu.fa7.gerpro7.entidades.Perfil;
import br.edu.fa7.gerpro7.util.DAOFactory;

/**
 * 
 * @author Junior Oliveira Classe que instancia somente uma vez
 * 
 */
public class ManterPerfilControlador {

	private static ManterPerfilControlador instance = null;

	private ManterPerfilControlador() {
	}

	public static ManterPerfilControlador getInstance() {
		if (instance == null) {
			instance = new ManterPerfilControlador();
		}
		return instance;
	}

	public List<Perfil> obterTodosPerfis() {
		PerfilHome dao = PerfilHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		List<Perfil> lista = dao.findAll();
		DAOFactory.getSession().getTransaction().commit();
		return lista;
	}

	public List<Funcionalidades> obterTodasFuncionalidades() {
		FuncionalidadesHome dao = FuncionalidadesHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		List<Funcionalidades> lista = dao.findAll();
		DAOFactory.getSession().getTransaction().commit();
		return lista;
	}

	public void salvar(Perfil perfil) throws Exception {
		PerfilHome dao = PerfilHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		validaNomeUnico(perfil);
		if (perfil.getIdPerfil() == null)// Perfil Novo
			dao.persist(perfil);
		else
			dao.merge(perfil);
		DAOFactory.getSession().getTransaction().commit();
	}

	private void validaNomeUnico(Perfil perfil) throws Exception {
		PerfilHome dao = PerfilHome.getInstance();
		perfil.setNome(perfil.getNome());
		List<Perfil> lista = dao.findByExample(perfil);

		for (Perfil perfil1 : lista) {
			if (!perfil1.getIdPerfil().equals(perfil.getIdPerfil()))
				throw new Exception("Nome do perfil já existe no cadastro");
		}

	}

	public void excluir(Perfil perfil) throws Exception {
		PerfilHome dao = PerfilHome.getInstance();

		try {
			DAOFactory.getSession().getTransaction().begin();
			dao.delete(perfil);
			DAOFactory.getSession().getTransaction().commit();
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			DAOFactory.getSession().getTransaction().rollback();
			throw new Exception("Entidade não pode ser removida pois esta em uso");
		}

	}
}
