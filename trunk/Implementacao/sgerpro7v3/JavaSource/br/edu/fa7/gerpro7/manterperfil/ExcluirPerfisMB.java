package br.edu.fa7.gerpro7.manterperfil;

import java.util.ArrayList;
import java.util.List;

import br.edu.fa7.gerpro7.entidades.Funcionalidades;
import br.edu.fa7.gerpro7.entidades.Perfil;
import br.edu.fa7.gerpro7.util.FacesBean;

public class ExcluirPerfisMB extends FacesBean{
	/**
	 * Propiedade que representa Perfil sendo editado
	 */
	private Perfil perfil = new Perfil();
	
	/**
	 * Propiedade que representa as funcionalidades para o perfil
	 */
	private List<Funcionalidades> funcionalidades = new ArrayList<Funcionalidades>();
	
	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public List<Funcionalidades> getFuncionalidades() {
		return funcionalidades;
	}

	public void setFuncionalidades(List<Funcionalidades> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}
	
	/**
	 * Metodo para salvar o perfil
	 * @return
	 */
	public String salvar(){
		try {
			return "listarPerfis";
		} catch (Exception e) {
			return null;
		}
	}
	
	/**
	 * Metodo para cancelar o processo de salvar o perfil
	 * @return
	 */
	public String cancelar(){
		try {
			return "listarPerfis";
		} catch (Exception e) {
			return null;
		}
	}
	
	public String excluir(){
		try{
			ManterPerfilControlador perfilControlador = ManterPerfilControlador.getInstance();
			perfilControlador.excluir(perfil);
			info("Perfil excluido com sucesso");
			List<Perfil> lista = ManterPerfilControlador.getInstance().obterTodosPerfis();
			ListarPerfisMB perilPerfisMB = (ListarPerfisMB) getBean("listarPerfisMB");
			perilPerfisMB.setLista(lista);
			return "listarPerfis";
		}catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}

}
