package br.edu.fa7.gerpro7.manterfuncionalidades;

import java.util.List;

import br.edu.fa7.gerpro7.entidades.Funcionalidades;
import br.edu.fa7.gerpro7.util.FacesBean;

public class EditarFuncionalidadesMB extends FacesBean{
	/*
	 * Propriedade que representa a funcionalidade que esta sendo editada
	 * */
	private Funcionalidades funcionalidadesEditado = new Funcionalidades();

	public Funcionalidades getFuncionalidadesEditado() {
		return funcionalidadesEditado;
	}

	public void setFuncionalidadesEditado(Funcionalidades funcionalidadesEditado) {
		this.funcionalidadesEditado = funcionalidadesEditado;
	}
	
	/*
	 * Metodo para Salvar Funcionalidade Editada
	 * */
	public String salvar(){
		try {
			ManterFuncionalidadesControlador funcionalidadeControlador = ManterFuncionalidadesControlador.getInstance();
			funcionalidadeControlador.salvar(funcionalidadesEditado);
			info("Funcionalidade Salva com Sucesso");
			List<Funcionalidades> lista = ManterFuncionalidadesControlador.getInstance().obterFuncionalidades();
	       
			ListaFuncionalidadeMB funcionalidadeMB = (ListaFuncionalidadeMB) getBean("listaFuncionalidadeMB");
			funcionalidadeMB.setLista(lista);
			
			return "listaFuncionalidade";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}
	/*
	 * Metodo para Cancelar Funcionalidade Editada
	 * */
	public String cancelar(){
		try {
			return "listaFuncionalidade";
		} catch (Exception e) {
			return null;
		}
	}

}
