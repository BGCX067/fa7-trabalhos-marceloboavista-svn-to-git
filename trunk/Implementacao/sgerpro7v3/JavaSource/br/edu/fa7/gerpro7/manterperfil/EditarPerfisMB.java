package br.edu.fa7.gerpro7.manterperfil;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import javax.faces.model.SelectItem;

import br.edu.fa7.gerpro7.entidades.Funcionalidades;
import br.edu.fa7.gerpro7.entidades.Perfil;
import br.edu.fa7.gerpro7.util.FacesBean;

public class EditarPerfisMB extends FacesBean {
	/**
	 * Propiedade que representa Perfil sendo editado
	 */
	private Perfil perfil = new Perfil();

	/**
	 * Propiedade que representa as funcionalidades para o perfil
	 */
	private List<SelectItem> funcionalidades = new ArrayList<SelectItem>();

	private List<String> funcionalidadesSelecionadas = new ArrayList<String>();

	public List<String> getFuncionalidadesSelecionadas() {
		return funcionalidadesSelecionadas;
	}

	public void setFuncionalidadesSelecionadas(
			List<String> funcionalidadesSelecionadas) {
		this.funcionalidadesSelecionadas = funcionalidadesSelecionadas;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public List<SelectItem> getFuncionalidades() {
		return funcionalidades;
	}

	public void setFuncionalidades(List<SelectItem> funcionalidades) {
		this.funcionalidades = funcionalidades;
	}

	/**
	 * Metodo para salvar o perfil
	 * 
	 * @return
	 */
	public String salvar() {
		try {
			ManterPerfilControlador perfilControlador = ManterPerfilControlador
					.getInstance();
			prepararSalvarListaFuncionalidades();
			perfilControlador.salvar(perfil);
			info("Perfil salvo com sucesso");
			List<Perfil> lista = ManterPerfilControlador.getInstance()
					.obterTodosPerfis();
			ListarPerfisMB perfisMB = (ListarPerfisMB) getBean("listarPerfisMB");
			perfisMB.setLista(lista);
			return "listarPerfis";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}

	/**
	 * Metodo para cancelar o processo de salvar o perfil
	 * 
	 * @return
	 */
	public String cancelar() {
		try {
			return "listarPerfis";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}

	public void prepararListaPerfis(List<Funcionalidades> listaFuncionalidades) {
		funcionalidades = new ArrayList<SelectItem>();
		funcionalidades.clear();
		for (Funcionalidades funcionalidade : listaFuncionalidades) {
			funcionalidades.add(new SelectItem(funcionalidade
					.getIdFuncionalidades(), funcionalidade
					.getNomeFuncionalidade()));
		}
		funcionalidadesSelecionadas = new ArrayList<String>();
		if (perfil.getFuncionalidadeses() != null) {
			for (Funcionalidades funcionalidades : perfil
					.getFuncionalidadeses()) {
				funcionalidadesSelecionadas.add(funcionalidades
						.getIdFuncionalidades().toString());
				
			}
		}

	}

	public void prepararSalvarListaFuncionalidades() {

		List<String> listaFuncionadades = this.getFuncionalidadesSelecionadas();
		HashSet<Funcionalidades> setFuncionalidades = new HashSet<Funcionalidades>();

		for (String idFuncionalidades : listaFuncionadades) {
			Funcionalidades funcionalidades = new Funcionalidades();
			funcionalidades.setIdFuncionalidades(Integer
					.parseInt(idFuncionalidades));
			setFuncionalidades.add(funcionalidades);
		}
		perfil.setFuncionalidadeses(setFuncionalidades);

	}

}
