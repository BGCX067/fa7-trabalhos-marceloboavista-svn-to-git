package br.edu.fa7.gerpro7.mantercasouso;

import java.util.ArrayList;
import java.util.List;

import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.logaraplicacao.LogarAplicacao;
import br.edu.fa7.gerpro7.util.FacesBean;

public class ListaCasosUsoMB extends FacesBean {

	/**
	 * Lista de casos de uso a ser exibida na grid
	 */
	private List<CasoUso> lista = new ArrayList<CasoUso>();

	/**
	 * Caso de uso selecionado na grid
	 */
	private CasoUso casoUsoSelecionado = null;

	public CasoUso getCasoUsoSelecionado() {
		return casoUsoSelecionado;
	}

	public void setCasoUsoSelecionado(CasoUso casoUsoSelecionado) {
		this.casoUsoSelecionado = casoUsoSelecionado;
	}

	public List<CasoUso> getLista() {
		return lista;
	}

	public void setLista(List<CasoUso> lista) {
		this.lista = lista;
	}

	public ListaCasosUsoMB() {

		/*
		 * LogarAplicacao aplicacao = new LogarAplicacao();
		 * aplicacao.criaDadosBanco(); CasoUso casoUso = new CasoUso();
		 * ComplexidadeUC complexidadeUC = new ComplexidadeUC();
		 * 
		 * casoUso.setIdCasoUso(1); casoUso.setNome("Teste da lista");
		 * complexidadeUC.setNome("Complexidade1");
		 * casoUso.setComplexidadeuc(complexidadeUC); lista.add(casoUso);
		 * 
		 * CasoUso casoUso2 = new CasoUso(); ComplexidadeUC complexidadeUC2 =
		 * new ComplexidadeUC();
		 * 
		 * casoUso2.setIdCasoUso(2); casoUso2.setNome("Teste da lista 2");
		 * complexidadeUC2.setNome("Complexidade2");
		 * casoUso2.setComplexidadeuc(complexidadeUC2); lista.add(casoUso2);
		 */

	}

	/**
	 * Prepara a tela de edi��o de novo
	 * 
	 * @return
	 */

	public String prepararNovo() {
		try {

			CasoUso casoUsoNovo = new CasoUso();
			EditarCasosUso editarCasosUso = (EditarCasosUso) getBean("editarCasoUso");
			editarCasosUso.setCasoUsoEditado(casoUsoNovo);
			// Metodo para obter a lista de complexidade
			ManterCasoUsoControlador controlador = ManterCasoUsoControlador
					.getInstance();
			List<ComplexidadeUC> listaComplexidadeUC = controlador
					.getTodasComplexidadesUC();
			editarCasosUso.prepararListaComplexidade(listaComplexidadeUC);

			return "editarCasosUso";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}

	}

	/**
	 * Metodo que prepara a tela de edi��o de caso de uso
	 * 
	 * @return
	 */
	public String prepararEditar() {
		try {
			// System.out.println(casoUsoSelecionado.getNome());

			EditarCasosUso editarCasosUso = (EditarCasosUso) getBean("editarCasoUso");
			editarCasosUso.setCasoUsoEditado(casoUsoSelecionado);

			return "editarCasosUso";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}

	}

	/**
	 * M�todo que prepara a tela de exclus�o
	 * 
	 * @return
	 */
	public String prepararExcluir() {
		try {
			// System.out.println(casoUsoSelecionado.getNome());

			ExcluirCasosUso excluirCasosUso = (ExcluirCasosUso) getBean("excluirCasoUso");
			excluirCasosUso.setCasoUsoEditado(casoUsoSelecionado);

			return "excluirCasosUso";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}

	}

}
