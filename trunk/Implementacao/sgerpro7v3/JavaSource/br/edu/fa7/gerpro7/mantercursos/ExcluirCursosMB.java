package br.edu.fa7.gerpro7.mantercursos;

import java.util.List;

import br.edu.fa7.gerpro7.entidades.Curso;
import br.edu.fa7.gerpro7.util.FacesBean;

public class ExcluirCursosMB extends FacesBean {
	/**
	 * Propriedade que representa o Curso que est� sendo
	 */
	private Curso cursoExcluido = new Curso();

	/**
	 * @return the cursoExcluido
	 */
	public Curso getCursoExcluido() {
		return cursoExcluido;
	}

	/**
	 * @param cursoExcluido
	 *            the cursoExcluido to set
	 */
	public void setCursoExcluido(Curso cursoExcluido) {
		this.cursoExcluido = cursoExcluido;
	}



	public String excluir() {

		try {
			ManterCursosControlador cursosControlador = ManterCursosControlador
					.getInstance();
			cursosControlador.excluir(cursoExcluido);
			info("Curso excluido com sucesso.");
			List<Curso> lista = ManterCursosControlador.getInstance()
					.obterTodosCursos();
			ListarCursosMB cursosMB = (ListarCursosMB) getBean("listarCursosMB");
			cursosMB.setLista(lista);
			return "listaCursos";

		} catch (Exception e) {
			error(e.getMessage());
			return null;

		}

	}
	
	public String cancelar() {

		try {

			return "listaCursos";

		} catch (Exception e) {
			error(e.getMessage());
			return null;

		}
	}

}