/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.fa7.gerpro7.logaraplicacao;

/**
 *
 * @author Marcelo
 */
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;

import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

public class AutenticarPhaseListener implements PhaseListener {

	static private String PAGINA_LOGIN = "/faces/LogarAplicacao/LogarAplicacao.jsp";
	static private String PAGINA_PUBLICAS = "faces/SolicitarSenhaUsuario/SolicitarSenhaUsuario.jsp"
			+ "faces/SolicitarCadastroUsuario/SolicitarCadastroUsuario.jsp";

	/**
	 * Metodo chamado sempre depois da execucao de uma determinada PHASE
	 * 
	 * @param event
	 *            - {@link PhaseEvent}
	 */
	public void afterPhase(PhaseEvent event) {
		System.out.println(event.getPhaseId());
		FacesContext ctx = event.getFacesContext();
		String paginaDestino = "";
		if (ctx.getViewRoot() != null) {
			paginaDestino = ctx.getViewRoot().getViewId();
		}
		System.out.println(paginaDestino);
		if (event.getPhaseId() != null) {
			ctx = event.getFacesContext();

			Object usuarioLogado;
			HttpSession session = (HttpSession) ctx.getExternalContext()
					.getSession(true);
			if (session.getAttribute("usuarioLogado") == null
					&& !PAGINA_PUBLICAS.contains(paginaDestino)
					&& !paginaDestino
							.equalsIgnoreCase("/LogarAplicacao/LogarAplicacao.jsp")
					&& event.getPhaseId().getOrdinal() == PhaseId.RESTORE_VIEW
							.getOrdinal()) {
				try {
					System.out.println(paginaDestino);
					// ctx.getViewRoot().setViewId(PAGINA_LOGIN);
					// ctx.getExternalContext().dispatch(paginaDestino);
					ctx.getExternalContext().redirect(
							ctx.getExternalContext().getRequestContextPath()
									+ PAGINA_LOGIN);

				} catch (Exception ex) {
					Logger.getLogger(AutenticarPhaseListener.class.getName())
							.log(Level.SEVERE, null, ex);
				}
			}
		}

	}

	/**
	 ** Metodo chamado sempre antes de se executar uma determinada PHASE
	 * 
	 * @param event
	 *            - {@link PhaseEvent}
	 */
	public void beforePhase(PhaseEvent event) {
		System.out.println(event.getPhaseId());
		FacesContext ctx = event.getFacesContext();
		String paginaDestino = "";
		if (ctx.getViewRoot() != null) {
			paginaDestino = ctx.getViewRoot().getViewId();
		}
		System.out.println(paginaDestino);

	}

	/**
	 * Retorna o PhaseId da fase do ciclo de vida JSF que esta sendo
	 * interceptado e gerênciado pelo Controle de acesso.
	 * 
	 * @return - {@link PhaseId}
	 */
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}
}