package br.edu.fa7.gerpro7.mantercomplexidadeuc;

import java.util.List;

import br.edu.fa7.gerpro7.dao.CasousoHome;
import br.edu.fa7.gerpro7.dao.ComplexidadeUCHome;
import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.util.DAOFactory;

public class ManterComplexidadeUCControlador {
	
	private static ManterComplexidadeUCControlador instance = null;
	private ManterComplexidadeUCControlador(){
	
	}
	public static ManterComplexidadeUCControlador getInstance() {
		if (instance == null){
				instance = new ManterComplexidadeUCControlador();
	}
	return instance;
	}
	public List<ComplexidadeUC> obterTodasComplexidadesUC() {
		ComplexidadeUCHome dao = ComplexidadeUCHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		List<ComplexidadeUC> lista = dao.findAll();
		DAOFactory.getSession().getTransaction().commit();
		return lista;
		
		
	}
	
	public void salvar(ComplexidadeUC complexidadeUCEditado) throws Exception {
		ComplexidadeUCHome dao = ComplexidadeUCHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		validaUnicidadeNome(complexidadeUCEditado);
		if (complexidadeUCEditado.getIdComplexidadeUc() == null)// Complexidade UC novo
			dao.persist(complexidadeUCEditado);
		else
			dao.merge(complexidadeUCEditado);
		DAOFactory.getSession().getTransaction().commit();
	}
	
	private void validaUnicidadeNome(ComplexidadeUC complexidadeUCEditado) throws Exception {
		ComplexidadeUCHome dao = ComplexidadeUCHome.getInstance();
		ComplexidadeUC complexidadeUC = new ComplexidadeUC();
		complexidadeUC.setNome(complexidadeUCEditado.getNome());
		List<ComplexidadeUC> listaComplexidadeUC = dao.findByExample(complexidadeUC);

		for (ComplexidadeUC complexidadeUC2 : listaComplexidadeUC) {
			if (!complexidadeUC2.getIdComplexidadeUc().equals(complexidadeUCEditado.getIdComplexidadeUc()))
				throw new Exception("Nome da complexidade de uso j� existe cadastrado");

		}

	}

	public void excluir(ComplexidadeUC complexidadeUCEditado) {
		ComplexidadeUCHome dao = ComplexidadeUCHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		dao.delete(complexidadeUCEditado);
		DAOFactory.getSession().getTransaction().commit();

	}
}
