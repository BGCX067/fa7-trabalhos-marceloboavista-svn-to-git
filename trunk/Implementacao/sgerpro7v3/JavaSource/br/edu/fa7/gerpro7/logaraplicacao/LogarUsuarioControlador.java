/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.fa7.gerpro7.logaraplicacao;

import javax.persistence.NoResultException;

import br.edu.fa7.gerpro7.dao.UsuarioHome;
import br.edu.fa7.gerpro7.entidades.Usuario;
import br.edu.fa7.gerpro7.util.DAOFactory;

/**
 *
 * @author Marcelo
 */
public class LogarUsuarioControlador {

   
/**
 * Metodo de controle de validaçao dos dados do usuario. Recebe os valores e faz o controle
 * @param usuarioSelecionado
 * @return
 * @throws java.lang.Exception
 */
    public Usuario validarUsuario(Usuario usuarioSelecionado) throws Exception {
        try {
        	DAOFactory.getSession().getTransaction().begin();
             if (usuarioSelecionado.getMatricula()==null ||usuarioSelecionado.getMatricula().isEmpty())
                throw new Exception("Campo Login Obrigatorio!");
            UsuarioHome dao = UsuarioHome.getInstance();
            Usuario usuarioRetornado = null; //dao.findByMatricula(usuarioSelecionado.getMatricula());
           
            
             if (usuarioSelecionado.getSenha().length()<6)
                throw new Exception("Tipo de senha invalido - A senha deve ter no minimo 6 caracteres!");
      
             if (!usuarioRetornado.isAprovado())
                throw new Exception("Login ou senha invalido!");
            
            if (!usuarioRetornado.getSenha().equals(usuarioSelecionado.getSenha()))
                throw new Exception("Login ou senha inválida!");
            
            DAOFactory.getSession().getTransaction().commit();
            return usuarioRetornado;
        } 
        catch (NoResultException e) {
        	DAOFactory.getSession().getTransaction().rollback();
            throw new Exception("Login ou senha invalida!");
        }
        
        catch (Exception e) {
        	DAOFactory.getSession().getTransaction().rollback();
            throw new Exception(e.getMessage());
        }

    //throw new UnsupportedOperationException("Not yet implemented");



    }
    
    
    
}
