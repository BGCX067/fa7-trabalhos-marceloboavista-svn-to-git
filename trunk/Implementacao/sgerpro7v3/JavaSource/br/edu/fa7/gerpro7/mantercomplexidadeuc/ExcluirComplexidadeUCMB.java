package br.edu.fa7.gerpro7.mantercomplexidadeuc;

import java.util.List;

import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.mantercasouso.ListaCasosUsoMB;
import br.edu.fa7.gerpro7.mantercasouso.ManterCasoUsoControlador;
import br.edu.fa7.gerpro7.util.FacesBean;

public class ExcluirComplexidadeUCMB extends FacesBean {
	
	/**
	 * Propriedade que representa o caso de uso que est� sendo excluido
	 */
	private ComplexidadeUC complexidadeUCEditado = new ComplexidadeUC();
	
	public ComplexidadeUC getComplexidadeUCEditado() {
		return complexidadeUCEditado;
	}

	public void setComplexidadeUCEditado(ComplexidadeUC complexidadeUCEditado) {
		this.complexidadeUCEditado = complexidadeUCEditado;
	}

	private ComplexidadeUC complexidadeUCExcluir = new ComplexidadeUC();

	/**
	 * Obtem objeto Complexidade Caso de Uso a ser editado
	 * @return
	 */

	public ComplexidadeUC getComplexidadeUCExcluir() {
		return complexidadeUCExcluir;
	}

	public void setComplexidadeUCExcluir(ComplexidadeUC complexidadeUCExcluir) {
		this.complexidadeUCExcluir = complexidadeUCExcluir;
	}

	
	public String excluir() {
		try {
			ManterComplexidadeUCControlador complexidadeUCControlador = ManterComplexidadeUCControlador.getInstance();
			complexidadeUCControlador.excluir(complexidadeUCEditado);
			info("Complexidade UC excluido com sucesso");
			//Atualiza a listagem de resultados
			List<ComplexidadeUC> lista = ManterComplexidadeUCControlador.getInstance()
					.obterTodasComplexidadesUC();

			ListarComplexidadeUCMB complexidadeUCMB = (ListarComplexidadeUCMB) getBean("listarComplexidadeUCMB");
			complexidadeUCMB.setLista(lista);
			return "listarComplexidadeUC";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}
	}
	
}
