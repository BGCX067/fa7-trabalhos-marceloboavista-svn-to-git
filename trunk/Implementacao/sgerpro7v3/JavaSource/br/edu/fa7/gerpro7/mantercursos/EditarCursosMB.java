package br.edu.fa7.gerpro7.mantercursos;

import java.util.List;

import br.edu.fa7.gerpro7.entidades.Curso;
import br.edu.fa7.gerpro7.util.FacesBean;

public class EditarCursosMB extends FacesBean {
	/**
	 * Propriedade que representa o Curso que est� sendo
	 */
	private Curso cursoEditado = new Curso();

	/**
	 * @return the cursoEditado
	 */
	public Curso getCursoEditado() {
		return cursoEditado;
	}

	/**
	 * @param cursoEditado
	 *            the cursoEditado to set
	 */
	public void setCursoEditado(Curso cursoEditado) {
		this.cursoEditado = cursoEditado;
	}
	


	public String salvar() {

		try {
			ManterCursosControlador cursoControlador = ManterCursosControlador
					.getInstance();
			cursoControlador.salvar(cursoEditado);
			info("Curso criado com sucesso.");
			List<Curso> lista = ManterCursosControlador.getInstance()
					.obterTodosCursos();
			ListarCursosMB cursosMB = (ListarCursosMB) getBean("listarCursosMB");
			cursosMB.setLista(lista);
			return "listaCursos";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}

	}
	
	public String cancelar() {

		try {

			return "listaCursos";

		} catch (Exception e) {
			error(e.getMessage());
			return null;

		}

	}

	
}
