package br.edu.fa7.gerpro7.entidades;

// Generated 19/04/2009 16:28:33 by Hibernate Tools 3.2.4.GA

import java.util.Date;
import java.util.HashSet;
import java.util.Set;


/**
 * Projeto generated by hbm2java
 */
public class Projeto implements java.io.Serializable {

	private Integer idProjeto;
	private StatusProjeto statusprojeto;
	private Usuario usuario;
	private Sistema sistema;
	private String sigla;
	private String titulo;
	private String descricao;
	private Date dataInicio;
	private Date dataFinal;
	private String problema;
	private String quemAfeta;
	private String solucao;
	private String ano;
	private String semestre;
	private Boolean aprovado;
	private String solucaoProjeto;
	private Set<UsuarioProjeto> usuarioprojetos = new HashSet<UsuarioProjeto>(0);
	private Set<CasoUso> casousos = new HashSet<CasoUso>(0);

	public Projeto() {
	}

	public Projeto(StatusProjeto statusprojeto, Usuario usuario, Sistema sistema) {
		this.statusprojeto = statusprojeto;
		this.usuario = usuario;
		this.sistema = sistema;
	}

	public Projeto(StatusProjeto statusprojeto, Usuario usuario,
			Sistema sistema, String sigla, String titulo, String descricao,
			Date dataInicio, Date dataFinal, String problema, String quemAfeta,
			String solucao, String ano, String semestre, Boolean aprovado,
			String solucaoProjeto, Set<UsuarioProjeto> usuarioprojetos,
			Set<CasoUso> casousos) {
		this.statusprojeto = statusprojeto;
		this.usuario = usuario;
		this.sistema = sistema;
		this.sigla = sigla;
		this.titulo = titulo;
		this.descricao = descricao;
		this.dataInicio = dataInicio;
		this.dataFinal = dataFinal;
		this.problema = problema;
		this.quemAfeta = quemAfeta;
		this.solucao = solucao;
		this.ano = ano;
		this.semestre = semestre;
		this.aprovado = aprovado;
		this.solucaoProjeto = solucaoProjeto;
		this.usuarioprojetos = usuarioprojetos;
		this.casousos = casousos;
	}

	public Integer getIdProjeto() {
		return this.idProjeto;
	}

	public void setIdProjeto(Integer idProjeto) {
		this.idProjeto = idProjeto;
	}

	public StatusProjeto getStatusprojeto() {
		return this.statusprojeto;
	}

	public void setStatusprojeto(StatusProjeto statusprojeto) {
		this.statusprojeto = statusprojeto;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public Sistema getSistema() {
		return this.sistema;
	}

	public void setSistema(Sistema sistema) {
		this.sistema = sistema;
	}

	public String getSigla() {
		return this.sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public String getTitulo() {
		return this.titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getDescricao() {
		return this.descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getDataInicio() {
		return this.dataInicio;
	}

	public void setDataInicio(Date dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Date getDataFinal() {
		return this.dataFinal;
	}

	public void setDataFinal(Date dataFinal) {
		this.dataFinal = dataFinal;
	}

	public String getProblema() {
		return this.problema;
	}

	public void setProblema(String problema) {
		this.problema = problema;
	}

	public String getQuemAfeta() {
		return this.quemAfeta;
	}

	public void setQuemAfeta(String quemAfeta) {
		this.quemAfeta = quemAfeta;
	}

	public String getSolucao() {
		return this.solucao;
	}

	public void setSolucao(String solucao) {
		this.solucao = solucao;
	}

	public String getAno() {
		return this.ano;
	}

	public void setAno(String ano) {
		this.ano = ano;
	}

	public String getSemestre() {
		return this.semestre;
	}

	public void setSemestre(String semestre) {
		this.semestre = semestre;
	}

	public Boolean getAprovado() {
		return this.aprovado;
	}

	public void setAprovado(Boolean aprovado) {
		this.aprovado = aprovado;
	}

	public String getSolucaoProjeto() {
		return this.solucaoProjeto;
	}

	public void setSolucaoProjeto(String solucaoProjeto) {
		this.solucaoProjeto = solucaoProjeto;
	}

	public Set<UsuarioProjeto> getUsuarioprojetos() {
		return this.usuarioprojetos;
	}

	public void setUsuarioprojetos(Set<UsuarioProjeto> usuarioprojetos) {
		this.usuarioprojetos = usuarioprojetos;
	}

	public Set<CasoUso> getCasousos() {
		return this.casousos;
	}

	public void setCasousos(Set<CasoUso> casousos) {
		this.casousos = casousos;
	}

}
