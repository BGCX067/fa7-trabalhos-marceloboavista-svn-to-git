package br.edu.fa7.gerpro7.dao;

// Generated 19/04/2009 16:28:34 by Hibernate Tools 3.2.4.GA

import static org.hibernate.criterion.Example.create;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;

import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.util.DAOFactory;

/**
 * Home object for domain model class Casouso.
 * 
 * @see br.edu.fa7.gerpro7.entidades.CasoUso
 * @author Hibernate Tools
 */
public class CasousoHome {

	private static CasousoHome instance = null;

	private CasousoHome() {
	}

	public static CasousoHome getInstance() {
		if (instance == null)
			instance = new CasousoHome();
		return instance;
	}

	private static final Log log = LogFactory.getLog(CasousoHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) DAOFactory.getSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(CasoUso transientInstance) {
		log.debug("persisting Casouso instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(CasoUso instance) {
		log.debug("attaching dirty Casouso instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(CasoUso instance) {
		log.debug("attaching clean Casouso instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(CasoUso persistentInstance) {
		log.debug("deleting Casouso instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public CasoUso merge(CasoUso detachedInstance) {
		log.debug("merging Casouso instance");
		try {
			CasoUso result = (CasoUso) sessionFactory.getCurrentSession()
					.merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public CasoUso findById(java.lang.Integer id) {
		log.debug("getting Casouso instance with id: " + id);
		try {
			CasoUso instance = (CasoUso) sessionFactory.getCurrentSession()
					.get(CasoUso.class.getName(), id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<CasoUso> findByExample(CasoUso instance) {
		log.debug("finding Casouso instance by example");
		try {
			List<CasoUso> results = (List<CasoUso>) sessionFactory
					.getCurrentSession()
					.createCriteria(CasoUso.class.getName()).add(
							create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}

	public List<CasoUso> findAll() {
		log.debug("finding all Casouso");
		try {
			List<CasoUso> results = (List<CasoUso>) sessionFactory
					.getCurrentSession().createQuery("from CasoUso").list();

			log.debug("find all successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
