package br.edu.fa7.gerpro7.manterperfil;

import java.util.ArrayList;
import java.util.List;

import br.edu.fa7.gerpro7.entidades.Funcionalidades;
import br.edu.fa7.gerpro7.entidades.Perfil;
import br.edu.fa7.gerpro7.util.FacesBean;

public class ListarPerfisMB extends FacesBean {

	private Perfil perfilSelecionado = null;

	private List<Perfil> lista = new ArrayList<Perfil>();

	public List<Perfil> getLista() {
		return lista;
	}

	public void setLista(List<Perfil> lista) {
		this.lista = lista;
	}

	public ListarPerfisMB() {
		// Funcionalidades funcionalidadesUC01 = new Funcionalidades();
		// funcionalidadesUC01.setIdFuncionalidades(1);
		// funcionalidadesUC01.setNomeFuncionalidade("Teste Funcionalidade");
		//		
		//		
		// Perfil perfil01 = new Perfil();
		// perfil01.setIdPerfil(1);
		// perfil01.setNome("Teste");
		// lista.add(perfil01);
		//		
		// Funcionalidades funcionalidadesUC02 = new Funcionalidades();
		// funcionalidadesUC01.setIdFuncionalidades(2);
		// funcionalidadesUC01.setNomeFuncionalidade("Funcionalidade 02");
		//	
		//		
		// Perfil perfil02 = new Perfil();
		// perfil02.setIdPerfil(2);
		// perfil02.setNome("Teste02");
		// lista.add(perfil02);
		//		
		// Perfil perfil03 = new Perfil();
		// perfil03.setIdPerfil(3);
		// perfil03.setNome("Teste03");
		// lista.add(perfil03);
		//		
		// Perfil perfil04 = new Perfil();
		// perfil04.setIdPerfil(4);
		// perfil04.setNome("Teste04");
		// lista.add(perfil04);
		//		
		// Perfil perfil05 = new Perfil();
		// perfil05.setIdPerfil(5);
		// perfil05.setNome("Teste05");
		// lista.add(perfil05);
		//		
		// Perfil perfil06 = new Perfil();
		// perfil06.setIdPerfil(6);
		// perfil06.setNome("Teste06");
		// lista.add(perfil06);
		//		
		// Perfil perfil07 = new Perfil();
		// perfil07.setIdPerfil(7);
		// perfil07.setNome("Teste07");
		// lista.add(perfil07);
		//		
		// Perfil perfil08 = new Perfil();
		// perfil08.setIdPerfil(8);
		// perfil08.setNome("Teste08");
		// lista.add(perfil08);

	}

	public String NovoPerfil() {
		try {
			Perfil perfil = new Perfil();
			EditarPerfisMB editaPerfil = (EditarPerfisMB) getBean("editarPerfisMB");
			editaPerfil.setPerfil(perfil);
			ManterPerfilControlador controlador = ManterPerfilControlador
					.getInstance();
			List<Funcionalidades> list = controlador
					.obterTodasFuncionalidades();
			editaPerfil.prepararListaPerfis(list);
			return "editarPerfil";
		} catch (Exception e) {
			error(e.getMessage());
			return null;
		}

	}

	public String EditarPerfil() {
		try {
			System.out.println("Editar o Perfil: "
					+ perfilSelecionado.getNome());
			EditarPerfisMB editarPerfil = (EditarPerfisMB) getBean("editarPerfisMB");
			editarPerfil.setPerfil(perfilSelecionado);
			ManterPerfilControlador controlador = ManterPerfilControlador
					.getInstance();
			List<Funcionalidades> list = controlador
					.obterTodasFuncionalidades();
			editarPerfil.prepararListaPerfis(list);
			return "editarPerfil";
		} catch (Exception e) {
			return null;
		}
	}

	public String ExcluirPerfil() {
		try {
			System.out.println("Excluir o Perfil: "
					+ perfilSelecionado.getNome());
			ExcluirPerfisMB excluirPerfisMB = (ExcluirPerfisMB) getBean("excluirPerfisMB");
			excluirPerfisMB.setPerfil(perfilSelecionado);
			return "excluirPerfil";
		} catch (Exception e) {
			return null;
		}
	}

	public Perfil getperfilSelecionado() {
		return perfilSelecionado;
	}

	public void setperfilSelecionado(Perfil perfilSelecionado) {
		this.perfilSelecionado = perfilSelecionado;
	}

}
