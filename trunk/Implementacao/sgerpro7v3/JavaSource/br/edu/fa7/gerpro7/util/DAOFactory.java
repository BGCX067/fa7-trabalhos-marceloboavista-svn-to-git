package br.edu.fa7.gerpro7.util;

/*
 * Created on 11/05/2005
 *
 * C�digo desenvolvido por Maur�cio Linhares
 * 
 */

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DAOFactory {

	private static SessionFactory factory;

	static {
		// Bloco est�tico que inicializa o Hibernate
		try {

			Configuration cfg = new Configuration().configure("hibernate.cfg.xml");
			SessionFactory sf = cfg.buildSessionFactory();
			// factory = new Configuration().configure().buildSessionFactory();
			factory = sf;

		} catch (Exception e) {

			e.printStackTrace();
			factory = null;
		}
	}

	public static Session getSession() {
		// Retorna a sess�o aberta
		return factory.getCurrentSession();

	}

	public static SessionFactory getSessionFactory() {
		// Retorna a sess�o aberta
		return factory;

	}

}
