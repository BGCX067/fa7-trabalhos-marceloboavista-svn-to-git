package br.edu.fa7.gerpro7.mantercasouso;

import java.util.List;

import br.edu.fa7.gerpro7.dao.CasousoHome;
import br.edu.fa7.gerpro7.dao.ComplexidadeUCHome;
import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.util.DAOFactory;

public class ManterCasoUsoControlador {

	private static ManterCasoUsoControlador instance = null;

	private ManterCasoUsoControlador() {
	}

	public static ManterCasoUsoControlador getInstance() {
		if (instance == null) {
			instance = new ManterCasoUsoControlador();
		}
		return instance;
	}

	public List<CasoUso> obterTodosCasosUso() {
		CasousoHome dao = CasousoHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		List<CasoUso> lista = dao.findAll();

		for (CasoUso casoUso : lista) {
			casoUso.getComplexidadeuc().getNome();
		}

		DAOFactory.getSession().getTransaction().commit();
		return lista;
	}

	public List<ComplexidadeUC> getTodasComplexidadesUC() throws Exception {
		ComplexidadeUCHome dao = ComplexidadeUCHome.getInstance();
		DAOFactory.getSession().beginTransaction();
		List<ComplexidadeUC> lista = dao.findAll();
		DAOFactory.getSession().getTransaction().commit();

		if (lista == null || lista.size() < 1) {
			throw new Exception(
					"Lista de complexidades vazia. Favor cadastrar no Manter Complexidade UC");
		}

		return lista;
	}

	/**
	 * Metodo que salva um caso de uso. Pode ser novo ou uma altera��o
	 * 
	 * @param casoUsoEditado
	 * @throws Exception
	 */
	public void salvar(CasoUso casoUsoEditado) throws Exception {
		CasousoHome dao = CasousoHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		validaUnicidadeNome(casoUsoEditado);
		if (casoUsoEditado.getIdCasoUso() == null)// Caso de uso novo
			dao.persist(casoUsoEditado);
		else
			dao.merge(casoUsoEditado);
		DAOFactory.getSession().getTransaction().commit();
	}

	/**
	 * Valida a unicidade do nome do caso de uso
	 * 
	 * @param nome
	 * @throws Exception
	 */
	private void validaUnicidadeNome(CasoUso casoUsoEditado) throws Exception {
		CasousoHome dao = CasousoHome.getInstance();
		CasoUso casoUso = new CasoUso();
		casoUso.setNome(casoUsoEditado.getNome());
		List<CasoUso> listaCasoUso = dao.findByExample(casoUso);

		for (CasoUso casoUso2 : listaCasoUso) {
			if (!casoUso2.getIdCasoUso().equals(casoUsoEditado.getIdCasoUso()))
				throw new Exception("Nome do caso de uso j� existe cadastrado");

		}

	}

	public void excluir(CasoUso casoUsoEditado) {
		CasousoHome dao = CasousoHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		dao.delete(casoUsoEditado);
		DAOFactory.getSession().getTransaction().commit();

	}

}
