package br.edu.fa7.gerpro7.mantercomplexidadeuc;

import java.util.List;

import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.mantercasouso.ListaCasosUsoMB;
import br.edu.fa7.gerpro7.mantercasouso.ManterCasoUsoControlador;
import br.edu.fa7.gerpro7.util.FacesBean;

public class EditarComplexidadeUCMB extends FacesBean {
	
	/**
	 * Propriedade que representa o caso de uso que est� sendo editado
	 */
	private ComplexidadeUC complexidadeUCEdit = new ComplexidadeUC();

	/**
	 * Obtem objeto Complexidade Caso de Uso a ser editado
	 * @return
	 */
	public ComplexidadeUC getComplexidadeUCEdit() {
		return complexidadeUCEdit;
	}

	public void setComplexidadeUCEdit(ComplexidadeUC complexidadeUCEdit) {
		this.complexidadeUCEdit = complexidadeUCEdit;
	}
	
	/**
	 * m�todo utilizado para salvar a complexidade de casos de uso
	 * @return
	 */
	public String save(){
		try {
			ManterComplexidadeUCControlador complexidadeUCControlador = ManterComplexidadeUCControlador.getInstance();
			complexidadeUCControlador.salvar(complexidadeUCEdit);
			info("Complexidade UC salva com sucesso");
			//Atualiza a listagem de resultados
			List<ComplexidadeUC> lista = ManterComplexidadeUCControlador.getInstance().obterTodasComplexidadesUC();
			
			ListarComplexidadeUCMB complexidadeUCMB = (ListarComplexidadeUCMB) getBean("listaComplexidadeUCMB");
			complexidadeUCMB.setLista(lista);
			return "listarComplexidadeUC";
		} catch (Exception e) {
			return null;
		}
	}
	

	
	
	/**
	 * m�todo utilizado para cancelar a complexidade de casos de uso
	 * @return
	 */
	public String cancelar(){
		try {
			return "listarComplexidadeUC";
		} catch (Exception e) {
			return null;
		}
	}
}
