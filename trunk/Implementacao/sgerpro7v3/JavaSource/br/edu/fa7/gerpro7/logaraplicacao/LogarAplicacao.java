/*
 * LogarAplicacao.java
 *
 * Created on 14/09/2008, 18:45:44
 */

package br.edu.fa7.gerpro7.logaraplicacao;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpSession;

import br.edu.fa7.gerpro7.dao.CasousoHome;
import br.edu.fa7.gerpro7.dao.ComplexidadeUCHome;
import br.edu.fa7.gerpro7.dao.CursoHome;
import br.edu.fa7.gerpro7.dao.FuncionalidadesHome;
import br.edu.fa7.gerpro7.dao.PerfilHome;
import br.edu.fa7.gerpro7.dao.StatusProjetoHome;
import br.edu.fa7.gerpro7.dao.StatusSistemaHome;
import br.edu.fa7.gerpro7.dao.UsuarioHome;
import br.edu.fa7.gerpro7.entidades.CasoUso;
import br.edu.fa7.gerpro7.entidades.ComplexidadeUC;
import br.edu.fa7.gerpro7.entidades.Curso;
import br.edu.fa7.gerpro7.entidades.Funcionalidades;
import br.edu.fa7.gerpro7.entidades.Perfil;
import br.edu.fa7.gerpro7.entidades.StatusProjeto;
import br.edu.fa7.gerpro7.entidades.StatusSistema;
import br.edu.fa7.gerpro7.entidades.Usuario;
import br.edu.fa7.gerpro7.util.DAOFactory;
import br.edu.fa7.gerpro7.util.FacesBean;

/**
 * <p>
 * Page bean that corresponds to a similarly named JSP page. This class contains
 * component definitions (and initialization code) for all components that you
 * have defined on this page, as well as lifecycle methods and event handlers
 * where you may add behavior to respond to incoming events.
 * </p>
 * 
 * @author Marcelo
 */
public class LogarAplicacao extends FacesBean {

	private Usuario usuarioLogin = new Usuario();

	
	/**
	 * <p>
	 * Construct a new Page bean instance.
	 * </p>
	 */
	public LogarAplicacao() {
	}

	/**
	 * * Cria os dados no Banco
	 */
	public void criaDadosBanco() {

		try {

			// Cria o curso de SI

			DAOFactory.getSession().getTransaction().begin();

			Curso curso = new Curso();
			curso.setIdCurso(1);
			try {

				curso.setNome("SI");
				CursoHome cursoHome = CursoHome.getInstance();
				cursoHome.merge(curso);

			} catch (Exception e) {
				e.printStackTrace();
			}

			// Cria as funcionalidades
			Funcionalidades funcionalidade1 = new Funcionalidades();
			try {
				funcionalidade1.setIdFuncionalidades(1);
				funcionalidade1.setNomeFuncionalidade("AprovarCadastroUsuario");
				FuncionalidadesHome daoFuncionalidade = FuncionalidadesHome.getInstance();
				daoFuncionalidade.merge(funcionalidade1);
			} catch (Exception e) {
				e.printStackTrace();
			}

			Funcionalidades funcionalidade2 = new Funcionalidades();
			try {

				funcionalidade2.setIdFuncionalidades(1);
				funcionalidade2
						.setNomeFuncionalidade("SolicitarCadastroSistema");
				FuncionalidadesHome daoFuncionalidade = FuncionalidadesHome.getInstance();
				daoFuncionalidade.merge(funcionalidade2);

			} catch (Exception e) {
				e.printStackTrace();
			}

			// Cria o Perfil de Aluno
			Perfil perfil = new Perfil();
			try {
				perfil.setIdPerfil(1);
				perfil.setNome("Aluno");
				PerfilHome daoPerfil = PerfilHome.getInstance();
				Set<Funcionalidades> listaFuncionalidades = new HashSet<Funcionalidades>();
				listaFuncionalidades.add(funcionalidade1);
				listaFuncionalidades.add(funcionalidade2);
				perfil.setFuncionalidadeses(listaFuncionalidades);
				daoPerfil.merge(perfil);
			} catch (Exception e) {
				e.printStackTrace();
			}

			UsuarioHome daoUsuario = UsuarioHome.getInstance();
			Usuario aluno = daoUsuario.findById(2);
			// Cria o usuário Administrador
			if (aluno == null) {
				aluno = new Usuario();
				aluno.setIdUsuario(2);
				aluno.setAprovado(true);
				aluno.setCelular("5555555");
				aluno.setCurso(curso);
				aluno.setEmail("malcantara@fa7.edu.br");
				aluno.setFone("5555555");
				aluno.setMatricula("11111111");
				aluno.setSenha("11112222");
				aluno.setPerfil(perfil);
				aluno.setNome("Aluno");

				daoUsuario.merge(aluno);
			}
			HttpSession session = (HttpSession) getExternalContext()
					.getSession(true);
			session.setAttribute("usuarioLogado", aluno);
			// Cria o Perfil de Orientador
			perfil = new Perfil();
			try {
				perfil.setIdPerfil(2);
				perfil.setNome("Orientador");
				PerfilHome daoPerfil = PerfilHome.getInstance();
				daoPerfil.merge(perfil);
			} catch (Exception e) {
				e.printStackTrace();
			}

			aluno = daoUsuario.findById(new Integer(3));
			// Cria o usuário Administrador
			if (aluno == null) {
				aluno = new Usuario();
				aluno.setIdUsuario(3);
				aluno.setAprovado(false);
				aluno.setCelular("5555555");
				aluno.setCurso(curso);
				aluno.setEmail("malcantara@fa7.edu.br");
				aluno.setFone("5555555");
				aluno.setMatricula("2222222");
				aluno.setSenha("11112222");
				aluno.setPerfil(perfil);
				aluno.setNome("Orientador");

				daoUsuario.merge(aluno);
			}

			StatusProjeto statusP = new StatusProjeto();
			statusP.setIdStatusProjeto(1);
			statusP.setNomeProjeto("Em Aprova��o");

			StatusProjetoHome daoStatusP = StatusProjetoHome.getInstance();
			daoStatusP.merge(statusP);

			StatusProjeto statusP1 = new StatusProjeto();
			statusP1.setIdStatusProjeto(2);
			statusP1.setNomeProjeto("Em Andamento");
			daoStatusP.merge(statusP1);

			StatusSistemaHome daoStatusS = StatusSistemaHome.getInstance();

			StatusSistema statusS1 = new StatusSistema();
			statusS1.setIdStatusSistema(1);
			statusS1.setNomeSistema("Em Andamento");
			daoStatusS.merge(statusS1);

			ComplexidadeUC compUC = new ComplexidadeUC();

			ComplexidadeUCHome complexidadeUCHome = ComplexidadeUCHome
					.getInstance();
			compUC.setIdComplexidadeUc(1);
			compUC.setNome("Complexo");
			complexidadeUCHome.merge(compUC);

			CasoUso casoUso = new CasoUso();

			CasousoHome casousoHome = CasousoHome.getInstance();

			casoUso.setComplexidadeuc(compUC);
			// casoUso.setIdCasoUso(1);
			casoUso.setDescricao("UC01");
			casoUso.setNome("UC01");

			casousoHome.merge(casoUso);

			DAOFactory.getSession().getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	public String logar() {
        
        criaDadosBanco();
        LogarUsuarioControlador controlador = new LogarUsuarioControlador();
        try {
            Usuario usuario = controlador.validarUsuario(usuarioLogin);
            HttpSession session = (HttpSession) getExternalContext().getSession(true);
            session.setAttribute("usuarioLogado", usuario);   
            //ExibirTelaPrincipalControlador exibirTelaPrincipalControlador = new ExibirTelaPrincipalControlador();
            return null;
        } catch (Exception ex) {
            
            error(ex.getMessage());
            return null;
        }
        
    }


}
