package br.edu.fa7.gerpro7.manterfuncionalidades;

import br.edu.fa7.gerpro7.entidades.Funcionalidades;

public class ExcluirFuncionalidadesMB {

	/*
	 * Propriedade que representa a funcionalidade que esta sendo excluida
	 * */
	private Funcionalidades funcionalidadesExcluida = new Funcionalidades();

	public Funcionalidades getFuncionalidadesExcluida() {
		return funcionalidadesExcluida;
	}

	public void setFuncionalidadesExcluida(Funcionalidades funcionalidadesExcluida) {
		this.funcionalidadesExcluida = funcionalidadesExcluida;
	}
	public String deletar(){
		try {
			
			
			return "listaFuncionalidade";
		} catch (Exception e) {
			return null;
		}
	}
	/*
	 * Metodo para Cancelar Funcionalidade Editada
	 * */
	public String cancelar(){
		try {
			return "listaFuncionalidade";
		} catch (Exception e) {
			return null;
		}
	}
}
