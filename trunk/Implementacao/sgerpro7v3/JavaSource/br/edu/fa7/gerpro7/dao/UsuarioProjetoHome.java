package br.edu.fa7.gerpro7.dao;

// Generated 19/04/2009 16:28:34 by Hibernate Tools 3.2.4.GA

import java.util.List;
import javax.naming.InitialContext;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.LockMode;
import org.hibernate.SessionFactory;

import br.edu.fa7.gerpro7.entidades.UsuarioProjeto;
import br.edu.fa7.gerpro7.util.DAOFactory;
import static org.hibernate.criterion.Example.create;

/**
 * Home object for domain model class Usuarioprojeto.
 * @see br.edu.fa7.gerpro7.entidades.UsuarioProjeto
 * @author Hibernate Tools
 */
public class UsuarioProjetoHome {

	private static final Log log = LogFactory.getLog(UsuarioProjetoHome.class);

	private final SessionFactory sessionFactory = getSessionFactory();

	protected SessionFactory getSessionFactory() {
		try {
			return (SessionFactory) DAOFactory.getSessionFactory();
		} catch (Exception e) {
			log.error("Could not locate SessionFactory in JNDI", e);
			throw new IllegalStateException(
					"Could not locate SessionFactory in JNDI");
		}
	}

	public void persist(UsuarioProjeto transientInstance) {
		log.debug("persisting Usuarioprojeto instance");
		try {
			sessionFactory.getCurrentSession().persist(transientInstance);
			log.debug("persist successful");
		} catch (RuntimeException re) {
			log.error("persist failed", re);
			throw re;
		}
	}

	public void attachDirty(UsuarioProjeto instance) {
		log.debug("attaching dirty Usuarioprojeto instance");
		try {
			sessionFactory.getCurrentSession().saveOrUpdate(instance);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void attachClean(UsuarioProjeto instance) {
		log.debug("attaching clean Usuarioprojeto instance");
		try {
			sessionFactory.getCurrentSession().lock(instance, LockMode.NONE);
			log.debug("attach successful");
		} catch (RuntimeException re) {
			log.error("attach failed", re);
			throw re;
		}
	}

	public void delete(UsuarioProjeto persistentInstance) {
		log.debug("deleting Usuarioprojeto instance");
		try {
			sessionFactory.getCurrentSession().delete(persistentInstance);
			log.debug("delete successful");
		} catch (RuntimeException re) {
			log.error("delete failed", re);
			throw re;
		}
	}

	public UsuarioProjeto merge(UsuarioProjeto detachedInstance) {
		log.debug("merging Usuarioprojeto instance");
		try {
			UsuarioProjeto result = (UsuarioProjeto) sessionFactory
					.getCurrentSession().merge(detachedInstance);
			log.debug("merge successful");
			return result;
		} catch (RuntimeException re) {
			log.error("merge failed", re);
			throw re;
		}
	}

	public UsuarioProjeto findById(br.edu.fa7.gerpro7.entidades.UsuarioProjetoId id) {
		log.debug("getting Usuarioprojeto instance with id: " + id);
		try {
			UsuarioProjeto instance = (UsuarioProjeto) sessionFactory
					.getCurrentSession().get(
							"br.edu.fa7.gerpro7.Usuarioprojeto", id);
			if (instance == null) {
				log.debug("get successful, no instance found");
			} else {
				log.debug("get successful, instance found");
			}
			return instance;
		} catch (RuntimeException re) {
			log.error("get failed", re);
			throw re;
		}
	}

	public List<UsuarioProjeto> findByExample(UsuarioProjeto instance) {
		log.debug("finding Usuarioprojeto instance by example");
		try {
			List<UsuarioProjeto> results = (List<UsuarioProjeto>) sessionFactory
					.getCurrentSession().createCriteria(
							"br.edu.fa7.gerpro7.Usuarioprojeto").add(
							create(instance)).list();
			log.debug("find by example successful, result size: "
					+ results.size());
			return results;
		} catch (RuntimeException re) {
			log.error("find by example failed", re);
			throw re;
		}
	}
}
