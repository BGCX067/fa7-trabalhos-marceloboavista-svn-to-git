package br.edu.fa7.gerpro7.manterstatussistema;

import java.util.List;

import br.edu.fa7.gerpro7.dao.StatusSistemaHome;
import br.edu.fa7.gerpro7.entidades.StatusSistema;
import br.edu.fa7.gerpro7.util.DAOFactory;

public class ManterStatusSistemaControlador {
	
	private static ManterStatusSistemaControlador instance = null; 
	
	private ManterStatusSistemaControlador() {
		}
	
	public static ManterStatusSistemaControlador getInstance(){
		if (instance == null) {
			instance = new ManterStatusSistemaControlador();
			}
		return instance;
	}
 
	public List<StatusSistema> obterTodosStatusSistema() {
		
		StatusSistemaHome dao = StatusSistemaHome.getInstance();
		
		DAOFactory.getSession().getTransaction().begin();
		
		List<StatusSistema> lista = dao.findAll();
		
		DAOFactory.getSession().getTransaction().commit();
		
		return lista;
	}

	public void salvar(StatusSistema statusSistemaEditado)throws Exception {
		StatusSistemaHome dao= StatusSistemaHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		validaUnicidadeNome(statusSistemaEditado);
		if (statusSistemaEditado.getIdStatusSistema()== null)
			dao.persist(statusSistemaEditado);
		else
			dao.merge(statusSistemaEditado);
		DAOFactory.getSession().getTransaction().commit();
		
			
	}
	
	private void validaUnicidadeNome(StatusSistema statusSistemaEditado) throws Exception {
		StatusSistemaHome dao = StatusSistemaHome.getInstance();
		StatusSistema statusSistema = new StatusSistema();
		statusSistema.setNomeSistema(statusSistemaEditado.getNomeSistema());
		List<StatusSistema> listaStatusSistema = dao.findByExample(statusSistema);
		
		for (StatusSistema statusSistema2 : listaStatusSistema) {
			if(!statusSistema2.getIdStatusSistema().equals(statusSistemaEditado.getIdStatusSistema()))
				throw new Exception("Nome do Status do Sistema ja cadastrado");
		}
					
	}
	public void excluir(StatusSistema statusSistemaEditado) {
		StatusSistemaHome dao = StatusSistemaHome.getInstance();
		DAOFactory.getSession().getTransaction().begin();
		dao.delete(statusSistemaEditado);
		DAOFactory.getSession().getTransaction().commit();

	}
}
