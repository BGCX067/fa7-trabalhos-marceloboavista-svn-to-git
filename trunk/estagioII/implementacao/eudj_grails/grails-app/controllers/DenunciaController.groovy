

class DenunciaController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ denunciaInstanceList: Denuncia.list( params ), denunciaInstanceTotal: Denuncia.count() ]
    }

    def show = {
        def denunciaInstance = Denuncia.get( params.id )

        if(!denunciaInstance) {
            flash.message = "Denuncia not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ denunciaInstance : denunciaInstance ] }
    }

    def delete = {
        def denunciaInstance = Denuncia.get( params.id )
        if(denunciaInstance) {
            try {
                denunciaInstance.delete(flush:true)
                flash.message = "Denuncia ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Denuncia ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Denuncia not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def denunciaInstance = Denuncia.get( params.id )

        if(!denunciaInstance) {
            flash.message = "Denuncia not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ denunciaInstance : denunciaInstance ]
        }
    }

    def update = {
        def denunciaInstance = Denuncia.get( params.id )
        if(denunciaInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(denunciaInstance.version > version) {
                    
                    denunciaInstance.errors.rejectValue("version", "denuncia.optimistic.locking.failure", "Another user has updated this Denuncia while you were editing.")
                    render(view:'edit',model:[denunciaInstance:denunciaInstance])
                    return
                }
            }
            denunciaInstance.properties = params
            if(!denunciaInstance.hasErrors() && denunciaInstance.save()) {
                flash.message = "Denuncia ${params.id} updated"
                redirect(action:show,id:denunciaInstance.id)
            }
            else {
                render(view:'edit',model:[denunciaInstance:denunciaInstance])
            }
        }
        else {
            flash.message = "Denuncia not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def denunciaInstance = new Denuncia()
        denunciaInstance.properties = params
        return ['denunciaInstance':denunciaInstance]
    }

    def save = {
        def denunciaInstance = new Denuncia(params)
        if(!denunciaInstance.hasErrors() && denunciaInstance.save()) {
            flash.message = "Denuncia ${denunciaInstance.id} created"
            redirect(action:show,id:denunciaInstance.id)
        }
        else {
            render(view:'create',model:[denunciaInstance:denunciaInstance])
        }
    }
}
