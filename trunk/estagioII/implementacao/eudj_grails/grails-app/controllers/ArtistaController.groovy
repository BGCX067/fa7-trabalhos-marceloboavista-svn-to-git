

class ArtistaController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ artistaInstanceList: Artista.list( params ), artistaInstanceTotal: Artista.count() ]
    }

    def show = {
        def artistaInstance = Artista.get( params.id )

        if(!artistaInstance) {
            flash.message = "Artista not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ artistaInstance : artistaInstance ] }
    }

    def delete = {
        def artistaInstance = Artista.get( params.id )
        if(artistaInstance) {
            try {
                artistaInstance.delete(flush:true)
                flash.message = "Artista ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Artista ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Artista not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def artistaInstance = Artista.get( params.id )

        if(!artistaInstance) {
            flash.message = "Artista not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ artistaInstance : artistaInstance ]
        }
    }

    def update = {
        def artistaInstance = Artista.get( params.id )
        if(artistaInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(artistaInstance.version > version) {
                    
                    artistaInstance.errors.rejectValue("version", "artista.optimistic.locking.failure", "Another user has updated this Artista while you were editing.")
                    render(view:'edit',model:[artistaInstance:artistaInstance])
                    return
                }
            }
            artistaInstance.properties = params
            if(!artistaInstance.hasErrors() && artistaInstance.save()) {
                flash.message = "Artista ${params.id} updated"
                redirect(action:show,id:artistaInstance.id)
            }
            else {
                render(view:'edit',model:[artistaInstance:artistaInstance])
            }
        }
        else {
            flash.message = "Artista not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def artistaInstance = new Artista()
        artistaInstance.properties = params
        return ['artistaInstance':artistaInstance]
    }

    def save = {
        def artistaInstance = new Artista(params)
        if(!artistaInstance.hasErrors() && artistaInstance.save()) {
            flash.message = "Artista ${artistaInstance.id} created"
            redirect(action:show,id:artistaInstance.id)
        }
        else {
            render(view:'create',model:[artistaInstance:artistaInstance])
        }
    }
}
