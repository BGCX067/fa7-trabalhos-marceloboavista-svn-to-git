

class MusicaController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ musicaInstanceList: Musica.list( params ), musicaInstanceTotal: Musica.count() ]
    }

    def search = {
        def c = Musica.createCriteria()
        def results = c.list {
            like("titulo", "%"+params.titulo+"%")
        }
        [musicList: results]
        
    }




    def search_results = {
        def c = Musica.createCriteria()
        def results = c.list {
            like("titulo", "%"+params.titulo+"%")
            like("compositor", "%"+params.compositor+"%")
            like("letra", "%"+params.letra+"%")
        }
        [musicList: results]
        //[ musicList: Musica.findAllByCompositorLike("%"+params.compositor+"%") ]
    }

    def show = {
        def musicaInstance = Musica.get( params.id )

        if(!musicaInstance) {
            flash.message = "Musica not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ musicaInstance : musicaInstance ] }
    }

    def delete = {
        def musicaInstance = Musica.get( params.id )
        if(musicaInstance) {
            try {
                musicaInstance.delete(flush:true)
                flash.message = "Música <strong>${musicaInstance.titulo}</strong> apagada com sucesso!"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Musica ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Musica not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def musicaInstance = Musica.get( params.id )

        if(!musicaInstance) {
            flash.message = "Musica not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ musicaInstance : musicaInstance ]
        }
    }

    def update = {
        def musicaInstance = Musica.get( params.id )
        if(musicaInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(musicaInstance.version > version) {
                    
                    musicaInstance.errors.rejectValue("version", "musica.optimistic.locking.failure", "Another user has updated this Musica while you were editing.")
                    render(view:'edit',model:[musicaInstance:musicaInstance])
                    return
                }
            }
            musicaInstance.properties = params
            if(!musicaInstance.hasErrors() && musicaInstance.save()) {
                flash.message = "Música <strong>${params.titulo}</strong> alterada com sucesso!"
                redirect(action:show,id:musicaInstance.id)
            }
            else {
                render(view:'edit',model:[musicaInstance:musicaInstance])
            }
        }
        else {
            flash.message = "Musica not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def musicaInstance = new Musica()
        musicaInstance.properties = params
        return ['musicaInstance':musicaInstance]
    }

    def save = {
        def musicaInstance = new Musica(params)

        //tratar arquivo transferido por upload
        def uploadedFile = request.getFile('musica')
        if(!uploadedFile.empty){
          //if(uploadedFile.contentType == "audio/mpeg"){
              /*
              println "Class: ${uploadedFile.class}"
              println "Name: ${uploadedFile.name}"
              println "OriginalFileName: ${uploadedFile.originalFilename}"
              println "Size: ${uploadedFile.size}"
              println "ContentType: ${uploadedFile.contentType}"
              */
              def webRootDir = servletContext.getRealPath("/")
              def userDir = new File(webRootDir, "/uploads/${musicaInstance.id}")
              userDir.mkdirs()
              uploadedFile.transferTo( new File( userDir, uploadedFile.originalFilename))
              musicaInstance.musica = uploadedFile.originalFilename

        }

        def musicas = Musica.findByTitulo(params.titulo)
        //println(musicas)
        
                
        if(uploadedFile.contentType != "image/jpeg"){

            if(musicas == null){

                if(!musicaInstance.hasErrors() && musicaInstance.save()){
                flash.message = "Música <strong>${musicaInstance.titulo}</strong> cadastrada com sucesso!"
                redirect(action:show,id:musicaInstance.id)
                }else{
                    render(view:'create',model:[musicaInstance:musicaInstance])
                }

            }else{
                flash.message = "Já existe uma musica com esse titulo"
                render(view:'create',model:[musicaInstance:musicaInstance])
            }

        }else{
            flash.message = "Arquivo Invalido"
            render(view:'create',model:[musicaInstance:musicaInstance])
        }
        


        /*
        if(!musicaInstance.hasErrors() && musicaInstance.save() && uploadedFile.contentType == "audio/mpeg") {
            flash.message = "Música <strong>${musicaInstance.titulo}</strong> cadastrada com sucesso!"
            redirect(action:show,id:musicaInstance.id)
        }
        else{
            flash.message = "Erro"
            render(view:'create',model:[musicaInstance:musicaInstance])
        }
        */


       
    }

}
