

class SetlistController {

    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ setlistInstanceList: Setlist.list( params ), setlistInstanceTotal: Setlist.count() ]
    }

    def show = {
        def setlistInstance = Setlist.get( params.id )

        if(!setlistInstance) {
            flash.message = "Setlist not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ setlistInstance : setlistInstance ] }
    }

    def delete = {
        def setlistInstance = Setlist.get( params.id )
        if(setlistInstance) {
            try {
                setlistInstance.delete(flush:true)
                flash.message = "Setlist ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Setlist ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Setlist not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def setlistInstance = Setlist.get( params.id )

        if(!setlistInstance) {
            flash.message = "Setlist not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ setlistInstance : setlistInstance ]
        }
    }

    def update = {
        def setlistInstance = Setlist.get( params.id )
        if(setlistInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(setlistInstance.version > version) {
                    setlistInstance.errors.rejectValue("version", "setlist.optimistic.locking.failure", "Another user has updated this Setlist while you were editing.")
                    render(view:'edit',model:[setlistInstance:setlistInstance])
                    return
                }
            }
            setlistInstance.properties = params
            if(!setlistInstance.hasErrors() && setlistInstance.save()) {
                flash.message = "Setlist ${params.id} updated"
                redirect(action:show,id:setlistInstance.id)
            }
            else {
                render(view:'edit',model:[setlistInstance:setlistInstance])
            }
        }
        else {
            flash.message = "Setlist not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def setlistInstance = new Setlist()
        setlistInstance.properties = params
        return ['setlistInstance':setlistInstance]
    }

    def avaliar = {

        def setlistInstance = Setlist.get( params.id )

        if(setlistInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(setlistInstance.version > version) {
                    setlistInstance.errors.rejectValue("version", "setlist.optimistic.locking.failure", "Another user has updated this Setlist while you were editing.")
                    render(view:'edit',model:[setlistInstance:setlistInstance])
                    return
                }
            }
            setlistInstance.properties = params
            if(!setlistInstance.hasErrors() && setlistInstance.save()) {
                flash.message = "Seu voto para a lista ${params.id} foi salvo com sucesso"
                redirect(action:show,id:setlistInstance.id)
            }
            else {
                render(view:'edit',model:[setlistInstance:setlistInstance])
            }
        }
        else {
            flash.message = "Setlist not found with id ${params.id}"
            redirect(action:list)
        }

    }

    def save = {
        def setlistInstance = new Setlist(params)
        if(!setlistInstance.hasErrors() && setlistInstance.save()) {
            flash.message = "Setlist ${setlistInstance.id} created"
            redirect(action:show,id:setlistInstance.id)
        }
        else {
            render(view:'create',model:[setlistInstance:setlistInstance])
        }
    }

    def ver_resultado = {
        def setlistInstance = new Setlist(params)
        def c = Setlist.createCriteria()
        def results = c.list {
            eq("id", (long) 5)
        }
        [ setlistInstanceList: results ]
    }

}
