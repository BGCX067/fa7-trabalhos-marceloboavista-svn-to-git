

class PapelController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ papelInstanceList: Papel.list( params ), papelInstanceTotal: Papel.count() ]
    }

    def show = {
        def papelInstance = Papel.get( params.id )

        if(!papelInstance) {
            flash.message = "Papel not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ papelInstance : papelInstance ] }
    }

    def delete = {
        def papelInstance = Papel.get( params.id )
        if(papelInstance) {
            try {
                papelInstance.delete(flush:true)
                flash.message = "Papel ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Papel ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Papel not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def papelInstance = Papel.get( params.id )

        if(!papelInstance) {
            flash.message = "Papel not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ papelInstance : papelInstance ]
        }
    }

    def update = {
        def papelInstance = Papel.get( params.id )
        if(papelInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(papelInstance.version > version) {
                    
                    papelInstance.errors.rejectValue("version", "papel.optimistic.locking.failure", "Another user has updated this Papel while you were editing.")
                    render(view:'edit',model:[papelInstance:papelInstance])
                    return
                }
            }
            papelInstance.properties = params
            if(!papelInstance.hasErrors() && papelInstance.save()) {
                flash.message = "Papel ${params.id} updated"
                redirect(action:show,id:papelInstance.id)
            }
            else {
                render(view:'edit',model:[papelInstance:papelInstance])
            }
        }
        else {
            flash.message = "Papel not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def papelInstance = new Papel()
        papelInstance.properties = params
        return ['papelInstance':papelInstance]
    }

    def save = {
        def papelInstance = new Papel(params)
        if(!papelInstance.hasErrors() && papelInstance.save()) {
            flash.message = "Papel ${papelInstance.id} created"
            redirect(action:show,id:papelInstance.id)
        }
        else {
            render(view:'create',model:[papelInstance:papelInstance])
        }
        
    }
}
