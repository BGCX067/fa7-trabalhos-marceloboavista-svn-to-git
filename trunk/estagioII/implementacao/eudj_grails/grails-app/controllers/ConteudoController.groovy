

class ConteudoController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ conteudoInstanceList: Conteudo.list( params ), conteudoInstanceTotal: Conteudo.count() ]
    }

    def show = {
        def conteudoInstance = Conteudo.get( params.id )

        if(!conteudoInstance) {
            flash.message = "Conteudo not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ conteudoInstance : conteudoInstance ] }
    }

    def delete = {
        def conteudoInstance = Conteudo.get( params.id )
        if(conteudoInstance) {
            try {
                conteudoInstance.delete(flush:true)
                flash.message = "Conteudo ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Conteudo ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Conteudo not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def conteudoInstance = Conteudo.get( params.id )

        if(!conteudoInstance) {
            flash.message = "Conteudo not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ conteudoInstance : conteudoInstance ]
        }
    }

    def update = {
        def conteudoInstance = Conteudo.get( params.id )
        if(conteudoInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(conteudoInstance.version > version) {
                    
                    conteudoInstance.errors.rejectValue("version", "conteudo.optimistic.locking.failure", "Another user has updated this Conteudo while you were editing.")
                    render(view:'edit',model:[conteudoInstance:conteudoInstance])
                    return
                }
            }
            conteudoInstance.properties = params
            if(!conteudoInstance.hasErrors() && conteudoInstance.save()) {
                flash.message = "Conteudo ${params.id} updated"
                redirect(action:show,id:conteudoInstance.id)
            }
            else {
                render(view:'edit',model:[conteudoInstance:conteudoInstance])
            }
        }
        else {
            flash.message = "Conteudo not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def conteudoInstance = new Conteudo()
        conteudoInstance.properties = params
        return ['conteudoInstance':conteudoInstance]
    }

    def save = {
        def conteudoInstance = new Conteudo(params)
        if(!conteudoInstance.hasErrors() && conteudoInstance.save()) {
            flash.message = "Conteudo ${conteudoInstance.id} created"
            redirect(action:show,id:conteudoInstance.id)
        }
        else {
            render(view:'create',model:[conteudoInstance:conteudoInstance])
        }
    }
	
	 
}
