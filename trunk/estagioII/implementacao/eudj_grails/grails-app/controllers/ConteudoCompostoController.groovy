

class ConteudoCompostoController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ conteudoCompostoInstanceList: ConteudoComposto.list( params ), conteudoCompostoInstanceTotal: ConteudoComposto.count() ]
    }

    def show = {
        def conteudoCompostoInstance = ConteudoComposto.get( params.id )

        if(!conteudoCompostoInstance) {
            flash.message = "ConteudoComposto not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ conteudoCompostoInstance : conteudoCompostoInstance ] }
    }

    def delete = {
        def conteudoCompostoInstance = ConteudoComposto.get( params.id )
        if(conteudoCompostoInstance) {
            try {
                conteudoCompostoInstance.delete(flush:true)
                flash.message = "ConteudoComposto ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "ConteudoComposto ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "ConteudoComposto not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def edit = {
        def conteudoCompostoInstance = ConteudoComposto.get( params.id )

        if(!conteudoCompostoInstance) {
            flash.message = "ConteudoComposto not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ conteudoCompostoInstance : conteudoCompostoInstance ]
        }
    }

    def update = {
        def conteudoCompostoInstance = ConteudoComposto.get( params.id )
        if(conteudoCompostoInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(conteudoCompostoInstance.version > version) {
                    
                    conteudoCompostoInstance.errors.rejectValue("version", "conteudoComposto.optimistic.locking.failure", "Another user has updated this ConteudoComposto while you were editing.")
                    render(view:'edit',model:[conteudoCompostoInstance:conteudoCompostoInstance])
                    return
                }
            }
            conteudoCompostoInstance.properties = params
            if(!conteudoCompostoInstance.hasErrors() && conteudoCompostoInstance.save()) {
                flash.message = "ConteudoComposto ${params.id} updated"
                redirect(action:show,id:conteudoCompostoInstance.id)
            }
            else {
                render(view:'edit',model:[conteudoCompostoInstance:conteudoCompostoInstance])
            }
        }
        else {
            flash.message = "ConteudoComposto not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def conteudoCompostoInstance = new ConteudoComposto()
        conteudoCompostoInstance.properties = params
        return ['conteudoCompostoInstance':conteudoCompostoInstance]
    }

    def save = {
        def conteudoCompostoInstance = new ConteudoComposto(params)
        if(!conteudoCompostoInstance.hasErrors() && conteudoCompostoInstance.save()) {
            flash.message = "ConteudoComposto ${conteudoCompostoInstance.id} created"
            redirect(action:show,id:conteudoCompostoInstance.id)
        }
        else {
            render(view:'create',model:[conteudoCompostoInstance:conteudoCompostoInstance])
        }
    }
}
