

class ComentarioController {
    
    def index = { redirect(action:list,params:params) }

    // the delete, save and update actions only accept POST requests
    static allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        params.max = Math.min( params.max ? params.max.toInteger() : 10,  100)
        [ comentarioInstanceList: Comentario.list( params ), comentarioInstanceTotal: Comentario.count() ]
    }

    def show = {
        def comentarioInstance = Comentario.get( params.id )

        if(!comentarioInstance) {
            flash.message = "Comentario not found with id ${params.id}"
            redirect(action:list)
        }
        else { return [ comentarioInstance : comentarioInstance ] }
    }

    def delete = {
        def comentarioInstance = Comentario.get( params.id )
        if(comentarioInstance) {
            try {
                comentarioInstance.delete(flush:true)
                flash.message = "Comentario ${params.id} deleted"
                redirect(action:list)
            }
            catch(org.springframework.dao.DataIntegrityViolationException e) {
                flash.message = "Comentario ${params.id} could not be deleted"
                redirect(action:show,id:params.id)
            }
        }
        else {
            flash.message = "Comentario not found with id ${params.id}"
            redirect(action:list)
        }
    }

	def deleteComentario = {
	  println("EEEE!")
		def comentarioExcluido = Comentario.get(params.id)
		
		println(comentarioExcluido)
	  	println(comentarioExcluido.conteudo.id)

		try {
			
			if(comentarioExcluido) {
        			comentarioExcluido.delete(flush:true)
				flash.message = "Comentario excluido com sucesso!"
				println("OOO!")
				redirect(action:"show",controller:"conteudo",id:comentarioExcluido.conteudo.id)


			} else {
		println("OOO!")
       	flash.message = "Erro!!!"
       	redirect(action:"list",controller:"cd")
			}
		}
		catch(org.springframework.dao.DataIntegrityViolationException e) {
      	flash.message = "AAA"
		println("OOO!")

    }
  }

    def edit = {
        def comentarioInstance = Comentario.get( params.id )

        if(!comentarioInstance) {
            flash.message = "Comentario not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ comentarioInstance : comentarioInstance ]
        }
    }

    def update = {
        def comentarioInstance = Comentario.get( params.id )
        if(comentarioInstance) {
            if(params.version) {
                def version = params.version.toLong()
                if(comentarioInstance.version > version) {
                    
                    comentarioInstance.errors.rejectValue("version", "comentario.optimistic.locking.failure", "Another user has updated this Comentario while you were editing.")
                    render(view:'edit',model:[comentarioInstance:comentarioInstance])
                    return
                }
            }
            comentarioInstance.properties = params
            if(!comentarioInstance.hasErrors() && comentarioInstance.save()) {
                flash.message = "Comentario ${params.id} updated"
                redirect(action:show,id:comentarioInstance.id)
            }
            else {
                render(view:'edit',model:[comentarioInstance:comentarioInstance])
            }
        }
        else {
            flash.message = "Comentario not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def create = {
        def comentarioInstance = new Comentario()
        comentarioInstance.properties = params
        return ['comentarioInstance':comentarioInstance]
    }

    def save = {
        def comentarioInstance = new Comentario(params)
        if(!comentarioInstance.hasErrors() && comentarioInstance.save()) {
            flash.message = "Comentario ${comentarioInstance.id} created"
            redirect(action:show,id:comentarioInstance.id)
        }
        else {
            render(view:'create',model:[comentarioInstance:comentarioInstance])
        }
    }

   def saveComentario = {
        def comentarioInstance = new Comentario(params)
	  	
        if(!comentarioInstance.hasErrors() && comentarioInstance.save()) {
            flash.message = "Comentario incluido com sucesso"
            redirect(action:"show",controller:"conteudo",id:comentarioInstance.conteudo.id)
        }
        else {
            //render(view:'create',model:[comentarioInstance:comentarioInstance])
            redirect(action:"show",controller:"conteudo",id:comentarioInstance.conteudo.id)
            flash.message = "Campo COMENTARIO não pode esta em branco"
        }
    }
	
}
