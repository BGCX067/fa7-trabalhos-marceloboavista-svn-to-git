class Usuario {

    String nome

    static hasMany = [seguidores:Usuario]

    static constraints = {
    }

    String toString(){
        return nome
    }
}
