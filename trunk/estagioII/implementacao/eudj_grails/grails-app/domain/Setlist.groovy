class Setlist {
    
    static hasMany = [conteudoComposto:ConteudoComposto]

    String nome

    Integer nota1 = 0

    Integer nota2 = 0

    Integer nota3 = 0

    Integer nota4 = 0

    Integer nota5 = 0
    
    static constraints = {
        nota1(nullable:true)
        nota2(nullable:true)
        nota3(nullable:true)
        nota4(nullable:true)
        nota5(nullable:true)
    }
}
