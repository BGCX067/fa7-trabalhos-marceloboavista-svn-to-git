

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Edit ConteudoComposto</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">ConteudoComposto List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New ConteudoComposto</g:link></span>
        </div>
        <div class="body">
            <h1>Edit ConteudoComposto</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${conteudoCompostoInstance}">
            <div class="errors">
                <g:renderErrors bean="${conteudoCompostoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post" >
                <input type="hidden" name="id" value="${conteudoCompostoInstance?.id}" />
                <input type="hidden" name="version" value="${conteudoCompostoInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="album">Album:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:conteudoCompostoInstance,field:'album','errors')}">
                                    <g:select name="album"
from="${Album.list()}"
size="5" multiple="yes" optionKey="id"
value="${conteudoCompostoInstance?.album}" />

                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="artista">Artista:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:conteudoCompostoInstance,field:'artista','errors')}">
                                    <g:select name="artista"
from="${Artista.list()}"
size="5" multiple="yes" optionKey="id"
value="${conteudoCompostoInstance?.artista}" />

                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="conteudo">Conteudo:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:conteudoCompostoInstance,field:'conteudo','errors')}">
                                    <g:select name="conteudo"
from="${Conteudo.list()}"
size="5" multiple="yes" optionKey="id"
value="${conteudoCompostoInstance?.conteudo}" />

                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
