<html>
  <head>
    <title><g:layoutTitle default="Grails" /></title>
    <link rel="stylesheet" href="${resource(dir:'css',file:'main.css')}" />
    <link rel="shortcut icon" href="${resource(dir:'images',file:'favicon.ico')}" type="image/x-icon" />
    <g:layoutHead />
    <g:javascript library="application" />
  </head>
  <body>
    <div id="total">
      <div id="spinner" class="spinner" style="display:none;">
        <img src="${resource(dir:'images',file:'spinner.gif')}" alt="Spinner" />
      </div>
      <div id="topo">
        <div id="navLogin">
          <g:render template="/common/navLogin" />
        </div>
        <div id="navPrimaria">
          <g:render template="/common/navPrimaria" />
        </div>
      </div>
      <div id="usuario_resumo">
        <img src="${resource(dir:'images',file:'avatar.jpg')}"  />
        <h2>Nome do Usuário</h2>
        <p>Ouvir DJ</p>
        <p>120 Músicas</p>
        <p>13 Setlists</p>
        <p>400 Ouvintes</p>
      </div>
      <div id="conteudo_principal"
           <g:layoutBody />
      </div>
    </div>
  </body>
</html>