

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Comentario List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Comentario</g:link></span>
        </div>
        <div class="body">
            <h1>Comentario List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <th>Conteudo</th>
                   	    
                   	        <g:sortableColumn property="descricao" title="Descricao" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${comentarioInstanceList}" status="i" var="comentarioInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${comentarioInstance.id}">${fieldValue(bean:comentarioInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:comentarioInstance, field:'conteudo')}</td>
                        
                            <td>${fieldValue(bean:comentarioInstance, field:'descricao')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${comentarioInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
