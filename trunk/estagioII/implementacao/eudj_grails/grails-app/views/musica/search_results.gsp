<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main" />
    <title>Musica List</title>
  </head>
  <body>
    <div class="nav">
      <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
      <span class="menuButton"><g:link class="create" action="create">New Musica</g:link></span>
      <span class="menuButton"><g:link class="list" action="list">Musica List</g:link></span>
      <span class="menuButton"><g:link class="list" action="search">Search Musica</g:link></span>
    </div>
    <div class="body">
      <h1>Musica List</h1>
      <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
      </g:if>
      <g:if test="${!musicList}">
        <div class="message">A busca não retornou resultados.</div>
      </g:if>
      <div class="list">
        <table>
          <thead>
            <tr>
              <g:sortableColumn property="id" title="Id" />
              <g:sortableColumn property="titulo" title="Titulo" />
              <g:sortableColumn property="compositor" title="Compositor" />
              <g:sortableColumn property="letra" title="Letra" />
            </tr>
          </thead>
          <tbody>
            <g:each in="${musicList}" status="i" var="musicaInstance">
              <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                <td><g:link action="show" id="${musicaInstance.id}">${musicaInstance.id}</g:link></td>
                <td>${musicaInstance.titulo}</td>
                <td>${musicaInstance.compositor}</td>
                <td>${musicaInstance.letra}</td>
              </tr>
            </g:each>
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>
