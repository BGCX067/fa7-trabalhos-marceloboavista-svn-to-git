

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Músicas Enviadas</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">Enviar Música</g:link></span>
            <span class="menuButton"><g:link class="list" action="search">Procurar Música</g:link></span>
        </div>
        <div class="body">
            <br/>
            <h1>Músicas Enviadas</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <!--<g:sortableColumn property="id" title="Id" />-->

                            <g:sortableColumn property="titulo" title="Titulo" />
                        
                   	        <g:sortableColumn property="compositor" title="Compositor" />
                        
                   	        <g:sortableColumn property="letra" title="Letra" />

                            <g:sortableColumn property="musica" title="Música" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${musicaInstanceList}" status="i" var="musicaInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <!--<td><g:link action="show" id="${musicaInstance.id}">${fieldValue(bean:musicaInstance, field:'id')}</g:link></td>-->

                            <td><g:link action="show" id="${musicaInstance.id}">${fieldValue(bean:musicaInstance, field:'titulo')}</g:link></td>

                            <td>${fieldValue(bean:musicaInstance, field:'compositor')}</td>
                        
                            <td>${fieldValue(bean:musicaInstance, field:'letra')}</td>

                            <td>${fieldValue(bean:musicaInstance, field:'musica')}</td>

                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${musicaInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
