

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Editar Música</title>

        <script type="text/javascript">
          function editarMusica(){
            document.getElementById('musicae').style.display = 'block';
            antigo = document.getElementById('musicae');
            antigo.id = "musica";
            antigo.name = "musica";
            //alert(antigo.id);
          }
        </script>

    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Musica List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Musica</g:link></span>
        </div>
        <div class="body">
            <br/>
            <h1>Editar Música</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${musicaInstance}">
            <div class="errors">
                <g:renderErrors bean="${musicaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form method="post">
                <input type="hidden" name="id" value="${musicaInstance?.id}" />
                <input type="hidden" name="version" value="${musicaInstance?.version}" />
                <div class="dialog">
                    <table>
                        <tbody>

                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="titulo">Titulo:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:musicaInstance,field:'titulo','errors')}">
                                    <input type="text" id="titulo" name="titulo" value="${fieldValue(bean:musicaInstance,field:'titulo')}"/>
                                </td>
                            </tr>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="compositor">Compositor:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:musicaInstance,field:'compositor','errors')}">
                                    <input type="text" id="compositor" name="compositor" value="${fieldValue(bean:musicaInstance,field:'compositor')}"/>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="letra">Letra:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:musicaInstance,field:'letra','errors')}">
                                    <g:textArea name="letra" id="letra" value="myValue" rows="5" cols="40" value="${fieldValue(bean:musicaInstance,field:'letra')}" />
                                </td>
                            </tr>

                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="musicas">Música:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:musicaInstance,field:'filename','errors')}">
                                    <input type="text" id="musicas" name="musicas" value="${fieldValue(bean:musicaInstance,field:'musica')}"
                                    readonly="readonly"/>
                                    <input type="button" value="Editar" onclick="editarMusica();">
                                </td>
                            </tr>

                            <tr class="prop">
                                <td valign="top" class="name">
                                  
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:musicaInstance,field:'musica','errors')}">
                                    <input type="file" id="musicae" name="musicae" style="display:none;"/>
                                </td>
                            </tr>
                            
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
