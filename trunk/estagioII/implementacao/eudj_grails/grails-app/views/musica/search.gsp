

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Search Musica</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Musica List</g:link></span>
        </div>
        <div class="body">
            <h1>Search Musica</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${musicaInstance}">
            <div class="errors">
                <g:renderErrors bean="${musicaInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="search_results" method="post">
                <div class="dialog">
                    <table>
                        <tbody>
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="titulo">Titulo:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:musicaInstance,field:'titulo','errors')}">
                                    <input type="text" id="titulo" name="titulo" value="${musicaInstance?.titulo}"/>
                                </td>
                            </tr>

                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="compositor">Compositor:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:musicaInstance,field:'compositor','errors')}">
                                    <input type="text" id="compositor" name="compositor" value="${fieldValue(bean:musicaInstance,field:'compositor')}"/>
                                </td>
                            </tr>

                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="letra">Letra:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:musicaInstance,field:'letra','errors')}">
                                    <g:textArea name="letra" id="letra" value="myValue" rows="5" cols="40" value="${fieldValue(bean:musicaInstance,field:'letra')}" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Search" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
