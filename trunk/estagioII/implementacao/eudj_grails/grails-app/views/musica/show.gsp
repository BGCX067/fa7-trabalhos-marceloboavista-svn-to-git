

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Música Enviada</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Músicas Enviadas</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">Enviar Música</g:link></span>
            <span class="menuButton"><g:link class="search" action="search">Buscar Música</g:link></span>
        </div>
        <div class="body">
            <br/>
            <h1>Música Enviada</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <!--<tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:musicaInstance, field:'id')}</td>
                            
                        </tr>-->
                    
                        <tr class="prop">
                            <td valign="top" class="name">Compositor:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:musicaInstance, field:'compositor')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Letra:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:musicaInstance, field:'letra')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Titulo:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:musicaInstance, field:'titulo')}</td>
                            
                        </tr>

                        <tr class="prop">
                            <td valign="top" class="name">Música:</td>

                            <td valign="top" class="value">${fieldValue(bean:musicaInstance, field:'musica')}</td>

                        </tr>
                    
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${musicaInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Tem certeza que deseja apagar esta música?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
