

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Setlist List</title>

        <!--// documentation resources //-->
        <script src='../js/jquery.js' type="text/javascript"></script>
        <script src='../js/documentation.js' type="text/javascript"></script>

        <!--// plugin-specific resources //-->
        <script src='../js/jquery.MetaData.js' type="text/javascript" language="javascript"></script>
        <script src='../js/jquery.rating.js' type="text/javascript" language="javascript"></script>

        <link href='../css/jquery.rating.css' type="text/css" rel="stylesheet"/>

        <script>

            var updateNota = function( nota ){
                var notaAtualizada = document.getElementById( "nota" + nota ).value++;
                notaAtualizada++;
                document.getElementById( "nota" + nota ).value = notaAtualizada;
            }

        </script>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Setlist</g:link></span>
        </div>
        <div class="body">
            <h1>Setlist List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                          <g:sortableColumn property="id" title="Id" />
                          <g:sortableColumn property="id" title="Nome" />
                           <g:sortableColumn property="id" title="Nota" />
                          <g:sortableColumn property="id" title="Ação" />
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${setlistInstanceList}" status="i" var="setlistInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">


                            <td><g:link action="show" id="${setlistInstance.id}">${fieldValue(bean:setlistInstance, field:'id')}</g:link></td>

                            <td>
                              ${fieldValue(bean:setlistInstance, field:'nome')}
                            </td>

                            <td>
                                <input name="nota${i}" type="radio" class="star" value="${fieldValue(bean:setlistInstance, field:'nota1')}" />
                                <input name="nota${i}" type="radio" class="star" value="${fieldValue(bean:setlistInstance, field:'nota2')}" />
                                <input name="nota${i}" type="radio" class="star" value="${fieldValue(bean:setlistInstance, field:'nota3')}" />
                                <input name="nota${i}" type="radio" class="star" value="${fieldValue(bean:setlistInstance, field:'nota4')}" />
                                <input name="nota${i}" type="radio" class="star" value="${fieldValue(bean:setlistInstance, field:'nota5')}" />
                            </td>

                            <td style="width: 150">
                                <div style="float: left; margin-right: 10px;">
                               <g:form action="avaliar">
                                    <input type="hidden" name="id" value="${setlistInstance?.id}" />

                                    <input type="hidden" name="nota1" id="nota1" value="${setlistInstance.nota1}" />
                                    <input type="hidden" name="nota2" id="nota2" value="${setlistInstance.nota2}" />
                                    <input type="hidden" name="nota3" id="nota3" value="${setlistInstance.nota3}" />
                                    <input type="hidden" name="nota4" id="nota4" value="${setlistInstance.nota4}" />
                                    <input type="hidden" name="nota5" id="nota5" value="${setlistInstance.nota5}" />

                                    <span class="button"><g:actionSubmit class="avaliar" value="Avaliar" /></span>
                                </g:form>
                              </div>
                                <div style="float: left;">
                                 <g:form action="ver_resultado" method="post">
                                    <g:hiddenField name="id" value="${setlistInstance?.id}" />
                                    <span class="button"><input class="save" type="submit" value="Resultado" /></span>
                                </g:form>
                              </div>
                            </td>

                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${setlistInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
