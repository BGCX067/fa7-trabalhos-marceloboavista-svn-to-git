<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Resultado Avaliação</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Setlist List</g:link></span>
        </div>
        <div class="body">
            <h1>Resultado Avaliação</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${setlistInstance}">
            <div class="errors">
                <g:renderErrors bean="${setlistInstance}" as="list" />
            </div>
            </g:hasErrors>
            <div class="list">
        <table>
          <thead>
            <tr>
              <g:sortableColumn property="id" title="Id" />
              <g:sortableColumn property="nota1" title="Nota 1" />
               <g:sortableColumn property="nota2" title="Nota 2" />
               <g:sortableColumn property="nota3" title="Nota 3" />
               <g:sortableColumn property="nota4" title="Nota 4" />
               <g:sortableColumn property="nota5" title="Nota 5" />
               <g:sortableColumn property="id" title="Quantidade Total de Votos" />
            </tr>
          </thead>
          <tbody>
            <g:each in="${setlistInstanceList}" status="i" var="setlistInstance">
              <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                <td><g:link action="show" id="${setlistInstance.id}">${setlistInstance.id}</g:link></td>
                <td>${setlistInstance.nota1}</td>
               <td>${setlistInstance.nota2}</td>
              <td>${setlistInstance.nota3}</td>
              <td>${setlistInstance.nota4}</td>
              <td>${setlistInstance.nota5}</td>
              <td>${setlistInstance.nota1 + setlistInstance.nota2 + setlistInstance.nota3 + setlistInstance.nota4 + setlistInstance.nota5}</td>
              </tr>
            </g:each>
          </tbody>
        </table>
      </div>
        </div>
    </body>
</html>
