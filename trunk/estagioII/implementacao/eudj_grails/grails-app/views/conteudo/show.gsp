<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
		<title>Show Conteudo</title>
    </head>
	
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Conteudo List</g:link></span>
            <span class="menuButton"><g:link class="create" action="create">New Conteudo</g:link></span>
        </div>
        <div class="body">
            <h1>Show Conteudo</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="dialog">
                <table>
                    <tbody>

                    
                        <tr class="prop">
                            <td valign="top" class="name">Id:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:conteudoInstance, field:'id')}</td>
                            
                        </tr>
                    
                        
                    
                        <tr class="prop">
                            <td valign="top" class="name">Data De Criacao:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:conteudoInstance, field:'dataDeCriacao')}</td>
                            
                        </tr>
                    
                        <tr class="prop">
                            <td valign="top" class="name">Nome:</td>
                            
                            <td valign="top" class="value">${fieldValue(bean:conteudoInstance, field:'nome')}</td>
                            
                        </tr>
                    
<br/><br/>
                        <tr class="prop">
                          <td valign="top" class="name"><strong>Comentários</strong></td>
                        </tr>
    				<tr class="prop">
				    <td  valign="top" style="text-align:left;" class="value">
				    <ul>
				     <g:each var="c" in="${conteudoInstance.comentarios}">

                                     <div align="right">${c.data}</div>
                                     <div align="left">${c.descricao}</div>
				    			  <br/>
                              <g:form name="formConteudo" url="[controller:'comentario',action:'deleteComentario']">
							
							<input type="hidden" name="id" value="${c?.id}" />
							<input type="submit" value="Deletar!" onclick="return confirm('Deseja realmente excluir o comentario?');" />
				      </g:form>   
                                		
                             </g:each>
					</ul>
					</td>
                       </tr>
				<g:form  controller="comentario" action="saveComentario" method="post" >
				
			     <tr class="prop">
					<td valign="top" class="value ${hasErrors(bean:comentarioInstance,field:'descricao','errors')}">
                           	         <textarea rows="2" cols="10" id="descricao" name="descricao" value="${fieldValue(bean:comentarioInstance,field:'descricao')}">
							</textarea>
                           	</td>
				</tr>
				<tr class="prop">
					<td valign="top" class="name">

						<input type="hidden" name="conteudo.id" value="${fieldValue(bean:conteudoInstance, field:'id')}"/>
						<input type="submit" value="Comentar!" />
					</td>
				</tr>
				</g:form>
				
                    </tbody>
                </table>
            </div>
            <div class="buttons">
                <g:form>
                    <input type="hidden" name="id" value="${conteudoInstance?.id}" />
                    <span class="button"><g:actionSubmit class="edit" value="Edit" /></span>
                    <span class="button"><g:actionSubmit class="delete" onclick="return confirm('Are you sure?');" value="Delete" /></span>
                </g:form>
            </div>
        </div>
    </body>
</html>
