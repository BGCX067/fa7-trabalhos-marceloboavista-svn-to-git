

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Conteudo List</title>
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="create" action="create">New Conteudo</g:link></span>
        </div>
        <div class="body">
            <h1>Conteudo List</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <div class="list">
                <table>
                    <thead>
                        <tr>
                        
                   	        <g:sortableColumn property="id" title="Id" />
                        
                   	        <g:sortableColumn property="dataDeCriacao" title="Data De Criacao" />
                        
                   	        <g:sortableColumn property="nome" title="Nome" />
                        
                        </tr>
                    </thead>
                    <tbody>
                    <g:each in="${conteudoInstanceList}" status="i" var="conteudoInstance">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                        
                            <td><g:link action="show" id="${conteudoInstance.id}">${fieldValue(bean:conteudoInstance, field:'id')}</g:link></td>
                        
                            <td>${fieldValue(bean:conteudoInstance, field:'dataDeCriacao')}</td>
                        
                            <td>${fieldValue(bean:conteudoInstance, field:'nome')}</td>
                        
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
            <div class="paginateButtons">
                <g:paginate total="${conteudoInstanceTotal}" />
            </div>
        </div>
    </body>
</html>
