

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="layout" content="main" />
        <title>Create Conteudo</title>         
    </head>
    <body>
        <div class="nav">
            <span class="menuButton"><a class="home" href="${resource(dir:'')}">Home</a></span>
            <span class="menuButton"><g:link class="list" action="list">Conteudo List</g:link></span>
        </div>
        <div class="body">
            <h1>Create Conteudo</h1>
            <g:if test="${flash.message}">
            <div class="message">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${conteudoInstance}">
            <div class="errors">
                <g:renderErrors bean="${conteudoInstance}" as="list" />
            </div>
            </g:hasErrors>
            <g:form action="save" method="post" >
                <div class="dialog">
                    <table>
                        <tbody>
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="dataDeCriacao">Data De Criacao:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:conteudoInstance,field:'dataDeCriacao','errors')}">
                                    <g:datePicker name="dataDeCriacao" value="${conteudoInstance?.dataDeCriacao}" precision="minute" ></g:datePicker>
                                </td>
                            </tr> 
                        
                            <tr class="prop">
                                <td valign="top" class="name">
                                    <label for="nome">Nome:</label>
                                </td>
                                <td valign="top" class="value ${hasErrors(bean:conteudoInstance,field:'nome','errors')}">
                                    <input type="text" id="nome" name="nome" value="${fieldValue(bean:conteudoInstance,field:'nome')}"/>
                                </td>
                            </tr> 
                        
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><input class="save" type="submit" value="Create" /></span>
                </div>
            </g:form>
        </div>
    </body>
</html>
