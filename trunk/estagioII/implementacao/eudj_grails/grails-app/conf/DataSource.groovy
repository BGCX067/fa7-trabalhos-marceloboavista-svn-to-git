/*
 *  # Faculadade 7 de Setembro
 *    - Estagio III - 2009.2
 *    - Professor: Eduardo Mendes
 *
 *
 *  DataSource da Aplica��o
 *
 *
 */

dataSource {
	pooled = true
	// Nome do Driver do Mysql
	driverClassName = "com.mysql.jdbc.Driver"
        // Dadso de Conex�o com a base de dados
        username = "root"
	password = ""
}
hibernate {
    cache.use_second_level_cache=true
    cache.use_query_cache=true
    cache.provider_class='com.opensymphony.oscache.hibernate.OSCacheProvider'
}
// environment specific settings
environments {
	development {
		dataSource {
			/*Na primeira vez que rodar, ele vai criar a tabela.
			Nas proximas recomendo colocar "update", para que a tabela
			n?o seja recriada */
			dbCreate = "update" // one of 'create', 'create-drop','update'
			url = "jdbc:mysql://localhost/eudj"
		}
	}
	test {
		dataSource {
			dbCreate = "update"
			url = "jdbc:mysql://localhost/eudj"
		}
	}
	production {
		dataSource {
			dbCreate = "update"
			url = "jdbc:mysql://localhost/eudj"
		}
	}
}