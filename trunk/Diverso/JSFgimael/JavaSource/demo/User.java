/*******************************************************************************
 * Copyright (c) 2007 Exadel, Inc. and Red Hat, Inc.
 * Distributed under license by Red Hat, Inc. All rights reserved.
 * This program is made available under the terms of the
 * Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Exadel, Inc. and Red Hat, Inc. - initial API and implementation
 ******************************************************************************/ 
package demo;

/**
 * Created by JBoss Developer Studio
 */
public class User {

	private static boolean getName ;

	private String name;
	
	private String sobreNome;

	/**
	 * @return User Name
	 */
	public  String getName() {
		return name;
	}

	/**
	 * @return the sobreNome
	 */
	public String getSobreNome() {
		return sobreNome;
	}

	/**
	 * @param sobreNome the sobreNome to set
	 */
	public void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}

	/**
	 * @param User Name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	public String validoString() {
		if (getName().equals("Gimael") ) {
			return "hello";	
		} else {
			return "naoHello";
		
	}
	}
}