<%@ taglib uri="http://java.sun.com/jsf/core" prefix="f"%>
<%@ taglib uri="http://java.sun.com/jsf/html" prefix="h"%>

<f:loadBundle var="Message" basename="demo.Messages" />

<html>
	<head>
		<title>Voc� n�o tem acesso a esta aplica��o!</title>
	</head>

	<body>
		<f:view>
			<h3>
				<h:outputText value="Sem permiss�o" />,
			</h3>
		</f:view>
	</body>

</html>