package br.edu.fa7.gerpro7v3.amarildo;

import java.util.Arrays;

public class Vetor {
	
	/**
	   * Atributo de armazenamento do vetor
	*/
	
	private int[] vetorInteger = null;
	
			
	public Vetor(int[] vetorInteger) {
		this.vetorInteger = vetorInteger;
	}
	public int soma() {
		
		  int soma = 0;
		  for (int i = 0; i < vetorInteger.length; i++) {
			  soma += vetorInteger[i];
			  
		  }
		  return soma;
	}
	
	public int maior() {
		
		int maior = vetorInteger[0];
		  
		for (int i = 0; i < vetorInteger.length; i++) {
			  
			  if (vetorInteger[i] > maior) {
				  maior = vetorInteger[i];
			  }
		   } 	  return maior;
		
	}
	public int menor() {
		
		  int menor = vetorInteger[0];
		  
		  for (int i = 0; i < vetorInteger.length; i++) {
			  
			  if (vetorInteger[i] < menor) {
				  menor = vetorInteger[i];
			  }
			  
		  }
		  return menor;
	}
	
	/**
	 *  Ordenando o vetor usando Metodo BOLHA
	 * 
	 */
	

	public Vetor getNovoVetorOrdenado (){
		
	    int indice1, indice2; 
	    int[] vetorInteger = this.vetorInteger.clone();
	    
	    for(indice1 = 0; indice1 < vetorInteger.length-2; indice1++){
	    	
	            for(indice2 = 0; indice2 < vetorInteger.length-1;indice2++){ 
	            	
	                if(vetorInteger[indice2] > vetorInteger[indice2+1]){
	                    int campoAux = vetorInteger[indice2]; 
	                    vetorInteger[indice2] = vetorInteger[indice2+1];
	                    vetorInteger[indice2+1] = campoAux;
	                }
	            }
	        }
//	        for(indice1 = 0; indice1 < 8; indice1++){
//	            System.out.print(vetorInteger[indice1]+" , ");
//	        }
	    Vetor vetorOrdenado = new Vetor(vetorInteger);
	    return(vetorOrdenado);
	    
    
	}

		
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(vetorInteger);
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vetor))
			return false;
		Vetor other = (Vetor) obj;
		if (!Arrays.equals(vetorInteger, other.vetorInteger))
			return false;
		return true;
	}
	public static void main(String[] args) {
		
		int[] vInteger = {1, 2, 10, - 20, 30, 40, 50, -1};
		Vetor vetor = new Vetor(vInteger);
		
		System.out.println("A soma do vetor � : "+vetor.soma());
		
		System.out.println("O Maior numero �  : "+vetor.maior());
		
		try {
			System.out.println("O Menor numero �  : "+vetor.menor());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
							
		System.out.println("O vetor ordenado pelo metodo Bolha");
		System.out.println(vetor.getNovoVetorOrdenado());
		
		System.out.println(" ");
		
		System.out.println("Raiz quadrada da Soma");
		
		double raizSoma = Math.sqrt(vetor.soma());
		
		System.out.println(raizSoma);
		
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return Arrays.toString(this.vetorInteger);
	}
	
	}
    