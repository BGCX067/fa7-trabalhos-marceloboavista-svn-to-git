package br.edu.fa7.gerpro7v3.junioroliveira;
import java.lang.Math;
import java.util.Arrays;

import sun.beans.editors.IntEditor;
/*
 * @author gerpro7
 */
public class Vetor {
	/*
	 * Atributo que armazena vetor de inteiros
	 */
	private int[] vetorInteger = null;
		
	
	public Vetor(int[] vetorInteger) {
		this.vetorInteger = vetorInteger;
	}
	
	
	public Vetor() {
		super();
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(vetorInteger);
		return result;
	}





	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vetor))
			return false;
		Vetor other = (Vetor) obj;
		if (!Arrays.equals(vetorInteger, other.vetorInteger))
			return false;
		return true;
	}





	public int soma(){
		int soma = 0;
		for(int i=0; i < vetorInteger.length; i++){
			soma += vetorInteger[i];
		}
		return soma;
	}
	
	public int maior(){
		int maior = 0;
		for(int i=0; i< vetorInteger.length; i++){
			if(vetorInteger[i] > maior){
				maior = vetorInteger[i];
			}
		}
		return maior;
	}
	
	public int menor(){
		int menor = 0;
		for (int i=0; i< vetorInteger.length; i++){
			if(vetorInteger[i] < menor ){
				menor = vetorInteger[i];
			}
		}
		return menor;
	}
	
	public double RaizSomaVetor(){
		double raiz;
		raiz = Math.sqrt(soma());
		int raizInt = (int)raiz;
		return raizInt;
		
	}
	
	/*public int[] bolhaComFlag(int[] vetor) {
		int n = vetor.length;
		boolean flag = true;
		for (int i = 1; i < n - 1; i++) {
			if (flag) {
				flag = false;
				for (int j = 0; j < n - i; j++) {
					if (vetor[j] > vetor[j + 1]) {
						int aux = vetor[j];
						vetor[j] = vetor[j + 1];
						vetor[j + 1] = aux;
						flag = true;
					}
				}
			}
		}
		return vetor;
	}*/
	
	
	
	public Vetor bolhaComFlag() {
		int[] vetorInteger = this.vetorInteger.clone();
		int n = vetorInteger.length;
		boolean flag = true;
		for (int i = 1; i < n - 1; i++) {
			if (flag) {
				flag = false;
				for (int j = 0; j < n - i; j++) {
					if (vetorInteger[j] > vetorInteger[j + 1]) {
						int aux = vetorInteger[j];
						vetorInteger[j] = vetorInteger[j + 1];
						vetorInteger[j + 1] = aux;
						flag = true;
					}
				}
			}
		}
		Vetor vetorOrdenado = new Vetor(vetorInteger);
		return vetorOrdenado;
	}
	
	public static void getNovoVetorOrdenado(int[] vetor){
		for(int i=0; i< vetor.length; i++){
			System.out.print(vetor[i]+ ", ");
		}
	}
	

	
	@Override
	public String toString() {
		
		return  Arrays.toString(this.vetorInteger);
	}


	public static void main(String[] args) {
		int[] vInteger = { 1, 2, 10, -20, 30, 40, 50, -1 };
		Vetor vetor = new Vetor(vInteger);
		System.out.print("Vetor original ");
		vetor.getNovoVetorOrdenado(vInteger);
		System.out.println();
		System.out.println("a soma do vetor �: " + vetor.soma());
		System.out.println("o maior elemento �: " + vetor.maior());
		System.out.println("o menor elemento �: " + vetor.menor());
		System.out.println("A raiz da soma do vetor �: " + vetor.RaizSomaVetor());
		vetor.bolhaComFlag();
		
		System.out.println();
		System.out.println("Vetor ordenado "+ vetor.bolhaComFlag());
		
	}
}
