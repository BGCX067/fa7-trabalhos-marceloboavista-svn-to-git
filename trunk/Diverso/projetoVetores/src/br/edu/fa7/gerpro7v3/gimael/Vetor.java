package br.edu.fa7.gerpro7v3.gimael;

import java.util.Arrays;


public class Vetor {

	// Atributo que armazena um vetor de inteiros.
	private int[] vetorInteger = { 1, 2, 10, -20, 30, 40, 50, -1 };

	// Construtor que recebe o vetor como argumento.
	public Vetor(int[] vetorInteger) {
		this.vetorInteger = vetorInteger;
	}

	// Construtor sem par�metros.
	public Vetor() {
	}

	// M�todo que faz a soma do vetor.
	public int soma() {
		int soma = 0;
		for (int i = 0; i < vetorInteger.length; i++) {
			soma += vetorInteger[i];
		}
		return soma;
	}

	// M�todo que retorna o maior elemento do vetor sem tratar exce��es.(o vetor
	// existe? Vetor.length=0).
	public int maiorElemento() {

		int maior = vetorInteger[0];

		for (int i = 0; i < vetorInteger.length - 1; i++) {
			if (maior < vetorInteger[i + 1]) {
				maior = vetorInteger[i + 1];
				i += 1;
			}
		}

		return maior;
	}

	// M�todo que retorna o menor elemento do vetor.
	public int menorElemento() {

		int menor = vetorInteger[0];

		for (int i = 0; i < vetorInteger.length - 1; i++) {
			if (menor > vetorInteger[i + 1]) {
				menor = vetorInteger[i + 1];
				i += 1;
			}
		}

		return menor;
	}

	public double raizQuadrada() {

		double rq = this.soma();
		rq = Math.sqrt(rq);

		return rq;
	}

	// M�todo que retorna o um novo vetor ordenado. O seu tamanho e valores s�o
	// aleat�rios.
	public Vetor vetorOrdenado() {

		int[] vet = new int[this.vetorInteger.length];
		for (int j = 0; j < this.vetorInteger.length; j++) {
			vet[j] = vetorInteger[j];
		}
		

		// m�todo de ordena��o.
		for (int j = 0; j < vet.length - 1; j++) {
			for (int j2 = 0; j2 < vet.length - 1; j2++) {
				if (vet[j2] > vet[j2 + 1]) {

					int aux = vet[j2];
					vet[j2] = vet[j2 + 1];
					vet[j2 + 1] = aux;

				}
			}

		}
		Vetor vetorOrdenado = new Vetor(vet);

		return vetorOrdenado;

	}

	// }

	// M�todo extra que faz a soma do vetor (sem o par�metro).
	static public int soma(int[] vetorInteger) {
		int soma = 0;
		for (int i = 0; i < vetorInteger.length; i++) {
			soma += vetorInteger[i];
		}
		return soma;
	}

	public static void main(String[] args) {

		// Vetor passado para o par�metro do costrutor durante a instancia��o.
		int[] vetint = { 1, 2, 10, -20, 30, 40, 50, -1 };

		// Cria��o do objeto vetor.
		Vetor vetor = new Vetor(vetint);
		
		System.out.println("O vetor �: " + vetor.toString());

		// a) Soma.
		System.out.println("A soma do vetor �: " + vetor.soma());

		// b) Maior elemento.
		System.out.println("O maior elemento do vetor �: "
				+ vetor.maiorElemento());

		// c) Menor elemento.
		System.out.println("O menor elemento do vetor �: "
				+ vetor.menorElemento());

		// d) Raiz quadrada da soma do vetor.
		System.out.println("A raiz quadrada da soma do vetor �: "
				+ vetor.raizQuadrada());

		// e) Novo vetor ordenado.
		Vetor vetorOrdenado = vetor.vetorOrdenado();

		System.out.println("O novo vetor ordenado �: " +
		vetorOrdenado);

	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(vetorInteger);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vetor))
			return false;
		Vetor other = (Vetor) obj;
		if (!Arrays.equals(vetorInteger, other.vetorInteger))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return Arrays.toString(this.vetorInteger);
	}

}
