/**
 * 
 */
package br.edu.fa7.gerpro7v3.profmarcelo;

/**
 * @author gerpro7
 * 
 */
public class Vetor {

	/**
	 * Atributo que aramazena o vetor de inteiros
	 */
	private int[] vetorInteger = null;

	public Vetor(int[] vetorInteger) {
		this.vetorInteger = vetorInteger;
	}

	public int soma() {
		int soma = 0;
		for (int i = 0; i < vetorInteger.length; i++) {
			soma += vetorInteger[i];
		}
		return soma;
	}

	static public int soma(int[] vetorInteger) {
		int soma = 0;
		for (int i = 0; i < vetorInteger.length; i++) {
			soma += vetorInteger[i];
		}
		return soma;
	}

	public int maiorElemento() throws Exception {

		if (vetorInteger != null && vetorInteger.length > 0) {
			int maior = vetorInteger[0];
			for (int i = 1; i < vetorInteger.length; i++) {
				if (maior < vetorInteger[i])
					maior = vetorInteger[i];
			}
			return maior;
		} else
			throw new Exception("Vetor vazio");
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int[] vInteger = { 1, 2, 10, -20, 30, 40, 50, -1 };
		Vetor vetor = new Vetor(vInteger);

		System.out.println("O vetor �: " + vetor);

		System.out.println("A soma do vetor �: " + vetor.soma());

		System.out.println("A soma do vetor (static) �: "
				+ Vetor.soma(vInteger));

		try {
			System.out.println("O maior elemento do vetor �: "
					+ vetor.maiorElemento());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		StringBuffer textoSaida = new StringBuffer();

		textoSaida.append("{ ");

		for (int i = 0; i < vetorInteger.length; i++) {
			textoSaida.append(vetorInteger[i]);
			if (i < vetorInteger.length - 1)
				textoSaida.append(", ");
		}
		textoSaida.append("}");

		return textoSaida.toString();
	}
}
