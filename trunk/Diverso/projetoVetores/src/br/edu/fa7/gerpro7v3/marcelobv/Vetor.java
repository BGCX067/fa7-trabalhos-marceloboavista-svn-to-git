package br.edu.fa7.gerpro7v3.marcelobv;

import java.util.Arrays;

import com.sun.org.apache.bcel.internal.generic.NEWARRAY;

public class Vetor {
	/**
	 * Atributo que armazena o vetor de inteiros
	 */
	private int[] vetor = null;
	
	/**
	 * 
	 * M�todo construtor 
	 */
	public Vetor(int[] vetor){
		this.vetor = vetor;
	}
	

	public int somaVetor(){
		int total = 0;
	    for(int i = 0; i < vetor.length; i++){
	      total += vetor[i];
	    }
	      return total ;
	      		
	}
	
	/**
	 * 
	 * m�todo da classe 
	 * 
	 */
	
	public static int soma(int[] vetor){
		int soma = 0;
		for (int i = 0; i < vetor.length; i++) {
			soma += vetor[i];
			
		}
		return soma;
	}
	
	/**
	 * 
	 * M�todo que retorna o maior elemento
	 */
	
	public int maiorElemento(){
	if(vetor != null && vetor.length > 0){
		int max = vetor[0];
		for (int i = 1; i < vetor.length; i++) {
			if (vetor[i] > max){
				max = vetor[i];
			}
			
		}
		return max;
	} else
		try {
			throw new Exception("Vetor Vazio");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return 0;
	}
	
	/**
	 * retorna raiz da soma de vetores
	 * @param args
	 */
	
	public double raizQuadrada(int[] vetor){
		int soma = 0;
		for (int i = 0; i < vetor.length; i++) {
			soma += vetor[i];
			
		}
		return Math.sqrt(soma);
	}
	
	public Vetor ordenaVetor(){
		int[] vetor = this.vetor.clone();
	       boolean isSorted;   
		   int tempVariable;   
		   int numberOfTimesLooped = 0;   
		      
		   do  
		   {   
		      isSorted = true;   
		  
		      for (int i = 1; i < vetor.length - numberOfTimesLooped; i++)   
		      {   
		         if (vetor[i] < vetor[i - 1])   
		         {   
		            tempVariable = vetor[i];   
		            vetor[i] = vetor[i - 1];   
		            vetor[i - 1] = tempVariable;   
		            isSorted = false;   
		         }   
		      }   
		  
		      numberOfTimesLooped++;   
		   }   
		   while (!isSorted);   
		   Vetor vetorordenado = new Vetor(vetor);
		   return vetorordenado;
		
	}


	@Override
	public String toString() {
		
		return Arrays.toString(this.vetor);
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(vetor);
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vetor))
			return false;
		Vetor other = (Vetor) obj;
		if (!Arrays.equals(vetor, other.vetor))
			return false;
		return true;
	}


	public static void main(String[] args){
		int[] iVetor = {1,2,10,-20,30,40,50,-1};
		Vetor vetor = new Vetor(iVetor);
		System.out.println("A soma do vetor �: " + vetor.somaVetor());
		System.out.println("O maior elemento �: " + vetor.maiorElemento());
		System.out.println("A Raiz da Soma do elemento �: " + vetor.raizQuadrada(iVetor));
		System.out.println("Vetor Ordenado " + vetor.ordenaVetor());
	}
}


