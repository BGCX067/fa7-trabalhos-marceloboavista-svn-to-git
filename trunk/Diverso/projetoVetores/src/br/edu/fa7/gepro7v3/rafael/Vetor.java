/**
 * 
 */
package br.edu.fa7.gepro7v3.rafael;

import java.util.Arrays;

/**
 * @author estagio
 * 
 */
public class Vetor {

	/**
	 * Atributos que armazena valores inteiro
	 */
	private int[] vetorInteger = { 1, 2, 10, -20, 30, 40, 50, -1 };

	public Vetor(int[] vetorInteger) {
		this.vetorInteger = vetorInteger;
	}

	public Vetor getNovoVetorOrdenado() {
		int[] ordenado = vetorInteger.clone();
		int menor = 0;
		for (int j = 0; j < ordenado.length; j++) {
			menor = ordenado[j];
			for (int i = j; i < ordenado.length; i++) {
				if (menor > ordenado[i]) {
					ordenado[j] = ordenado[i];
					ordenado[i] = menor;
					menor = ordenado[j];
				}
			}
		}

		Vetor vetorOrdenado = new Vetor(ordenado);

		return vetorOrdenado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + Arrays.hashCode(vetorInteger);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vetor))
			return false;
		Vetor other = (Vetor) obj;
		if (!Arrays.equals(vetorInteger, other.vetorInteger))
			return false;
		return true;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return Arrays.toString(this.vetorInteger);
	}

	public int soma() {
		int soma = 0;
		for (int i = 0; i < vetorInteger.length; i++) {
			soma += vetorInteger[i];
		}
		return soma;
	}

	static public int soma(int[] vetorInteger) {
		int soma = 0;
		for (int i = 0; i < vetorInteger.length; i++) {
			soma += vetorInteger[i];
		}
		return soma;
	}

	public int maiorElemento() throws Exception {
		if (vetorInteger != null && vetorInteger.length > 0) {
			int maior = vetorInteger[0];
			for (int i = 1; i < vetorInteger.length; i++) {
				if (maior < vetorInteger[i])
					maior = vetorInteger[i];
			}
			return maior;
		} else
			throw new Exception("Vetor Vazio");
	}

	/**
	 * @param args
	 */

	public int menorElemento() {
		int menor = vetorInteger[0];
		for (int i = 1; i < vetorInteger.length; i++) {
			if (menor > vetorInteger[i])
				menor = vetorInteger[i];
		}
		return menor;
	}

	public double raizQuadrada() {
		double raiz;
		int valor = soma(vetorInteger);
		raiz = Math.sqrt(valor);
		return raiz;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] vInteger = { 1, 2, 10, -20, 30, 40, 50, -1 };
		Vetor vetor = new Vetor(vInteger);
		System.out.println("A soma do vetor e :" + vetor.soma());
		System.out.println("A soma do vetor e (stastic) :"
				+ Vetor.soma(vInteger));
		System.out.println("A raiz quadrada da soma do vetor e :"
				+ vetor.raizQuadrada());
		System.out.println("O Menor elemento do vetor e :"
				+ vetor.menorElemento());
		System.out.println("Veotr Ordenado:" + vetor.getNovoVetorOrdenado());
		try {
			System.out.println("O maior elemento do vetor e :"
					+ vetor.maiorElemento());
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

}
