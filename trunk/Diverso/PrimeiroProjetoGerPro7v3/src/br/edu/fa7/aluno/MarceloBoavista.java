package br.edu.fa7.aluno;

import javax.swing.JFrame;
import javax.swing.JLabel;

public class MarceloBoavista {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		JFrame frame = new JFrame("HelloWorldSwing");
	    final JLabel label = new JLabel("Ola Mundo Invocado");
	    frame.getContentPane().add(label);

	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.pack();
	    frame.setVisible(true);
	  

	}

}
