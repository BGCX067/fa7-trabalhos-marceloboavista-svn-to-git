CREATE TABLE Perfil (
  idPerfil INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  Nome VARCHAR(255) NULL,
  PRIMARY KEY(idPerfil)
);

CREATE TABLE Funcionalidades (
  idFuncionalidades INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nomeFuncionalidade VARCHAR(250) NULL,
  PRIMARY KEY(idFuncionalidades)
);

CREATE TABLE StatusProjeto (
  idStatusProjeto INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NOT NULL,
  PRIMARY KEY(idStatusProjeto)
);

CREATE TABLE StatusSistema (
  idStatusSistema INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nomeSistema VARCHAR(250) NULL,
  PRIMARY KEY(idStatusSistema)
);

CREATE TABLE ComplexidadeUC (
  idComplexidadeUC INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NULL,
  PRIMARY KEY(idComplexidadeUC)
);

CREATE TABLE Curso (
  idCurso INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  nome VARCHAR(255) NULL,
  PRIMARY KEY(idCurso)
);

CREATE TABLE CasoUso (
  idCasoUso INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  ComplexidadeUC_idComplexidadeUC INTEGER UNSIGNED NOT NULL,
  nome VARCHAR(255) NULL,
  descricao VARCHAR(255) NULL,
  PRIMARY KEY(idCasoUso),
  INDEX CasoUso_FKIndex2(ComplexidadeUC_idComplexidadeUC),
  FOREIGN KEY(ComplexidadeUC_idComplexidadeUC)
    REFERENCES ComplexidadeUC(idComplexidadeUC)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Usuario (
  idUsuario INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  Perfil_idPerfil INTEGER UNSIGNED NOT NULL,
  Curso_idCurso INTEGER UNSIGNED NOT NULL,
  matricula VARCHAR(10) NOT NULL,
  nome VARCHAR(255) NOT NULL,
  fone VARCHAR(11) NOT NULL,
  celular VARCHAR(11) NULL,
  email VARCHAR(255) NOT NULL,
  aprovado BOOL NOT NULL,
  senha VARCHAR(8) NOT NULL,
  PRIMARY KEY(idUsuario),
  INDEX Usuario_FKIndex1(Curso_idCurso),
  INDEX Usuario_FKIndex2(Perfil_idPerfil),
  FOREIGN KEY(Curso_idCurso)
    REFERENCES Curso(idCurso)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(Perfil_idPerfil)
    REFERENCES Perfil(idPerfil)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Perfil_Funcionalidades (
  Funcionalidades_idFuncionalidades INTEGER UNSIGNED NOT NULL,
  Perfil_idPerfil INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(Funcionalidades_idFuncionalidades, Perfil_idPerfil),
  INDEX Perfil_Funcionalidades_FKIndex1(Funcionalidades_idFuncionalidades),
  INDEX Perfil_Funcionalidades_FKIndex2(Perfil_idPerfil),
  FOREIGN KEY(Funcionalidades_idFuncionalidades)
    REFERENCES Funcionalidades(idFuncionalidades)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(Perfil_idPerfil)
    REFERENCES Perfil(idPerfil)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE Sistema (
  idSistema INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  StatusSistema_idStatusSistema INTEGER UNSIGNED NOT NULL,
  Usuario_idUsuario INTEGER UNSIGNED NOT NULL,
  nome VARCHAR(10) NULL,
  problema VARCHAR(255) NULL,
  afeta VARCHAR(255) NULL,
  impacto VARCHAR(255) NULL,
  solucao VARCHAR(255) NULL,
  PRIMARY KEY(idSistema),
  INDEX Sistema_FKIndex1(Usuario_idUsuario),
  INDEX Sistema_FKIndex2(StatusSistema_idStatusSistema),
  FOREIGN KEY(Usuario_idUsuario)
    REFERENCES Usuario(idUsuario)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(StatusSistema_idStatusSistema)
    REFERENCES StatusSistema(idStatusSistema)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
)
AUTO_INCREMENT = 1;

CREATE TABLE Projeto (
  idProjeto INTEGER UNSIGNED NOT NULL AUTO_INCREMENT,
  Sistema_idSistema INTEGER UNSIGNED NOT NULL,
  Sigla VARCHAR(5) NULL,
  Titulo VARCHAR(255) NULL,
  Descricao VARCHAR(1000) NULL,
  DataInicio DATE NULL,
  DataFinal DATE NULL,
  Problema VARCHAR(1000) NULL,
  QuemAfeta VARCHAR(500) NULL,
  Solucao VARCHAR(1000) NULL,
  Ano VARCHAR(4) NULL,
  Semestre VARCHAR(2) NULL,
  aprovado BOOL NULL,
  Usuario_idUsuario INTEGER UNSIGNED NOT NULL,
  StatusProjeto_idStatusProjeto INTEGER UNSIGNED NOT NULL,
  solucao_projeto VARCHAR(255) NULL,
  PRIMARY KEY(idProjeto),
  INDEX Projeto_FKIndex1(StatusProjeto_idStatusProjeto),
  INDEX Projeto_FKIndex2(Usuario_idUsuario),
  INDEX Projeto_FKIndex3(Sistema_idSistema),
  FOREIGN KEY(StatusProjeto_idStatusProjeto)
    REFERENCES StatusProjeto(idStatusProjeto)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(Usuario_idUsuario)
    REFERENCES Usuario(idUsuario)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(Sistema_idSistema)
    REFERENCES Sistema(idSistema)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE UsuarioProjeto (
  Projeto_idProjeto INTEGER UNSIGNED NOT NULL,
  Usuario_idUsuario INTEGER UNSIGNED NOT NULL,
  aprovado BOOL NULL,
  PRIMARY KEY(Projeto_idProjeto, Usuario_idUsuario),
  INDEX UsuarioProjeto_FKIndex1(Projeto_idProjeto),
  INDEX UsuarioProjeto_FKIndex2(Usuario_idUsuario),
  FOREIGN KEY(Projeto_idProjeto)
    REFERENCES Projeto(idProjeto)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(Usuario_idUsuario)
    REFERENCES Usuario(idUsuario)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);

CREATE TABLE CasoUso_Projeto (
  CasoUso_idCasoUso INTEGER UNSIGNED NOT NULL,
  Projeto_idProjeto INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY(CasoUso_idCasoUso, Projeto_idProjeto),
  INDEX CasoUso_Projeto_FKIndex1(CasoUso_idCasoUso),
  INDEX CasoUso_Projeto_FKIndex2(Projeto_idProjeto),
  FOREIGN KEY(CasoUso_idCasoUso)
    REFERENCES CasoUso(idCasoUso)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION,
  FOREIGN KEY(Projeto_idProjeto)
    REFERENCES Projeto(idProjeto)
      ON DELETE NO ACTION
      ON UPDATE NO ACTION
);


