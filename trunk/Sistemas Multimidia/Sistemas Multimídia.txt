Sistemas Multim�dia
--------------------------------------------------------------------------
aula: 16/11/2009
--------------------------------------------------------------------------

Imagem
	
	-> Matriz de cores

	-> Cor: true color 24 bits RGB(8bits por componente)
	
	=> Matriz de Inteiros (32bits => +8bits Alta)
		->Mapa de bits (Bitmap)

	-> Usa-se ou escreve-se bibliotecas para ler o arquivo de acordo
	com a sua especifica��o.

	-> Com a Matriz em m�os, joga-se para a placa de v�deo
	(framebuffer) para a exibi��o.

	-> Para esse passo, usa-se bibliotecas ou escreve-se atrav�s da
	API do S.O. usado.

	-> Exemplo de biblioteca livre: SDL.

--------------------------------------------------------------------------

Procedimento de Imagens

	-> Trabalhos feitos no Bitmap
	-> Efeitos mais comuns e simples:
		* Brilho
		* P&B (escala de cinza)
		* Negativo
		* Limiar (Threshold)
	-> Efeitos mais complexos s�o obtidos atrav�s da Matris de
	Convolu��o e do estudo matem�tico da distribui��o das 
	frequ�ncias.
--------------------------------------------------------------------------

"Para cada pixel, fa�a ..."

-> Brilho

	r = r * valor 
	g = g * valor
	b = b * valor

	(valor > 1 => aumentar o brilho)
	(valor < 1 => diminuir o brilho)

-> Escala de cinza

	media = (r+g+b)/3;
	r = m;
	g = m;
	b = m;

	------------------------------------------------------------------

	m = (0.212671*r + 0.715160*g + 0.072169*b);
	(algor�tmo da m�dia ponderada)

-> Negativo
	
	r = 1 - r;
	g = 1 - g;
	b = 1 - b;

-> Limiar

	r,g,b = 1, se L >= x
	r,g,b = 1, se 0 < x

-> S�pia
	
	media = (r+g+b)/3;
	r = m;
	g = m;
	b = m;	

--------------------------------------------------------------------------
aula: 18/11/2009
--------------------------------------------------------------------------
O �UDIO

-> Propriedades f�sicas do som

	SOM: Percebido pela movimenta��o do t�mpano causado por varia��es
	de press�o no ar.
	
	- � uma onda mec�nica que se propaga no ar.
	
	- ONDA: uma varia��o peri�dica de alguma propriedade.
	
	- � a varia��o peri�dica da press�o do ar.

-> Representar

-> frequ�ncia: Quantidade de repeti��es da onda por unidade de tempo.

	- Unidade maiscomum: Hz (vezes por segundo)


-> Par�metro percept�veis do som:
	
	- Intensidade:	- Percep��o da Amplitudade.
			- Volume do som.

	- Altura: 	- Percep��o de sons graves e agudos.
			- Graves: baixos; Agudos: altos.
			- Percep��o da freq�ncia da onda. 
			  frequ�ncias altas: agudos
			  frequ�ncias baixas: graves

	- Timbre: 	- quantidade que distingue sons de mesma
			  intensidade e altura emitidos por 
			  instrumentos diferentes.


1. Intensidade

	Ondas com intensidades diferentes e mesma frequ�ncia[fugura]

	-> Unidade de medida da intensidade sonora: decib�is(dB)

	-> 0dB: Limiar da audiabilidade.

	-> 120dB: Limiar da Dor.

	-> acima de 100dB: Polui��o sonora.

	-> Com 16 bits, conseguimos representar uma faixa de 16db.

2.A Altura

	-> Limites da percep��o do ouvido humano:
		M�nimo: 16Hz
		M�ximo: 15KHz a 20Hz
		Melhor taxa de audiabilidade: 300Hz a 3500Hz
------------------------------------------------------------------------

Representa��o:

Dom�nio do tempo   x 	Dom�nio da frequ�ncia
[figura]		[figura]

Ru�do Branco
[figura]

--------------------------------------------------------------------------

Filtragens

	Processamentos no dom�nio da frequ�ncia.
	- Filtro passa-baixa: 	retira freq. altas.
	- Filtro passa-alta: 	retira freq. baixas.
	- Filtro passa-faixa:	retira freq. fora de uma determinada faixa.
	- Filtro rejeita-baixa:	retira freq. dentro de uma determinada faixa.


--------------------------------------------------------------------------
aula: 23/11/2009
--------------------------------------------------------------------------

Representa��o Digital do som

-> Digitaliza��o do som

1. Filtragem: Filtro anal�gico para limita��o das frequ�ncias.
   Evita ocorr�ncia de pseudon�mea.

2. Amostragem: Convers�o do sinal anal�gico cont�nuo em discreto 
   (sequ�ncia) de pulsos.

3. Quantiza��o: convers�o dos pulsos em n�meros.

4. Arquivo de �udio: grava��o em arquivo da sequ�ncia de pulsos quantizada.


A AMOSTRAGEM

[Figura]

- Taxa de amostragem:
	Quantidade de amostras capturadas em uma unidade de tempo (1s).
- Unidade: Hz
- Exemplo: 5Hz


PSEUDON�MEA

[Figura]

Frequ�ncia de Nyguist:
	Para representar bem uma onda, a taxa de amostragem precisa
	ser, pelo menos, o dobro da frequ�ncia da onda.


- Ouvido Humano:
	At� 20KHz
	
- Logo precisamos de, no m�nimo 40KHz da taxa de amostragem para digitalizar
  as frequ�ncias aud�veis.

- CD - �udio:
	Taxa de amostragem: 44,1KHz.

- Telefone: 
	At� 3,5KHz.
	Logo, 8KHz de taxa de amostragem.


QUANTIZA��O

[Figura]

-> Atribui��o dos valores bin�rios para cada amostra.
-> No exemplo: 3bits.

-> Amplitude m�xima: 96KdB

-> Suficiente: 16 bits.

-> CD - �udio  44,1kHz a 16 bits


ASPECTOS QUANTITATIVOS

-> CD - Audio:	44,1kHz amostras
		16bits por amostra
		2 canais (estereo)

1 h de m�sica

=> 3600 s de musica
=> 44100x3600 amostras
=> 158.760.ooo amostras
=> 158.760.000 x 2 bytes
=> 317.520.000 bytes
=> 302.81 MBytes (x2 canais-estereo)
=> 604MBytes 


CONCLUS�O

-> Um som digital � um array de n�meros.
-> Para som estereo, geralmente temos posi��es �mpares o lado esquerdo e,
-> posi��es pares, o lado direito.
-> O arquivo de som possui esse array gerando com as devidas compacta��es
   e cabe�alhos.


PROCESSAMENTO

-> Utiliza-se ou constr�i-se bibliotecas que leiam os arquivos de �udio
   e devolvem o array.
-> O processamento nada mais � do que um trabalho nesse array.
-> Faz-se processamentos no dom�nio do tempo e da frequencia.


DOMINIO DA FREQUENCIA

- Utiliza-se a "Transformada Discreta de fourrier"
- Algor�tmo que devolve as v�rias ondas contidas no som separados de acordo
  com suas frequencias.


DOMINIO DO TEMPO

-> Processamento nas amplitudes pelo tempo.
-> Basicamente uma leitura da array aplicando-se alguns c�lculos nas
   amostras.

Exemplo:
	-> Aumentar o volume:
	
		- para cada amostra[i],
		  amostra[i] = v * amostra[i].

		v > 1: Aumenta
		v < 1 Diminui

	-> Fade-out / in

		- Como podeira ser feito?
		(Aumentando ou diminuindo o progressivo do volume do som)

	-> Como poderia ser feito um processamento para dar a impress�o de
	   que um som est� se movimentando da esquerda para direita??

--------------------------------------------------------------------------
aula: 25/11/2009
--------------------------------------------------------------------------


V�DEO

-> Sequ�ncia de imagens

-> Taxa padr�o: ~30fps (Televis�o)

-> fps:	frames per second
	quadros por segundo
	iamgens por segundo

-> Cinema: 24 fps

-> Sistemas estereosc�picos: at� 60 fps


Armazenamento de v�deo

-> V�deo sem compacta��o
	
	. Exemplo: 1 h de v�deo FullHD (Blue-Ray)		
		
		* 1960 x 1080 x 3 bytes x 3600 x 30 fps
		obs: 3 bytes => cada pixel em RGB
		
		= 685.842.200.000 bytes
		= 638,74 GBytes

		* Velocidade:
			30fpsx1960x1080x3 = 181,69MBytes/s


Compacta��o de v�deo

. Compacta��o de cada quadro
. Compacta��o entre quadros
. T�cnicas de compacta��o de arquivos quaisquer

1. ex:	Retirar aquilo que o olho humano n�o percebe.
	-> Tratar em blocos ao inv�s de pixels.
	-> Cromin�ncia: O olho humano � menos sens�vel � cor do que
	   � luminosidade.


V�deo

. Logo, utiliza-se um sistema de cores que separa lumin�ncia de 
  cromin�ncia.
	ex: HUV e YIQ. 	onde Y->luminancia , IQ->crominancia
	
. Mesmo no v�deo anal�gico, essa id�ia foi utilizada, pois possibilitou:
	a. Redu��o da banda utilizada por um canal de televis�o.
	b. Compatibilidadecom o P&B.

. Guarda-se a luminosidade p/ todos os pixels, mas a cromin�nca � 
guardada apenas para pixels alternados.


. Como a televis�o/ computador precisa da imagem em RGB para exibi��o,
faz-se o processo inverso de transforma��o de YIQ p/ RGB.

2. Compacta��o entre quadros
	. Blocos de imagem que n�o se alteram de um quadro para o 
	outro, n�o s�o armazenados.

3. Ex: C�digo de Heffman


.Codecs

ex:	Motion Jpeg x Mpeg

	Motion Jpeg
		-> Sequencia de imagens JPEG.
		-> N�o h� compacta��o temporal (entre quadros) 

	MPEG
		-> Inspirado no motion JPEG
		-> Por�m introduziu-se a compacta��o temporal.



--------------------------------------------------------------------------
aula: 30/11/2009
--------------------------------------------------------------------------

Compacta��o de dados

. Suponha a seguinte String:
	AABACCCDEABCD

. Com a codifica��o de 8 bits por caractere, teremos:
	13 caracteres * 8 bits:
		104 bits (13 bytes)

. Como temos apenas 5 caracteres diferentes � desnecess�rio utilizar 
8 bits/caractere, pois tal codifica��o poderia representar 256 caracteres
diferentes.

1� id�ia para redu��o
-> 3 bits s�o suficientes por caractere
-> Poder�amos fazer a seguinte codifica��o

CARACTERE	C�DIGO
A		000
B		001	
C		010
D		011
E		100
 

- Logo, teremos 
	13 caracteres * 3 bits = 39 bits (4,875 bytes)

- O arquivo codificado ficaria:
	
	000 000 001 000 010 010 010 011 100 000 001 010 011
	 A   A   B   A   C   C   C   D   E   A   B   C   D

2� Id�ia p/ compacta��o

-> c�digo de tamanho vari�vel

-> Poder�amos fazer a seguinte codifica��o

CARACTERE	C�DIGO
A		0
B		1	
C		10
D		11
E		100

- Logo ter�amos:

	4 A's * 1 bit  = 4
+	2 B's * 1 bits = 2
+	4 C's * 2 bits = 8
+	2 D's * 2 bits = 4
+	1 E's * 3 bits = 3
total		21 bits (2.625 bytes)

O arquivo codificado ficaria:

001010101011100011011



Problemas:

-> Como o tamanho � vari�vel, n�o sabemos "onde parar" assumir que um arquivo foi formado.


Compacta��o de Dados

. Perceba que o 'C', que aparece 4 vezes, ficou com um c�digo maior que o 'B'
que apareceu apenas 2 vezes.


3� id�ia: C�digo de Huffman

-> Resolve os dois problemas:
	. Assume tamanho vari�vel, mas sabe quando um caractere � formado;
	. Os carac. que aparecem mais vezes, ficam com os c�digos menores

-> C�digo de prefixo n�o pode corresponder a outro carac.
	Ou seja, basta arranjar uma forma de de montar os c�digos sem que
	o in�cio do c�digo de um carac forme o c�d de outros caracteres.

. No Exemplo t�nhamos:

	C:10 e E:100 ou seja, o c�digo de 'C' era prefixo de 'E'.

. por esse motivo n�o foi poss�vel "descompactar", pois, ao ler '10', n�o 
daria pra saber se j� se tratava de C ou era apenas o in�cio de E.

-> O c�digo de Hoffman ficaria:

Carac	C�digo
A	00
B	01
C	10
D	110
E 	111

4*2 + 2*2 + 4*2 + 2*3 + 1*3
8 + 4 + 8 + 6 + 3 = 29bits = 3.625 bytes

Embora maior garantimos a "descompacta��o", pois nenhum caractere
 tem seu c�digo com o prefixo do outro.
	



--------------------------------------------------------------------------
aula: 02/12/2009
--------------------------------------------------------------------------

C�digo de Huffman

-> Trabalha com c�digo de prefixo, evitando que eles formem c�digos
completos de outros caracteres.

-> C�digo de tamanho vari�vel onde os caracteres que mais aparecem
t�m os c�digos de tamanhos menores e vice-versa.

-> Gera a menor codifica��o poss�vel para um determinado conjunto
de caracteres, num determinado texto.

-> Funciona com qualquer tipo de arquivo. Basta considerarmos como
s�mbolo unit�rio do arquivo ao inv�s de caracteres, bytes.

-> Nota��o:
	-> Alfabeto: 	conjunto de s�mbolos presentes na string que se
			deseja compactar.
	
	-> S�mbolo: 	unidade da String

-> Utiliza-se uma �rvore.

-> Os s�mbolos ficam nas folhas e o caminho da raiz at� as eles s�o 
seus respectivos c�digos.

-> A �rvore � montada de tal forma que os s�mbolos que mais aparecem 
ficam em n�veis mais altos da �rvore e os que menos aparecem ficam em 
n�veis mais profundos.

-> isso garante 1.

. O Algor�tmo:

1. Ordene os s�mbolos  de acordo com o n� de vezes que aparece na String

2. Monte um ramo com os dois s�mbolos que menos aparecem.

3. Considere o ramo como um novo s�mbolo cuja frequ�ncia (quantidade 
de vezes que aparece) � a soma da frequencia de seus n�s filhos

4. Se ainda houver mais de um s�bolo, v� para o passo 1.


Exemplo:

AAABBCCABACBBADEDFFD (Tamanho original: 20 bytes)

A: 6
B: 5
C: 3
D: 3
E: 1
F: 2



[FIGURA DA �RVORE]

				(20)
			  /
			(12)
		 /
		(6)
	 /				 \
	(3) 				(8)
 /	 |	 |	 |		 |	 \
E:1	F:2	D:3	A:6 		C:3	B:5


S�mbolo 	C�digo
A		01
B		11
C		10
D		001
E		0000
F		0001



Tamanho Final:		6 * 2bits + 5 * 2bits
		+ 	3 * 2bits + 3 * 3bits
		+ 	1 * 4bits + 2 * 4bits
		
		total	12 + 10 + 6 + 9 + 4 + 8 = 49bits = 6,125bytes


Considera��es

-> Quando o arquivo � muito pequeno, compact�-lo pode aumentar seu tamanho:
A tabela com os c�digos tem que ser guardada.

-> Arquivos j� compactados n�o diminuem seu tamanho ao recompactar, pois a
redund�ncia  j� fora tirada.

-> Trata-se de um algor�tmo guloso! Algor�tmos que usam t�cnicas gulosas
ou seja eles semprem fazem o melhor a cada passo. sem analisar o global.
		