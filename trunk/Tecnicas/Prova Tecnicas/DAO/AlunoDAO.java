package br.edu.fa7.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

public class AlunoDAO {
	public void getAlunos() {
		Connection conn = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			Context initCtx = new InitialContext();
			Context envCtx = (Context) initCtx.lookup("java:comp/env");
			DataSource ds = (DataSource) envCtx.lookup("jdbc/fa7");
			conn = ds.getConnection();
			stmt = conn.createStatement();
			rs = stmt.executeQuery("SELECT * FROM alunos");
			while (rs.next()) {
				String alunoNome = rs.getString("nome");
				String alunoIdade = rs.getString("idade");
				int alunoId = rs.getInt("id");
				System.out.println(alunoId);
				System.out.println(alunoNome);
				System.out.println(alunoIdade);
			}
		} catch (NamingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
