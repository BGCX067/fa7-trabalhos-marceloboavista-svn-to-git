package br.edu.fa7.dao;

import java.sql.*;
import java.util.*;
import javax.naming.*;

import br.fa7.edu.model.Aluno;

public class AlunoDAO {
    public List<Aluno> getAlunos() {
        List<Aluno> alunos = new ArrayList<Aluno>();
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            Context initCtx = new InitialContext();
            Context envCtx = (Context) initCtx.lookup("java:comp/env");
            //Obtendo um DataSource
			DataSource ds = (DataSource) envCtx.lookup("jdbc/fa7");
            conn = ds.getConnection();
            stmt = conn.createStatement();
            rs = stmt.executeQuery("SELECT * FROM alunos");
            //Percorrendo resultados
			while (rs.next()) {
                String nome = rs.getString("nome");
                String idade = rs.getString("idade");
                int id = rs.getInt("id");
                
                Aluno a = new Aluno(nome, idade, id);
                alunos.add(a);
            }
        } catch (NamingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
			
			//Liberando os recursos do sistema
		} finally {
            try {
                rs.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            
            try {
                stmt.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            try {
                conn.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return alunos;

    }
}