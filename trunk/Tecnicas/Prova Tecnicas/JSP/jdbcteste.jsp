	

<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<sql:query var="rs" dataSource="jdbc/fa7">
select id, nome, idade from alunos
</sql:query>

<html>
  <head>
    <title>Teste JDBC</title>
  </head>
  <body>

  <h2>Results</h2>
  
<c:forEach var="row" items="${rs.rows}">
    Nome ${row.nome}<br/>
    Idade ${row.idade}<br/>
</c:forEach>

  </body>
</html>
