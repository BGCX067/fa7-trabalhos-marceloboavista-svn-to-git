<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Listagem de alunos</title>
</head>
<body>
<h1>Lista de alunos</h1>

<c:forEach items="${alunos}" var="alu" varStatus="i">
	<h2>Aluno n.� ${i.count}</h2>
	<p>Nome: ${alu.nome}</p>
	<p>Idade: ${alu.idade}</p>
	<p>Curso: ${alu.curso}</p>

</c:forEach>
<a href="inserirAluno.jsp">Inserir um Novo Aluno</a>
</body>
</html>