package br.fa7.edu.model;

public class Aluno {
	private String nome;
	private String idade;
	private String curso;
	private int id;
	
	public Aluno(String nome, String idade, String curso) {
		this.nome = nome;
		this.idade = idade;
		this.curso = curso;
	}
	public Aluno() {
	}
	
	
	
	public Aluno(String nome, String idade, int id) {
		super();
		this.nome = nome;
		this.idade = idade;
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getIdade() {
		return idade;
	}
	public void setIdade(String idade) {
		this.idade = idade;
	}
	public String getCurso() {
		return curso;
	}
	public void setCurso(String curso) {
		this.curso = curso;
	}
	
	
}
