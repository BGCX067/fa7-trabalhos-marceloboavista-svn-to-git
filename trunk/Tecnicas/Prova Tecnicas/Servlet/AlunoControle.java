package br.fa7.edu.controle;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.edu.fa7.dao.AlunoDAO;
import br.edu.fa7.negocio.AlunoNegocio;
import br.fa7.edu.model.Aluno;


/**
 * Servlet implementation class for Servlet: NovoAluno
 * 
 */
public class AlunoControle extends HttpServlet {
	static final long serialVersionUID = 1L;
	
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		AlunoDAO dao = new AlunoDAO();
		dao.getAlunos();
		
		AlunoNegocio negocio = new AlunoNegocio();
		
		List<Aluno> alunos = negocio.recuperarAlunos();
		
		request.setAttribute("alunos", alunos);
		RequestDispatcher controle =
			request.getRequestDispatcher("exibirAlunos.jsp");
		
		controle.forward(request, response);
		//response.sendRedirect("lencol.jsp");
	}
	
	@Override
	public void doPost(
			HttpServletRequest request,
			HttpServletResponse response)
			throws ServletException, IOException {
		
		String nome = request.getParameter("nome");
		String idade = request.getParameter("idade");
		String curso = request.getParameter("curso");
		
		Aluno aluno = new Aluno(nome, idade, curso);
		
		AlunoNegocio ng = new AlunoNegocio();
		
		ng.inserirAluno(aluno);
		
		request.setAttribute("novoAluno", aluno);
		
		RequestDispatcher controle =
			request.getRequestDispatcher("aluno.jsp");
		
		controle.forward(request, response);
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
}