Reserva = {

	nome: null,

	periodo_inicial: null,

	periodo_final: null,

	tp_quarto: null,

	arrayReserva: new Array(),

	validaReserva: function( nome, periodo_inicial, periodo_final, tp_quarto ){

		if( nome == "" || periodo_inicial == "" || periodo_final == "" || tp_quarto == "" ){
			return false;
		}	

		return true;

	},

	reservar: function( nome, periodo_inicial, periodo_final, tp_quarto ){

		if( !this.validaReserva( nome, periodo_inicial, periodo_final, tp_quarto ) ){
			alert( "Nenhum campo pode ser vazio" );
			return false;
		}		

		this.nome = nome;
		this.periodo_incial = periodo_inicial;
		this.periodo_final = periodo_final;
		this.tp_quarto = tp_quarto;
		this.arrayReserva.push( this.nome );
		this.arrayReserva.push( this.periodo_incial );
		this.arrayReserva.push( this.periodo_final );
		this.arrayReserva.push( this.tp_quarto );
		
	},

	verReservas: function(){
		alert( this.arrayReserva );
	}

}