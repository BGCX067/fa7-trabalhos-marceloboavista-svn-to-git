import java.awt.Graphics;
import java.awt.Panel;
import java.util.*;
import javax.swing.JPanel;

import com.sun.corba.se.impl.orbutil.graph.Graph;

public class SortingAnimation {
	protected long[] elements;


	public SortingAnimation(int size){
		this.elements = new long[size];
	}
	
	public void populate(){
		for(int i=0; i <this.elements.length; i++){
			this.elements[i] = Math.round(Math.random()* this.elements.length * 3);
		}
	}
	public static  void troca (long[] v, int j, int aposJ)
	  {
	     long aux = 0;
	     aux = v [j];
	     v [j] = v [aposJ];
	     v [aposJ] = aux;
	     DesenhaTela.update(v,j);     
	 
	  }
	
	public void bolha(long[] vetor){
		for(int i=0;i<vetor.length-1;i++){
			for(int x=0;x<vetor.length-1-i;x++){
			  if(vetor[x]>vetor[x+1]){
				troca(vetor,x,x+1);
								
			  }
			}
		}
		
	}
		
	 static int partition (long a[], int m, int n) {
			long x = a[m];  // Pivot-Element
			int j = n + 1;
			int i = m - 1;
				
			while (true) {
			    j--;
			    while (a[j] > x) j--;
			    i++;
			    while (a[i] < x) i++;
			    if (i < j) troca(a, i, j);
			    else return j;
			}
		    }

	 static void qsort (long a[], int l, int r) {
			if (l < r) {
			    int r2 = partition (a, l, r);
			    qsort (a, l, r2);
			    qsort (a, r2 + 1, r);
			}
		    }
	public  void selectionSort ( long [ ] v ){ 
		for (int i = 0; i < v.length - 1; i++){ 
			int k = i;
		for (int j = i + 1; j < v.length; j++)
			if ( v [ j ] < v [ k ] )
				k = j;
			troca( v,k, i );
		}
	}
	
     /* Algoritimo de insercao */
	public void insercao( long[ ] lista) {
		
		for (int i = 1; i < lista.length; i++) {
			long pivo = lista[i];
			int j = i - 1;
			while ((j >= 0) && (lista[j] > pivo)) {
				lista[j + 1] = lista[j];
				j--;
			}
			lista[j + 1] = pivo;
			DesenhaTela.update(lista, j); 
		}
		
	}
	/* Algoritimo Shell Sort */
	public void shellSort(long[] lista) {
		int h = 1;
		while (h <= (lista.length - 1)) {
			h = 3 * h + 1;
		}
		do {
			h = h / 3;
			for (int i = h; i < lista.length; i++) {
				long pivo = lista[i];
				
				int j = i - h;
				
				while ((j >= 0) && (lista[j] > pivo)) {
					lista[j + h] = lista[j];
					j = j - h;
				}
				lista[j + h] = pivo;
				DesenhaTela.update(lista, j); 
			}
		} while (h > 1);
		
	}
	
	/* Algoritimo Merge Sort */
	
	public void mergeSort(final long[] lista,final int inicio,final int fim) {
		
		if (inicio < fim) {
			int meio = ((fim + inicio) / 2);
			
			if (inicio < meio) {
				mergeSort(lista, inicio, meio);
			}
			
			if ((meio + 1) < fim) {
				mergeSort(lista, meio + 1, fim);
			}
			merge(lista, inicio, meio, fim);
		}
		
	}
	
	/* M�todo Merge do Mergesort */
	public void merge( long[] lista, int inicio,  int meio, int fim) {
		
		 int i = inicio;
		 int j = meio + 1;
		 int k = 0;
		long[] aux = new long[fim - inicio];
		while ((i <= meio) && (j <= fim)) {
			if (lista[i] <= lista[j]) {
				aux[k] = lista[i];
				i++;
			} else {
				aux[k] = lista[j];
				j++;
			}
			k++;
			
		}
		if (i <= meio) {
			for (j = meio; j >= i; j--) {
				lista[fim - meio + j] = lista[j];
			}
		}
		for (i = 0; i < k; i++) {
			lista[inicio + i] = aux[i];
			DesenhaTela.update(lista,i); 
		
		}
	}
	/* Bucket Sort 
	public void bucketSort (long[] lista, int m)
    {
	long[] buckets = new long[m];

	for (int j = 0; j < m; ++j)
	    buckets [j] = 0;
	for (int i = 0; i < lista.length; ++i)
	    ++buckets [(int) lista[i]];
	for (int i = 0, j = 0; j < m; ++j)
	    for (int k = (int) buckets [j]; k > 0; --k){
		lista [i++] = j;
	DesenhaTela.update(lista,i); 
	    }
    }*/
	
		
		public void bucketSort(long [] iElementos, int iUniverso) {
		long [] iContadores = new long [iElementos.length];
		int nElem = iElementos.length;
		// inicializa todos os contadores com zero
		for (int i = 0; i < iUniverso; ++i) {
		iContadores[i] = 0;
			DesenhaTela.update(iElementos,i);
		}
		// incrementa o contador na posi��o correspondente ao valor encontrado
		for (int j = 0; j < nElem; ++j) {
		++iContadores[(int)iElementos[j]];
			DesenhaTela.update(iElementos,j);
		}
		// coloca os valores ordenadamente no vetor resultante
		for (int i = 0, j = 0; i < iUniverso; ++i) {
		for ( ; iContadores[i] > 0; --iContadores[i]) {
		iElementos[j++] = i;
			DesenhaTela.update(iElementos,i);
		}
		}
		}
		
	
	/* Counting Sort */
	public void countingSort(long a[], int m){
	for(int i = 1; i < a.length; i++){
			if(a[i-1] == a[i]){
			int aux = (int)a[i - 1];
				System.out.println(aux);
			}
		}		


		long temp[] = new long[m];
  
    
    for (int i = 0; i < m; i++)
        temp[i] = 0;
 
    for (int i = 0; i < a.length; i++)
        temp[(int)a[i]] += 1;
 
    
    int pos = 0;
    for (int i = 0; i < temp.length; i++){
        int count = (int)temp[i];
        for (int j = 0; j < count; j++){
		DesenhaTela.update(a,count); 
            a[pos] = i;
            pos++;
        }
    }
    
 
}

		
	/* heap sort */
		
	public void heapSort(long a[]){
	int N = a.length;
	for (int k = N/2; k > 0; k--) {
	    downheap(a, k, N);
	}
	do {
            long T = a[0];
            a[0] = a[N - 1];
            a[N - 1] = T;
	    N = N - 1;
	   downheap(a, 1, N);
		DesenhaTela.update(a,N); 
	} while (N > 1);
    }

	public void downheap(long a[], int k, int N){
	long T = a[k - 1];
	while (k <= N/2) {
            int j = k + k;
            if ((j < N) && (a[j - 1] < a[j])) {
	        j++;
	    }
	    if (T >= a[j - 1]) {
		break;
	    } else {
                a[k - 1] = a[j - 1];
                k = j;
	}
	}
        a[k - 1] = T;
	
    }



	   /* end HeapSorter */
	
    /* Radix Sort */
    
	

	/* end Radix */

	
}
