import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class Ordena {
	int comparacao;
	long movimentacao;
	PrintWriter print;

	public static void main(String[] args) {
		Ordena ordenacao = new Ordena();

		try {
			ordenacao.print = new PrintWriter(new FileOutputStream(new File(
					"C:/Documents and Settings/anderson/Desktop/Saida.txt")));
		} catch (FileNotFoundException e) {
			System.out
					.println("Nao foi possivel criar o arquivo de texto em C:/");
		}

		ordenacao.print.println("Selecao: ");
		ordenacao.print.println();
		ordenacao.print.print("Antes: ");
		ordenacao.tabelaSelecao();
		ordenacao.print.println();
		ordenacao.print.println("ShellSort: ");
		ordenacao.print.println();
		ordenacao.print.print("Antes: ");
		ordenacao.tabelaShellsort();
		ordenacao.print.println();
		ordenacao.print.println("MergeSort: ");
		ordenacao.print.println();
		ordenacao.print.print("Antes: ");
		ordenacao.tabelaMergesort();
		ordenacao.print.println();
		ordenacao.print.println("QuickSort: ");
		ordenacao.print.println();
		ordenacao.print.print("Antes: ");
		ordenacao.comparacao = 0;
		ordenacao.movimentacao = 0;
		ordenacao.tabelaQuicksort();
		ordenacao.print.close();
	}

	// Metodos que auxiliam na cria��o, impress�o de listas e contagem de tempo

	// Gera lista Aleat�ria
	public int[] gerarListaAleatoria(int numero) {
		int[] lista = new int[numero];
		for (int i = 0; i < lista.length; i++) {
			lista[i] = (int) (Math.random() * (numero * 5));
		}
		return lista;
	}

	// Gera lista Decrescente
	public int[] gerarListaDecrescente(int numero) {
		int[] lista = new int[numero];
		int d = lista.length;
		for (int i = 0; i < lista.length; i++) {
			d--;
			lista[i] = d;
		}
		return lista;
	}

	// Gera lista Crescente
	public int[] gerarListaCrescente(int numero) {
		int[] lista = new int[numero];
		for (int i = 0; i < lista.length; i++) {
			lista[i] = i;
		}
		return lista;
	}

		// Produz tabela para o algoritmo Sele��o
	public void tabelaSelecao() {
		System.out.println("Selecao");
		System.out.println("Tipo da Lista  " + " Tamanho " + " Comparacoes "
				+ " Movimentacoes " + " Tempo(ms) ");

		System.out.print("                  " + 1000);
		int[] lista = gerarListaAleatoria(1000);
		imprimirLista(lista);
		int[] copiaLista = new int[1000];
		long tempo = startTime();
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			selecao(copiaLista);
		}
		tempo = finalTime(tempo);
		print.print("Depois: ");
		imprimirLista(copiaLista);
		System.out.println("        " + comparacao + "       " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Aleatoria " + "      " + 10000);
		lista = gerarListaAleatoria(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			selecao(copiaLista);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "      " + movimentacao
				+ "        " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaAleatoria(100000);
		tempo = startTime();
		selecao(lista);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "     " + movimentacao
				+ "      " + tempo);

		System.out.print("                  " + 1000);
		lista = gerarListaDecrescente(1000);
		copiaLista = new int[1000];
		tempo = startTime();
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			selecao(copiaLista);
		}
		tempo = finalTime(tempo);
		System.out.println("        " + comparacao + "       " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Decrescente " + "    " + 10000);
		lista = gerarListaDecrescente(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			selecao(copiaLista);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "      " + movimentacao
				+ "        " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaDecrescente(100000);
		tempo = startTime();
		selecao(lista);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "     " + movimentacao
				+ "      " + tempo);

		System.out.print("                  " + 1000);
		lista = gerarListaCrescente(1000);
		copiaLista = new int[1000];
		tempo = startTime();
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			selecao(copiaLista);
		}
		tempo = finalTime(tempo);
		System.out.println("        " + comparacao + "       " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Crescente " + "      " + 10000);
		lista = gerarListaCrescente(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			selecao(copiaLista);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "      " + movimentacao
				+ "        " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaCrescente(100000);
		tempo = startTime();
		selecao(lista);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "     " + movimentacao
				+ "      " + tempo);
		System.out.println();
		System.out.println();

	}

	// Produz tabela para o algoritmo Shellsort
	public void tabelaShellsort() {
		System.out.println("Shellsort");
		System.out.println("Tipo da Lista  " + " Tamanho " + " Comparacoes "
				+ " Movimentacoes " + " Tempo(ms) ");

		System.out.print("                  " + 1000);
		// Criar e imprimir a lista antes de ser ordenada
		int[] lista = gerarListaAleatoria(1000);
		imprimirLista(lista);
		int[] copiaLista = new int[1000];
		long tempo = startTime();
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			shellSort(copiaLista);
		}
		tempo = finalTime(tempo);
		// Imprimir a lista p�s-ordena��o.
		print.print("Depois: ");
		imprimirLista(copiaLista);
		System.out.println("       " + comparacao + "       " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Aleatoria " + "      " + 10000);
		lista = gerarListaAleatoria(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			shellSort(copiaLista);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "      " + movimentacao
				+ "          " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaAleatoria(100000);
		tempo = startTime();
		shellSort(lista);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "     " + movimentacao
				+ "         " + tempo);

		System.out.print("                  " + 1000);
		lista = gerarListaDecrescente(1000);
		copiaLista = new int[1000];
		tempo = startTime();
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			shellSort(copiaLista);
		}
		tempo = finalTime(tempo);
		System.out.println("       " + comparacao + "       " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Decrescente " + "    " + 10000);
		lista = gerarListaDecrescente(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			shellSort(copiaLista);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "      " + movimentacao
				+ "          " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaDecrescente(100000);
		tempo = startTime();
		shellSort(lista);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "     " + movimentacao
				+ "         " + tempo);

		System.out.print("                  " + 1000);
		lista = gerarListaDecrescente(1000);
		copiaLista = new int[1000];
		tempo = startTime();
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			insercao(copiaLista);
		}
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "      " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Crescente " + "      " + 10000);
		lista = gerarListaCrescente(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			shellSort(copiaLista);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "      " + movimentacao
				+ "          " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaCrescente(100000);
		tempo = startTime();
		shellSort(lista);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "     " + movimentacao
				+ "          " + tempo);
		System.out.println();
		System.out.println();

	}

	// Produz tabela para o algoritmo Mergesort
	public void tabelaMergesort() {
		System.out.println("Mergesort");
		System.out.println("Tipo da Lista  " + " Tamanho " + " Comparacoes "
				+ " Movimentacoes " + " Tempo(ms) ");

		System.out.print("                  " + 1000);
		int[] lista = gerarListaAleatoria(1000);
		imprimirLista(lista);
		int[] copiaLista = new int[1000];
		long tempo = startTime();
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			mergeSort(lista, 0, 999);
		}
		tempo = finalTime(tempo);
		print.print("Depois: ");
		imprimirLista(copiaLista);
		System.out.println("       " + comparacao + "       " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Aleatoria " + "      " + 10000);
		lista = gerarListaAleatoria(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			mergeSort(lista, 0, 9999);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "      " + movimentacao
				+ "          " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaAleatoria(100000);
		tempo = startTime();
		mergeSort(lista, 0, 99999);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "     " + movimentacao
				+ "         " + tempo);

		System.out.print("                  " + 1000);
		lista = gerarListaDecrescente(1000);
		tempo = startTime();
		copiaLista = new int[1000];
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			mergeSort(lista, 0, 999);
		}
		tempo = finalTime(tempo);
		System.out.println("       " + comparacao + "       " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Decrescente " + "    " + 10000);
		lista = gerarListaDecrescente(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			mergeSort(lista, 0, 9999);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "      " + movimentacao
				+ "          " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaDecrescente(100000);
		tempo = startTime();
		mergeSort(lista, 0, 99999);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "     " + movimentacao
				+ "         " + tempo);

		System.out.print("                  " + 1000);
		lista = gerarListaCrescente(1000);
		tempo = startTime();
		copiaLista = new int[1000];
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			mergeSort(lista, 0, 999);
		}
		tempo = finalTime(tempo);
		System.out.println("       " + comparacao + "       " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Crescente " + "      " + 10000);
		lista = gerarListaCrescente(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			mergeSort(lista, 0, 9999);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "      " + movimentacao
				+ "          " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaCrescente(100000);
		tempo = startTime();
		mergeSort(lista, 0, 99999);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "     " + movimentacao
				+ "         " + tempo);
		System.out.println();
		System.out.println();
	}

	// Produz tabela para o algoritmo Quicksort
	public void tabelaQuicksort() {
		System.out.println("Quicksort");
		System.out.println("Tipo da Lista  " + " Tamanho " + " Comparacoes "
				+ " Movimentacoes " + " Tempo(ms) ");

		System.out.print("                  " + 1000);
		int[] lista = gerarListaAleatoria(1000);
		imprimirLista(lista);
		int[] copiaLista = new int[1000];
		long tempo = startTime();
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			quickSort(lista, 0, 999);
		}
		tempo = finalTime(tempo);
		print.print("Depois: ");
		imprimirLista(copiaLista);
		System.out.println("       " + comparacao + "        " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Aleatoria " + "      " + 10000);
		lista = gerarListaAleatoria(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			quickSort(lista, 0, 9999);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "       " + movimentacao
				+ "          " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaAleatoria(100000);
		tempo = startTime();
		quickSort(lista, 0, 99999);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "     " + movimentacao
				+ "         " + tempo);

		System.out.print("                  " + 1000);
		lista = gerarListaDecrescente(1000);
		tempo = startTime();
		copiaLista = new int[1000];
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			quickSort(lista, 0, 999);
		}
		tempo = finalTime(tempo);
		System.out.println("       " + comparacao + "        " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Decrescente " + "    " + 10000);
		lista = gerarListaDecrescente(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			quickSort(lista, 0, 9999);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "       " + movimentacao
				+ "          " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaDecrescente(100000);
		tempo = startTime();
		quickSort(lista, 0, 99999);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "      " + movimentacao
				+ "         " + tempo);

		System.out.print("                  " + 1000);
		lista = gerarListaCrescente(1000);
		tempo = startTime();
		copiaLista = new int[1000];
		for (int i = 0; i < 100; i++) {
			for (int j = 0; j < lista.length; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			quickSort(lista, 0, 999);
		}
		tempo = finalTime(tempo);
		System.out.println("       " + comparacao + "        " + movimentacao
				+ "          " + tempo / 100);

		System.out.print(" Crescente " + "      " + 10000);
		lista = gerarListaCrescente(10000);
		copiaLista = new int[10000];
		tempo = startTime();
		for (int i = 0; i < 10; i++) {
			for (int j = 0; j < 10000; j++) {
				copiaLista[j] = lista[j];
			}
			comparacao = 0;
			movimentacao = 0;
			quickSort(lista, 0, 9999);
		}
		tempo = finalTime(tempo);
		System.out.println("      " + comparacao + "       " + movimentacao
				+ "          " + tempo / 10);

		System.out.print("                " + 100000);
		lista = gerarListaCrescente(100000);
		tempo = startTime();
		quickSort(lista, 0, 99999);
		tempo = finalTime(tempo);
		System.out.println("     " + comparacao + "      " + movimentacao
				+ "         " + tempo);
		System.out.println();
		System.out.println();

	}

	// Imprime o arquivo saida.txt que ser� salvo no diret�rio C:\
	public void imprimirLista(int[] lista) {
		for (int i = 0; i < lista.length; i++) {
			print.print(lista[i] + "  ");
		}
		print.println();
	}

	// Come�a a contagem do tempo em milissegundos
	public long startTime() {
		long tempo = System.currentTimeMillis();
		return tempo;
	}

	// Finaliza a contagem do tempo e calcula o tempo decorrido em milissegundos
	public long finalTime(long tempo) {
		long corrente = System.currentTimeMillis() - tempo;
		return corrente;
	}

	// M�todo que executa o algoritmo Bolha com Flag
	public int[] bolhaComFlag(int[] lista) {
		comparacao = 0;
		movimentacao = 0;
		int n = lista.length;
		boolean troca = false;
		for (int i = 1; i < n; i++) {
			for (int j = 0; j < (n - i); j++) {
				comparacao++;
				if (lista[j] > lista[j + 1]) {
					int aux = lista[j];
					lista[j] = lista[j + 1];
					lista[j + 1] = aux;
					movimentacao += 3;
					troca = true;
				}
			}
			comparacao++;
			if (!troca) {
				return lista;
			}
		}
		return lista;
	}

	// M�todo que executa o algoritmo Inser��o
	public int[] insercao(int[] lista) {
		comparacao = 0;
		movimentacao = 0;
		int n = lista.length;
		for (int i = 1; i < n; i++) {
			int pivo = lista[i];
			movimentacao++;
			int j = i - 1;
			comparacao += 2;
			while ((j >= 0) && (lista[j] > pivo)) {
				lista[j + 1] = lista[j];
				movimentacao++;
				comparacao += 2;
				j--;
			}
			lista[j + 1] = pivo;
			movimentacao++;
		}
		return lista;
	}

	// M�todo que executa o algoritmo Sele��o
	public int[] selecao(int[] lista) {
		comparacao = 0;
		movimentacao = 0;
		int n = lista.length;
		for (int i = 0; i < n - 1; i++) {
			int min = i;
			for (int j = i + 1; j < n; j++) {
				comparacao++;
				if (lista[j] < lista[min]) {
					min = j;
				}
			}
			int aux = lista[i];
			lista[i] = lista[min];
			lista[min] = aux;
			movimentacao += 3;
		}
		return lista;
	}

	// M�todo que executa o algoritmo Shellsort
	public int[] shellSort(int[] lista) {
		comparacao = 0;
		movimentacao = 0;
		int h = 1;
		int n = lista.length;
		comparacao++;
		while (h <= (n - 1)) {
			h = 3 * h + 1;
			comparacao++;
		}
		do {
			h = h / 3;
			for (int i = h; i < n; i++) {
				int pivo = lista[i];
				movimentacao++;
				int j = i - h;
				comparacao += 2;
				while ((j >= 0) && (lista[j] > pivo)) {
					lista[j + h] = lista[j];
					j = j - h;
					movimentacao++;
					comparacao += 2;
				}
				lista[j + h] = pivo;
				movimentacao++;
			}
			comparacao++;
		} while (h > 1);
		return lista;
	}

	// M�todo que executa o algoritmo Mergesort
	public int[] mergeSort(int[] lista, int inicio, int fim) {
		comparacao++;
		if (inicio < fim) {
			int meio = ((fim + inicio) / 2);
			comparacao++;
			if (inicio < meio) {
				mergeSort(lista, inicio, meio);
			}
			comparacao++;
			if ((meio + 1) < fim) {
				mergeSort(lista, meio + 1, fim);
			}
			merge(lista, inicio, meio, fim);
		}
		return lista;
	}

	// M�todo Merge do Mergesort
	public int[] merge(int[] lista, int inicio, int meio, int fim) {
		int i = inicio;
		int j = meio + 1;
		int k = 0;
		int[] aux = new int[fim - inicio];
		comparacao += 2;
		while ((i <= meio) && (j <= fim)) {
			comparacao += 3;
			if (lista[i] <= lista[j]) {
				aux[k] = lista[i];
				movimentacao++;
				i++;
			} else {
				aux[k] = lista[j];
				movimentacao++;
				j++;
			}
			k++;
		}
		comparacao++;
		if (i <= meio) {
			for (j = meio; j >= i; j--) {
				lista[fim - meio + j] = lista[j];
				movimentacao++;
			}
		}
		for (i = 0; i < k; i++) {
			lista[inicio + i] = aux[i];
			movimentacao++;
		}
		return lista;
	}

	// Metodo que executa o algoritmo Quicksort
	public int[] quickSort(int[] lista, int inicio, int fim) {
		comparacao++;
		if (inicio < fim) {
			int j = particao(lista, inicio, fim);
			comparacao++;
			if (inicio < (j - 1)) {
				quickSort(lista, inicio, j - 1);
			}
			comparacao++;
			if ((j + 1) < fim) {
				quickSort(lista, j + 1, fim);
			}
		}
		return lista;
	}

	// M�todo parti��o do Quicksort
	public int particao(int[] lista, int inicio, int fim) {
		// A formula mostrada abaixo faz com que o pivo seja escolhido
		// aleat�rio, mesmo que o inicio
		// da lista seja diferente de 0.
		int pivo = lista[(int) ((int) (fim - inicio + 1) * Math.random() + inicio)];
		movimentacao++;
		int i = inicio + 1;
		int j = fim;
		comparacao++;
		while (i <= j) {
			comparacao += 3;
			while ((i <= j) && (lista[i] <= pivo)) {
				i++;
				comparacao += 2;
			}
			comparacao++;
			while (lista[j] > pivo) {
				j--;
				comparacao++;
			}
			comparacao++;
			if (i < j) {
				int aux = lista[i];
				lista[i] = lista[j];
				lista[j] = aux;
				movimentacao += 3;
				i++;
				j--;
			}
		}
		lista[inicio] = lista[j];
		lista[j] = pivo;
		movimentacao += 2;
		return j;
	}
}