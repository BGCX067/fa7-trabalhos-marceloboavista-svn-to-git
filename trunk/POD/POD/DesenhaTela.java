import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.WindowConstants;




/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class DesenhaTela extends javax.swing.JFrame {
	private Desenha painelDesenha;
	private static Desenha painelDesenha2;
	private JLabel barraLabel2;
	private JLabel barraLabel1;
	private JMenuBar jMenuBar1;
	private JMenu jMenu1;
	private JMenu jMenu2;
	private JMenuItem jMenuItem;
	private JMenuItem jMenuItem1;
	private JMenuItem jMenuItem2;
	private JMenuItem jMenuItem3;
	private JMenuItem jMenuItem4;
	private JMenuItem jMenuItem5;
	private JMenuItem jMenuItem6;
	private JMenuItem jMenuItem7;
	private JMenuItem jMenuItem8;
	private JMenuItem jMenuItem9;
		private JMenuItem jMenuItem10;
	private JButton jButton1;
	private boolean populou=false;



	public DesenhaTela() {
		super();

		SortingAnimation barra = new SortingAnimation(100);
		initGUI(barra);
	}
	private void initGUI(final SortingAnimation barra) {
		try {
			{
				jMenuBar1 = new JMenuBar();
				setJMenuBar(jMenuBar1);
				jMenuBar1.setPreferredSize(new java.awt.Dimension(392, 22));
				{
					jMenu1 = new JMenu();
					jMenuBar1.add(jMenu1);
					jMenu1.setText("Menu");
					{
						jMenuItem1 = new JMenuItem();
						jMenu1.add(jMenuItem1);
						jMenuItem1.setText("Popular");
						
						jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								if(e.getActionCommand().equals("Popular")){	
						
									barra.populate();	
									painelDesenha.draw(barra.elements,painelDesenha.getGraphics(),-1);
						            populou = true;
						
								}
							
							}
						});
						jMenuItem2 = new JMenuItem();
						jMenu1.add(jMenuItem2);
						jMenuItem2.setText("Bolha");
						jMenuItem2.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								if(e.getActionCommand().equals("Bolha")){
									if(populou){
									final long[] elementos = new long[barra.elements.length];
									for(int i=0; i < barra.elements.length; i++){
										elementos[i] = barra.elements[i];		
									}
									Thread bolha = new Thread(){
										public void run(){
											long Ti = System.currentTimeMillis();
											barra.bolha(elementos);									
											long Tf = System.currentTimeMillis();
											long totalTempo = Tf-Ti;
											JOptionPane.showMessageDialog(null,"Tempo: "+totalTempo+" milisegundos","Tempo", JOptionPane.INFORMATION_MESSAGE);
										}
		
									};
									
									bolha.setName("Bolha Sort");
									bolha.start();
																	
									}else{
									JOptionPane.showMessageDialog(null,"Primeiro popule o array!!","Aviso", JOptionPane.ERROR_MESSAGE);
									}
								}
								
							}
						});
						jMenuItem3 = new JMenuItem();
						jMenu1.add(jMenuItem3);
						jMenuItem3.setText("QuickSort");
						jMenuItem3.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								if(e.getActionCommand().equals("QuickSort")){
									if(populou){
									final long[] elementos = new long[barra.elements.length];
									for(int i=0; i < barra.elements.length; i++){
										elementos[i] = barra.elements[i];		
									}
									Thread quick = new Thread(){
										public void run(){
											long Ti = System.currentTimeMillis();
											barra.qsort(elementos,0,elementos.length-1);									
											long Tf = System.currentTimeMillis();
											long totalTempo = Tf-Ti;
											JOptionPane.showMessageDialog(null,"Tempo: "+totalTempo+" milisegundos","Tempo", JOptionPane.INFORMATION_MESSAGE);
										}
		
									};
									
									quick.setName("Quick Sort");
									quick.start();
									
									}else{
									JOptionPane.showMessageDialog(null,"Primeiro popule o array!!","Aviso", JOptionPane.ERROR_MESSAGE);
									}
								}
								
							}
						});
						jMenuItem4 = new JMenuItem();
						jMenu1.add(jMenuItem4);
						jMenuItem4.setText("Selection Sort");
						jMenuItem4.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								if(e.getActionCommand().equals("Selection Sort")){
									if(populou){
									final long[] elementos = new long[barra.elements.length];
									for(int i=0; i < barra.elements.length; i++){
										elementos[i] = barra.elements[i];		
									}
									Thread selection = new Thread(){
										public void run(){
											long Ti = System.currentTimeMillis();
											barra.selectionSort(elementos);
																				
											long Tf = System.currentTimeMillis();
											long totalTempo = Tf-Ti;
											JOptionPane.showMessageDialog(null,"Tempo: "+totalTempo+" milisegundos","Tempo", JOptionPane.INFORMATION_MESSAGE);
										}
		
									};
									
									selection.setName("Selection Sort");
									selection.start();
									
								    }else{
									JOptionPane.showMessageDialog(null,"Primeiro popule o array!!","Aviso", JOptionPane.ERROR_MESSAGE);
									}
								}
								
							}
						});
						//Aki cumeca eu
						jMenuItem5 = new JMenuItem();
						jMenu1.add(jMenuItem5);
						jMenuItem5.setText("Insertion Sort");
						jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								if(e.getActionCommand().equals("Insertion Sort")){
									if(populou){
									final long[] elementos = new long[barra.elements.length];
									for(int i=0; i < barra.elements.length; i++){
										elementos[i] = barra.elements[i];		
									}
									Thread insercao = new Thread(){
										public void run(){
											long Ti = System.currentTimeMillis();
											barra.insercao(elementos);
																				
											long Tf = System.currentTimeMillis();
											long totalTempo = Tf-Ti;
											JOptionPane.showMessageDialog(null,"Tempo: "+totalTempo+" milisegundos","Tempo", JOptionPane.INFORMATION_MESSAGE);
										}
		
									};
									
									insercao.setName("Insertion Sort");
									insercao.start();
									
									
									
									}else{
									JOptionPane.showMessageDialog(null,"Primeiro popule o array!!","Aviso", JOptionPane.ERROR_MESSAGE);
									}
								}
								
							}
						});
						jMenuItem6 = new JMenuItem();
						jMenu1.add(jMenuItem6);
						jMenuItem6.setText("Shell Sort");
						jMenuItem6.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								if(e.getActionCommand().equals("Shell Sort")){
									if(populou){
									final long[] elementos = new long[barra.elements.length];
									for(int i=0; i < barra.elements.length; i++){
										elementos[i] = barra.elements[i];		
									}
									Thread shell = new Thread(){
										public void run(){
											long Ti = System.currentTimeMillis();
											barra.shellSort(elementos);
																				
											long Tf = System.currentTimeMillis();
											long totalTempo = Tf-Ti;
											JOptionPane.showMessageDialog(null,"Tempo: "+totalTempo+" milisegundos","Tempo", JOptionPane.INFORMATION_MESSAGE);
										}
		
									};
									
									shell.setName("Shell Sort");
									shell.start();
									
									
									}else{
									JOptionPane.showMessageDialog(null,"Primeiro popule o array!!","Aviso", JOptionPane.ERROR_MESSAGE);
									}
								}
								
							}
						});
						jMenuItem7 = new JMenuItem();
						jMenu1.add(jMenuItem7);
						jMenuItem7.setText("Merge Sort");
						jMenuItem7.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								if(e.getActionCommand().equals("Merge Sort")){
									if(populou){
									final long[] elementos = new long[barra.elements.length];
									for(int i=0; i < barra.elements.length; i++){
										elementos[i] = barra.elements[i];		
									}
									
									Thread merge = new Thread(){
										public void run(){
											long Ti = System.currentTimeMillis();
											barra.mergeSort(elementos,0,elementos.length-1);
																				
											long Tf = System.currentTimeMillis();
											long totalTempo = Tf-Ti;
											JOptionPane.showMessageDialog(null,"Tempo: "+totalTempo+" milisegundos","Tempo", JOptionPane.INFORMATION_MESSAGE);
										}
		
									};
									
									merge.setName("Merge Sort");
									merge.start();
									
									}else{
									JOptionPane.showMessageDialog(null,"Primeiro popule o array!!","Aviso", JOptionPane.ERROR_MESSAGE);
									}
								}
								
							}
						});
						jMenuItem8 = new JMenuItem();
						jMenu1.add(jMenuItem8);
						jMenuItem8.setText("Bucket Sort");
						jMenuItem8.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								if(e.getActionCommand().equals("Bucket Sort")){
									if(populou){
									final long[] elementos = new long[barra.elements.length];
									for(int i=0; i < barra.elements.length; i++){
										elementos[i] = barra.elements[i];		
									}
									
									Thread Bucket = new Thread(){
										public void run(){
											long Ti = System.currentTimeMillis();
											barra.bucketSort(elementos, 101);
																				
											long Tf = System.currentTimeMillis();
											long totalTempo = Tf-Ti;
											JOptionPane.showMessageDialog(null,"Tempo: "+totalTempo+" milisegundos","Tempo", JOptionPane.INFORMATION_MESSAGE);
										}
		
									};
									
									Bucket.setName("Bucket Sort");
									Bucket.start();
									
									}else{
									JOptionPane.showMessageDialog(null,"Primeiro popule o array!!","Aviso", JOptionPane.ERROR_MESSAGE);
									}
								}
								
							}
						});
						jMenuItem9 = new JMenuItem();
						jMenu1.add(jMenuItem9);
						jMenuItem9.setText("Counting Sort");
						jMenuItem9.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								if(e.getActionCommand().equals("Counting Sort")){
									if(populou){
									final long[] elementos = new long[barra.elements.length];
									for(int i=0; i < barra.elements.length; i++){
										elementos[i] = barra.elements[i];		
									}
									
									Thread Counting = new Thread(){
										public void run(){
											long Ti = System.currentTimeMillis();
											//barra.countingSort(elementos, 1000);
											barra.countingSort(elementos, 101);
											long Tf = System.currentTimeMillis();
											long totalTempo = Tf-Ti;
											JOptionPane.showMessageDialog(null,"Tempo: "+totalTempo+" milisegundos","Tempo", JOptionPane.INFORMATION_MESSAGE);
										}
		
									};
									
									Counting.setName("Counting Sort");
									Counting.start();
									
									}else{
									JOptionPane.showMessageDialog(null,"Primeiro popule o array!!","Aviso", JOptionPane.ERROR_MESSAGE);
									}
								}
								
							}
						});
						jMenuItem10 = new JMenuItem();
						jMenu1.add(jMenuItem10);
						jMenuItem10.setText("Heap Sort");
						jMenuItem10.addActionListener(new java.awt.event.ActionListener() {
							public void actionPerformed(java.awt.event.ActionEvent e) {
								if(e.getActionCommand().equals("Heap Sort")){
									if(populou){
									final long[] elementos = new long[barra.elements.length];
									for(int i=0; i < barra.elements.length; i++){
										elementos[i] = barra.elements[i];		
									}
									
									Thread Heap = new Thread(){
										public void run(){
											long Ti = System.currentTimeMillis();
											//barra.countingSort(elementos, 1000);
											barra.heapSort(elementos);
											long Tf = System.currentTimeMillis();
											long totalTempo = Tf-Ti;
											JOptionPane.showMessageDialog(null,"Tempo: "+totalTempo+" milisegundos","Tempo", JOptionPane.INFORMATION_MESSAGE);
										}
		
									};
									
									Heap.setName("Heap Sort");
									Heap.start();
									
									}else{
									JOptionPane.showMessageDialog(null,"Primeiro popule o array!!","Aviso", JOptionPane.ERROR_MESSAGE);
									}
								}
								
							}
						});
						
					}
				}
			}
	
			
			{
			
				
				painelDesenha = new Desenha();
				getContentPane().add(painelDesenha);
				painelDesenha.setBounds(7, 42, 329, 238);
				painelDesenha.setForeground(new java.awt.Color(255,128,64));								
				painelDesenha.setBackground(new java.awt.Color(255,255,255));			
			}
			{	
				
				painelDesenha2 = new Desenha();
				painelDesenha2.setBounds(357, 42, 329, 238);
				painelDesenha2.setForeground(new java.awt.Color(255,255,255));
				getContentPane().add(painelDesenha2);
				painelDesenha2.setBackground(new java.awt.Color(255,255,255));
			}
			{
				barraLabel1 = new JLabel();
				getContentPane().add(barraLabel1);
				barraLabel1.setText("Barras desordenadas:");
				barraLabel1.setBounds(7, 7, 168, 28);
			}
			{
				barraLabel2 = new JLabel();
				getContentPane().add(barraLabel2);
				barraLabel2.setText("Barras ordenadas:");
				barraLabel2.setBounds(357, 7, 182, 28);
			}
		
			setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			getContentPane().setLayout(null);
			this.setTitle("Ordena��o");
			pack();
			this.setSize(700, 340);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	
	public static void tempo(int size){
        int i = size > 100 ? 10 : 100;
        try
        {
            System.out.println(Thread.currentThread().getName());
        	Thread.sleep(i);
            
        }
        catch(InterruptedException interruptedexception) { 
        	
        	
        }

    }
	public static void update(long[] vetor,int posJ){
		 painelDesenha2.draw(vetor,painelDesenha2.getGraphics(), posJ);
		 tempo(vetor.length);
	}
}
