
public class Ordena2 {
	public int[] sort(int a[], int m){
	    int temp[] = new int[m];
	  
	    //initialize
	    for (int i = 0; i < m; i++)
	        temp[i] = 0;
	 
	    for (int i = 0; i < a.length; i++)
	        temp[a[i]] += 1;
	 
	    // sort (overwrite a)
	    int pos = 0;
	    for (int i = 0; i < temp.length; i++){
	        int count = temp[i];
	        for (int j = 0; j < count; j++){
	            a[pos] = i;
	            pos++;
	        }
	    }
	    return a;
	 
	}
	
	    public static void bucketSort (int[] a, int m)
	    {
		int[] buckets = new int[m];

		for (int j = 0; j < m; ++j)
		    buckets [j] = 0;
		for (int i = 0; i < a.length; ++i)
		    ++buckets [a [i]];
		for (int i = 0, j = 0; j < m; ++j)
		    for (int k = buckets [j]; k > 0; --k)
			a [i++] = j;
	    }
		
	
}
