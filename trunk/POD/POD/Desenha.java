import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.Graphics;
import java.util.Vector;

import javax.security.auth.RefreshFailedException;
import javax.swing.JPanel;

public class Desenha extends JPanel {
	private Color[] cores = {Color.GREEN, Color.BLACK, Color.ORANGE, Color.RED, Color.BLUE, Color.CYAN, Color.MAGENTA,Color.YELLOW};
	private long[] barra;
	private Color[] corDaBarra;
	private boolean teste=false;
	
	public void draw(long [] valor,Graphics t,int posJ){
		int tamanho = valor.length;
		barra = new long[tamanho];
		barra = valor;
		corDaBarra = new Color[tamanho];
		
		double maximo = 0;
		for (int cont=0; cont<tamanho; cont++) {
			if (valor[cont] > maximo) {
				maximo = valor[cont];
			}
		}
		
		for (int cont=0; cont<tamanho; cont++) {
			
			barra[cont] = (int) (100* (valor[cont]/maximo) );
			corDaBarra[cont] = Color.BLACK;
			if(cont == posJ){
				corDaBarra[cont] = Color.RED;
			}
			
	
		}
		teste = true;
		paint(t);
	
	}
	
	public void paint(Graphics g){
		
		super.paint(g);
		
		if(teste==true){
	
		int larguraJanela = getWidth();
		int alturaJanela  = getHeight();
		int baseBarras = alturaJanela - 20;
		int quantidadeBarras = barra.length;
		int tamanhoItem = (larguraJanela / quantidadeBarras );
		int espacoEsquerda = (int) (5 * tamanhoItem);
		int larguraBarra = (int) (0.8 * tamanhoItem);
		int alturaBarraMax = alturaJanela - 70 ; 
		
		for (int cont=0; cont< quantidadeBarras; cont++) {
			
			int altura = (int) ((barra[cont] * alturaBarraMax)/ 100);
			int x = cont * tamanhoItem + espacoEsquerda;
			int y = baseBarras - altura;
			//String tam = Long.toString(barra[cont]);
			g.setColor(corDaBarra[cont]);
			g.fillRect(x, y, larguraBarra, altura);
			Font f = new Font("Arial", Font.BOLD, 6);
			g.setFont(f);
			//g.drawString(tam,x,baseBarras+15);
		}
		 g.dispose();
	   	
		}
		
	}
	

	
}
