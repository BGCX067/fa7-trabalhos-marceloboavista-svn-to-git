import java.io.FileWriter;
import java.util.*;

public class ordena{
    
    //Conte�do do arquivo saida.txt
    public static String conteudo = "";
    
    //O n�mero de movimenta��es que inicia com 0 e vai resetando a cada m�todo
    public static int movimentacoes = 0;
    
    //Se a lista foi gerada aleat�ria, decrescente ou crescente
    public static String tipoDeLista = "";
    
    //Qual o tamanho da minha lista
    public static int tamanhoDaLista = 0;
    
    //Qual m�todo estou utilizando
    public static String nomeDoMetodo = "";
    
    //Contagem inicial do tempo levado para ordenar uma lista
    public static long inicio = 0;
    
    //Tempo levado para ordenar uma lista
    public static long fim = 0;

    public static void main( String[] args ){
     
        tamanhoDaLista = Integer.parseInt( args[0] );
        int numeroDaLista = Integer.parseInt( args[1] );
        
    	int lista[] = gerarLista( numeroDaLista );
        
        int[] listaCounting = lista .clone();
        int[] listaBucket = lista .clone();
        int[] listaRadix = lista .clone();
        int[] listaHeap = lista .clone();
        
        ordenarLista( listaCounting, 1 );        
        ordenarLista( listaBucket, 2 );        
        ordenarLista( listaRadix, 3 );
        ordenarLista( listaHeap, 4 );
        
        if( tamanhoDaLista < 100 ){
            System.out.println( "O arquivo foi gerado com sucesso!" );
            conteudo += "------------------------------------------";
            gerarArquivo( conteudo );
        }
        
    }
    
    public static int[] gerarLista( int numeroDaLista ){
        
        int[] lista = new int[tamanhoDaLista];
        Random r = new Random();
        
        switch( numeroDaLista ){
            
            //Lista Aleat�ria
            case 1:
            default:
                tipoDeLista = "Aleat�ria";
                for( int i = 0; i < tamanhoDaLista; i++ ){
                    lista[i] = r.nextInt( 10 * tamanhoDaLista );
                }
            break;
            
            //Lista Descrescente
            case 2:     
                tipoDeLista = "Decrescente";
                
                int listAux = tamanhoDaLista;
                
                for( int i = 0; i < tamanhoDaLista; i++ ){
                    lista[i] = listAux;
                    listAux--;
                }                
            break;
            
            //Lista Crescente
            case 3:        
                tipoDeLista = "Crescente";
                
                int cont = 0;
                
                for( int i = 0; i < tamanhoDaLista; i++ ){
                    lista[i] = cont++;
                }                
            break;
            
        }        
        
        return lista;
        
    }
    
    public static void ordenarLista( int[] lista, int numeroDoMetodo ){
        
        int listaDesordenada[] = lista.clone();
        int[] listaOrdenada = new int[lista.length];
        inicio = System.currentTimeMillis();
        movimentacoes = 0;
        
        switch( numeroDoMetodo ){
            
            case 1:
            default:                
                nomeDoMetodo = "Counting Sort";
                listaOrdenada = countingSort( lista );
            break;
            
            case 2:
                nomeDoMetodo = "Bucket Sort";
                listaOrdenada = bucketSort( lista );
            break;
            
            case 3:
                nomeDoMetodo = "Radix Sort";
                listaOrdenada = radixSort( lista );
            break;
            
            case 4:
                nomeDoMetodo = "Heap Sort";
                listaOrdenada = heapSort( lista );
            break;
            
        }
        
        fim = System.currentTimeMillis() - inicio;
        
        imprimirInformacoes( fim );
        
        if( lista.length < 100 ){
            ordena.conteudo += nomeDoMetodo + "\n\r" + "\n\r";
            ordena.conteudo += "Antes: " + imprimirLista( listaDesordenada ) + "\n\r" + "\n\r";
            ordena.conteudo += "Depois: " + imprimirLista( listaOrdenada ) + "\n\r" + "\n\r" + "\n\r" + "\n\r";
        }
        
    }
    
    public static int[] countingSort( int[] lista ){
       
        
        int maiorNumeroDaLista = lista[0], menorNumeroDaLista = lista[0];

        for( int i = 1; i < lista.length; i++ ){
            if( lista[i] > maiorNumeroDaLista ){
                maiorNumeroDaLista = lista[i];
                movimentacoes++;
            }    
            else if( lista[i] < menorNumeroDaLista ){
                menorNumeroDaLista = lista[i];
                movimentacoes++;
            }
        }

        int intervalo = maiorNumeroDaLista - menorNumeroDaLista + 1;

        int[] counts = new int[intervalo];

        for( int i = 0; i < lista.length; i++ ){
            counts[lista[i] - menorNumeroDaLista]++;
            movimentacoes++;
        }

        int outputPos = 0;

        for( int i = 0; i < intervalo; i++ ){
            for( int j = 0; j < counts[i]; j++ ){
                lista[outputPos] = i + menorNumeroDaLista;
                outputPos++;
                movimentacoes++;
            }
        }
        
        return lista;

    }
    
    public static int[] bucketSort( int[] lista ){
        
        int maiorNumero = 0;
        
        for( int l = 0; l < lista.length; l++ ){
            if( lista[l] > maiorNumero ){
                maiorNumero = lista[l];
            }
        }
        
        int[] buckets = new int[maiorNumero + 1];

        for( int j = 0; j <= maiorNumero; ++j )
            buckets[j] = 0;
        for( int i = 0; i < lista.length; ++i)
            ++buckets[lista[i]];
        for( int i = 0, j = 0; j <= maiorNumero; ++j )
            for (int k = buckets[j]; k > 0; --k)
                lista[i++] = j;
                movimentacoes++;
        
        return lista;
        
    }
    
    public static int[] radixSort( int lista[] ){
        
        int maximoDeCaracteres = 4;
        int[] listaAux = new int[lista.length];
        int[] listaAuxOriginal = listaAux;

        int rshift = 0;
        
        for( int mask = ~( -1 << maximoDeCaracteres ); mask != 0; mask <<= maximoDeCaracteres, rshift += maximoDeCaracteres ){

            int[] cntarray = new int[1 << maximoDeCaracteres];

            for( int p = 0; p < lista.length; ++p ){
                int key = ( lista[p] & mask ) >> rshift;
                ++cntarray[key];
            }

            for( int i = 1; i < cntarray.length; ++i ){
                cntarray[i] += cntarray[i-1];
            }

            for( int p = lista.length-1; p >= 0; --p ){
                int key = ( lista[p] & mask ) >> rshift;
                --cntarray[key];
                listaAux[cntarray[key]] = lista[p];
                movimentacoes++;
            }

            int[] temp = listaAux; 
            listaAux = lista; 
            lista = temp;
            
        }
        
        if( lista == listaAuxOriginal ){
            System.arraycopy( lista, 0, listaAux, 0, lista.length );
        }

        return listaAux;
        
    }
    
    public static int[] heapSort( int[] lista ){
        
        int tamanho = lista.length;
        
        for( int k = tamanho/2; k > 0; k-- ){    
            heapDown( lista, k, tamanho );
            movimentacoes++;
        }
        do {
            int T = lista[0];
            lista[0] = lista[tamanho - 1];
            lista[tamanho - 1] = T;
            tamanho = tamanho - 1;
            heapDown( lista, 1, tamanho );
            movimentacoes++;
        } while( tamanho > 1 );
        
        return lista;
        
    }
    
    public static void heapDown( int lista[], int k, int tamanho ){
        
        int T = lista[k - 1];
            
        while( k <= tamanho/2 ){

            int j = k + k;

            if( ( j < tamanho ) && ( lista[j - 1] < lista[j] ) ){
                j++;
            }

            if( T >= lista[j - 1] ){
                break;
            } 
            else {
                lista[k - 1] = lista[j - 1];
                k = j;
            }

        }

        lista[k - 1] = T;       
        
    }
    
    public static String imprimirLista( int lista[] ) {
        
        String strLista = "";
        
        for( int i = 0; i < lista.length; i++ ){
            strLista += lista[i] + " ";
        }
        
        return strLista;
        
    }
    
    public static void imprimirInformacoes( long tempo ){
        
        String informacoes = "";
        
        informacoes += nomeDoMetodo + "(" + tamanhoDaLista + " elementos - " + tipoDeLista +  ")" + "\n\r" + "\n\r";
        informacoes += "Movimenta��es: " + movimentacoes + "\n\r" + "\n\r";
        informacoes += "Tempo: " + tempo + " ms" + "\n\r" + "\n\r" + "\n\r" + "\n\r";
        
        System.out.println( informacoes );
        
    }
    
    public static void gerarArquivo( String conteudo ) {
        try {
            FileWriter arquivo = new FileWriter( "saida.txt" );
            char[] carac = conteudo.toCharArray();
            for( int i = 0; i < carac.length; i++ ){
                arquivo.write( carac[i] );
            }
            arquivo.close();
        } catch ( Exception e ) {
            e.getMessage();
        }
    }

}
