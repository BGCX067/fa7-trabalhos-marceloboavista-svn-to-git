/* Trabalho de POD: Compara��o dos m�todos de Ordena��o, Couting Sort, Bucket Sort, Radix Sort e Heap Sort.
 * Prof.: Glauber
 * Equipe: Lucas Apoena, Julio Pontes, Jos� Marcelo e Alexssandro;
*/

/* 
 *	VITOR!!!!
 *	Muda a estrutura do c�digo!!! Num vai entregar igual n!
*/







import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class ordena {

	// Guarda o num�ro de movimenta��es
	public static long metMovimentacoes;

	public static String nomeLista;
	public static String nomeMetodo;
	public static String Str_Txt = "";
	public static int listaIncial[];
	public static int listaAux[];

	// Tamanho de lista limite para que gere o arquivo sa�da
	public static int tamLimite = 100;

	public static void main(String[] args) {
		int tamanho = Integer.parseInt(args[0]);
		int tipo = Integer.parseInt(args[1]);
		listaIncial = new int[tamanho];// 500 1000 8000 64000
		listaAux = new int[listaIncial.length];

		gerarLista(tipo, listaIncial);

		for (int i = 1; i <= 4; i++) {
			ordenaListas(listaIncial, i);
		}
		if (listaIncial.length < tamLimite) {
			System.out.println("O arquivo sa�da.txt foi gerado!");
			escreveSaida(Str_Txt);
		}
	}

	// Gerar Listas:
	public static void gerarLista(int tipoLista, int ordLista[]) {
		/*
		 * M�todo para gerar listas: 1 - Aleat�rias; 2 - Decrescentes 3 -
		 * Crescentes; Parametros: ordLista -> Lista tipoLista -> Tipo da lista
		 * escolhida Retorno: String tipoNome -> tipo de lista gerada
		 */
		switch (tipoLista) {
		case 1: {
			nomeLista = "Aleat�ria";
			Random r = new Random();
			for (int i = 0; i < ordLista.length; i++) {
				ordLista[i] = r.nextInt(10 * ordLista.length);
			}
			break;
		}
		case 2: {
			nomeLista = "Decrescente";
			int aux = ordLista.length;
			for (int i = 0; i < ordLista.length; i++) {
				ordLista[i] = aux;
				aux--;
			}
			break;
		}
		case 3: {
			nomeLista = "Crescente";
			for (int i = 0; i < ordLista.length; i++) {
				ordLista[i] = i + 1;
			}
			break;
		}
		default:
			nomeLista = "Tipo de lista incorreto";
			;
			break;
		}
	}

	// Met�do utilizado para copiar a lista inicial
	public static void cloner(int ordLista[]) {
		for (int i = 0; i < ordLista.length; i++) {
			listaAux[i] = ordLista[i];
		}
	}

	// Imprimir Lista
	public static String imprimeLista(int ordLista[], String texto) {
		String lista = texto;
		for (int i = 0; i < ordLista.length; i++) {
			lista += " " + ordLista[i];
		}
		return lista;
	}

	// Gerar o arquivo sa�da.txt
	public static void escreveSaida(String texto) {
		try {
			FileWriter writer = new FileWriter("saida.txt");
			char[] c = texto.toCharArray();
			for (int cont = 0; cont < c.length; cont++) {
				writer.write(c[cont]);
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// M�todo para organizar a escolha dos m�todos de ordena��o
	public static void ordenaListas(int ordLista[], int metodo) {
		metMovimentacoes = 0;

		/*
		 * O Java 1.4 parece n�o aceitar o clone() para realizar copias de
		 * Matriz de int[], s� se transformar em um objeto optamos ent�o em
		 * criar o m�todo cloner que tamb�m realiza a copia de uma matriz.
		 */
		// int listaAux[] = ordLista.clone(); // Parece que s� copia int[] em
		// vers�es superior a 1.4
		cloner(listaIncial);
		int listaAux[] = ordena.listaAux;

		long inicio = System.currentTimeMillis();
		switch (metodo) {
		case 1:
			//metCoutingSort(listaAux);
			nomeMetodo = "Counting Sort ";
			break;
		case 2:
			//bucketSort(listaAux);
			 metBucketSort(listaAux);
			nomeMetodo = "Bucket Sort ";
			break;
		case 3:
			metRadixSort(listaAux);
			nomeMetodo = "Radix Sort  ";
			break;
		case 4:
			// metShellsort(listaAux);
			nomeMetodo = "Heap Sort ";
			break;
		default:
			System.out.println("Nenhum m�todo fora escolhido");
			break;
		}

		// Se o tamanho da lista for maior ou igual h� 100
		if (ordLista.length >= tamLimite) {
			System.out.println(nomeMetodo + "(" + ordLista.length
					+ " elementos - " + nomeLista + ")");
			System.out.println("Movimenta��es: " + metMovimentacoes);
			System.out.println("Tempo: "
					+ (System.currentTimeMillis() - inicio) + " ms");
			System.out.println();
		} else {
			String listaInicial = imprimeLista(ordLista, "\n" + "# Antes: ");
			String listaOrdenada = imprimeLista(listaAux, "\n" + "# Depois: ");
			Str_Txt += "\n\r" + "\n\r" + nomeMetodo + "\n\r" + listaInicial
					+ "\n\r" + listaOrdenada + "\n\r";
		}
	}

	// M�todos de ordena��o

	// M�todo Couting Sort
	public static void metCoutingSort(int ordLista[]) {

		int max = ordLista[0], min = ordLista[0];
		metMovimentacoes += 2;
		int n = ordLista.length;

		// Para encontrar o intervalo, busca os valores minimo e m�ximo que
		// est�o no vetor
		for (int i = 1; i < n; ++i) {
			if (ordLista[i] > max) {
				max = ordLista[i];
				metMovimentacoes++;
			} else if (ordLista[i] < min) {
				min = ordLista[i];
				metMovimentacoes++;
			}
		}
		int tamIntervalo = max - min + 1;
		// Inicializa um vetor auxiliar;
		int[] Aux = new int[tamIntervalo];
		// Contar as ocorr�ncias dos valores na lista
		for (int i = 0; i < n; ++i) {
			Aux[ordLista[i] - min]++;
			metMovimentacoes++;
		}
		// Gera o resultado
		int j = 0;
		for (int i = min; i <= max; i++) {
			for (int z = 0; z < Aux[i - min]; z++) {
				metMovimentacoes++;
				ordLista[j++] = i;
			}
		}
	}

/* Ta dando pau ainda to mechendo nele... se tu arranjar a solu��o me passa!
	public static void metBucketSort(int ordLista[]) {
		int k = ordLista[0];
		
		// Pega o maior valor do intervalo
		for (int i = 1; i < ordLista.length; i++) {
			if (ordLista[i] > k) {
				k = ordLista[i];
			}
		}
		LinkedList bucket[] = new LinkedList[2];
		//List bucket = new LinkedList[k+1];
		// Inicializar o bucket com a LinkedList
	//	for (int i = 0; i < ordLista.length; i++) {
		//	bucket[i] = null;
		//}
		for (int i = ordLista.length-1; i > 0; i--) {
		//	int indice = (int) (ordLista[i] * (ordLista.length/(k+1))); //(ordLista[i]/k) * ordLista.length;
			bucket[1].add(new Integer(ordLista[i]));
		}
		
		Para (i = n � 1 at� 0) (Passo n-1) {// Construindo as listas ligadas;
			Insira L[i] no inicio da lista  ligada apontada por
			B[  L[i] * n/(k+1) ]. //  Quer dizer ch�o, ou seja tem que deixar em inteiro  
		}

		
	
		}

		for (i = 0; i < ordLista.length; i++) {
			// bucket[(int) (ordLista.length * ordLista[i])].add(new
			// Integer(ordLista[i]));
			// buckets[(int) (n * array[i].getKey())].insert(array[i]);
			int indice = ordLista[i] * (ordLista.length / k);

			bucket[indice].add(new Integer(ordLista[i]));
		}
		for (i = 0; i < ordLista.length; i++) {
			Collections.sort(bucket[i]);
			for (Iterator p = bucket[i].iterator(); p.hasNext();) {
				ordLista[j] = ((Integer) p.next()).intValue();
				j++;
			}
		
	}
*/

	public static void metRadixSort(int[] ordLista) {

		int[] t = new int[ordLista.length];

		int r = 4;
		int b = 32;

		int[] count = new int[1 << r];
		int[] pref = new int[1 << r];

		int groups = (int) Math.ceil((double) b / (double) r);

		int mask = (1 << r) - 1;

		for (int c = 0, shift = 0; c < groups; c++, shift += r) {

			for (int j = 0; j < count.length; j++) {

				count[j] = 0;
			}

			for (int i = 0; i < ordLista.length; i++) {

				count[(ordLista[i] >> shift) & mask]++;
			}

			pref[0] = 0;

			for (int i = 1; i < count.length; i++) {

				pref[i] = pref[i - 1] + count[i - 1];
			}

			for (int i = 0; i < ordLista.length; i++) {

				t[pref[(ordLista[i] >> shift) & mask]++] = ordLista[i];
			}

			System.arraycopy(t, 0, ordLista, 0, t.length);
		}

	}

}
