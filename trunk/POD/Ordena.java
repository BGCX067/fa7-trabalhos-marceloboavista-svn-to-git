import java.io.FileWriter;
import java.util.Arrays;
import java.util.Random;

public class Ordena{
    
    public static String conteudo = "";

    public static void main( String[] args ){
     
        int tamanhoDaLista = Integer.parseInt( args[0] );
        int tipoDaLista = Integer.parseInt( args[1] );
        
    	int lista[] = gerarLista( tamanhoDaLista, tipoDaLista );
        
    	int[] listaCounting = lista.clone();
    	int[] listaBucket = lista.clone();
    	int[] listaRadix = lista.clone();
    	int[] listaHeap = lista.clone();
    	
        ordenarLista( listaCounting, 1 );
        ordenarLista( listaBucket, 2 );
        ordenarLista( listaRadix, 3 );
        ordenarLista( listaHeap, 4 );
        
        if( tamanhoDaLista < 100 ){
            System.out.println( "O arquivo foi gerado com sucesso!" );
            Ordena.conteudo += "------------------------------------------";
            gerarArquivo( Ordena.conteudo );
        }
        
    }
    
    public static int[] gerarLista( int tamanhoDaLista, int tipoDaLista ){
        
        int[] lista = new int[tamanhoDaLista];
        Random r = new Random();
        
        switch( tipoDaLista ){
            
            //Lista Aleatória
            case 1:
            default:                
                for( int i = 0; i < tamanhoDaLista; i++ ){
                    lista[i] = r.nextInt( 10 * tamanhoDaLista );
                }
            break;
            
            //Lista Descrescente
            case 2:                
                int listAux = tamanhoDaLista;
                
                for( int i = 0; i < tamanhoDaLista; i++ ){
                    lista[i] = listAux;
                    listAux--;
                }                
            break;
            
            //Lista Crescente
            case 3:                
                int cont = 0;
                
                for( int i = 0; i < tamanhoDaLista; i++ ){
                    lista[i] = cont++;
                }                
            break;
            
        }        
        
        return lista;
        
    }
    
    public static void ordenarLista( int[] lista, int tipoDaLista ){
        
        String nomeDoMetodo;
        int listaDesordenada[] = lista.clone();
        long inicio = System.currentTimeMillis();        
        int[] listaOrdenada = new int[lista.length];
        
        switch( tipoDaLista ){
            
            case 1:
            default:
                nomeDoMetodo = "Counting Sort";
                listaOrdenada = countingSort( lista );
            break;
            
            case 2:
                nomeDoMetodo = "Bucket Sort";
                listaOrdenada = bucketSort( lista );
            break;
            
            case 3:
                nomeDoMetodo = "Radix Sort";
                listaOrdenada = radixSort( lista );
            break;
            
            case 4:
                nomeDoMetodo = "Heap Sort";
                listaOrdenada = heapSort( lista );
            break;
            
        }
        
        if( lista.length < 100 ){
            ordena.conteudo += nomeDoMetodo + "\n\r" + "\n\r";
            ordena.conteudo += "Antes: " + imprimirLista( listaDesordenada ) + "\n\r" + "\n\r";
            ordena.conteudo += "Depois: " + imprimirLista( listaOrdenada ) + "\n\r" + "\n\r" + "\n\r" + "\n\r";
        }
        
        System.out.println( ( System.currentTimeMillis() - inicio ) + " milisegundos" );
        
        //return listaOrdenada;
    
    }
    
    public static int[] countingSort( int[] lista ){
       
        int maiorNumeroDaLista = lista[0], menorNumeroDaLista = lista[0];

        for( int i = 1; i < lista.length; i++ ){
            if( lista[i] > maiorNumeroDaLista ){
                maiorNumeroDaLista = lista[i];
            }    
            else if( lista[i] < menorNumeroDaLista ){
                menorNumeroDaLista = lista[i];
            }
        }

        int intervalo = maiorNumeroDaLista - menorNumeroDaLista + 1;

        int[] counts = new int[intervalo];

        for( int i = 0; i < lista.length; i++ ){
            counts[lista[i] - menorNumeroDaLista]++;
        }

        int outputPos = 0;

        for( int i = 0; i < intervalo; i++ ){
            for( int j = 0; j < counts[i]; j++ ){
                lista[outputPos] = i + menorNumeroDaLista;
                outputPos++;
            }
        }
        
        return lista;

    }
   
//bucket do anderson

public static void bucketSort (int[] a, int m)
	    {
		int[] buckets = new int[m];

		for (int j = 0; j < m; ++j)
		    buckets [j] = 0;
		for (int i = 0; i < a.length; ++i)
		    ++buckets [a [i]];
		for (int i = 0, j = 0; j < m; ++j)
		    for (int k = buckets [j]; k > 0; --k)
			a [i++] = j;
	    }

   
    public static int[] bucketSort( int[] lista ){
        
        int maiorNumeroDaLista = 0;
        
        for( int k = 0; k < lista.length; k++ ){
            if( lista[k] > maiorNumeroDaLista ){
                maiorNumeroDaLista = lista[k];
            }
        }
        
        int i, j;
        int[] count = new int[10000];
        
        Arrays.fill( count, 0 );

        for( i = 0; i < lista.length; i++ ) {
            count[lista[i]]++;
        }

        for( i = 0, j = 0; i < count.length; i++ ) {
            for( ; count[i] > 0; ( count[i] )-- ) {         
                lista[j++] = i;
            }
        }
        
        return lista;
        
    }
    
    public static int[] radixSort( int[] lista ){
     
        int maximoDeCaracteres = 4;
        int[] listaAux = new int[lista.length];
        int[] listaAuxOriginal = listaAux;

        int rshift = 0;
        
        for( int mask = ~( -1 << maximoDeCaracteres ); mask != 0; mask <<= maximoDeCaracteres, rshift += maximoDeCaracteres ){

            int[] cntarray = new int[1 << maximoDeCaracteres];

            for( int p = 0; p < lista.length; ++p ){
                int key = ( lista[p] & mask ) >> rshift;
                ++cntarray[key];
            }

            for( int i = 1; i < cntarray.length; ++i ){
                cntarray[i] += cntarray[i-1];
            }

            for( int p = lista.length-1; p >= 0; --p ){
                int key = ( lista[p] & mask ) >> rshift;
                --cntarray[key];
                listaAux[cntarray[key]] = lista[p];
            }

            int[] temp = listaAux; 
            listaAux = lista; 
            lista = temp;
            
        }
        
        if( lista == listaAuxOriginal ){
            System.arraycopy( lista, 0, listaAux, 0, lista.length );
        }

        return listaAux;
        
    }
  
	//HeapSort - peguei na internet
  
	public void heapSort(long a[]){
		int N = a.length;
		for (int k = N/2; k > 0; k--) {
		    downheap(a, k, N);
		}
		do {
	            int T = a[0];
	            a[0] = a[N - 1];
	            a[N - 1] = T;
		    N = N - 1;
			downheap(a, 1, N);
			DesenhaTela.update(a,N); 
		} while (N > 1);
    }

		public void downheap(long a[], int k, int N){
			long T = a[k - 1];
			while (k <= N/2) {
	            int j = k + k;
	            if ((j < N) && (a[j - 1] < a[j])) {
		        j++;
				}
				if (T >= a[j - 1]) {
				break;
				} 
				else {
	                a[k - 1] = a[j - 1];
	                k = j;
				}
			}
	        a[k - 1] = T;
		}	

  
    /*public static int[] heapSort( int[] lista ){
        
        for( int i = lista.length/2 - 1; i >= 0; i-- ){
            
            int w = 2 * i + 1;
            
            while( w < lista.length ){
                
                if( w + 1 < lista.length ){                    
                    if( lista[w + 1] > lista[w] ){
                        w++;
                    }
                }
                
                if( a[v] < a[w] ) 
                    int t=a[i];
                    a[i]=a[j];
                    a[j]=t;
                    v=w;        // continue
                    w=2*v+1;
                }
                
                
                
            }
            
        }

        
        return lista;
        
    }*/

    public static String imprimirLista( int lista[] ) {
        
        String strLista = "";
        
        for( int i = 0; i < lista.length; i++ ){
            strLista += lista[i] + " ";
        }
        
        return strLista;
        
    }
    
    public static void gerarArquivo( String conteudo ) {
        try {
            FileWriter arquivo = new FileWriter( "saida.txt" );
            char[] carac = conteudo.toCharArray();
            for( int i = 0; i < carac.length; i++ ){
                arquivo.write( carac[i] );
            }
            arquivo.close();
        } catch ( Exception e ) {
            e.getMessage();
        }
    }

}
