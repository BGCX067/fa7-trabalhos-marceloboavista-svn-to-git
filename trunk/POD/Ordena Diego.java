import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

/**
 * @author Diego Cardoso
 * 
 */
public class Ordena {

	public static void main(String args[]) {
		int a[] = new int[Integer.parseInt(args[0])];
		int tam = Integer.parseInt(args[0]);
		int tipo = Integer.parseInt(args[1]);
		String tipoStr = tipoLista(tipo);
		geraLista(a, Integer.parseInt(args[1]));
		
		int[] b = (int[]) a.clone();
		int[] c = (int[]) a.clone();
		int[] d = (int[]) a.clone();
		int[] e = (int[]) a.clone();

		/*
		 * Só imprime se a lista conter menos que 100 elementos
		 */
		if (tam <= 100) {
			criaArquivo(a, tipo);
		}

		System.out.println("Lista original:");
		imprimeLista(b);
		System.out.println("***********************************");
		ordenaLista(b, 1, tam, tipoStr);// 1 = inserção
		System.out.println("***********************************");
		ordenaLista(b, 2, tam, tipoStr);// 2 = mergesort
		System.out.println("***********************************");
		ordenaLista(b, 3, tam, tipoStr);// 3 = bolha com flag
		System.out.println("***********************************");
		ordenaLista(b, 4, tam, tipoStr);// 4 = Seleção
		System.out.println("***********************************");
		ordenaLista(b, 5, tam, tipoStr);// 5 = ShellSort
	}

	/**
	 * O método ordenaLista ordena a lista usando os métodos inserção,
	 * mergesort, bolha com flag, seleção e shellsort
	 * 
	 * @param v
	 * @param metodo
	 * @param tamanhoLista
	 * @param tipo
	 */
	public static void ordenaLista(int v[], int metodo, int tamanhoLista,
			String tipo) {
		long inicio = System.currentTimeMillis();
		if (metodo == 1) {
			System.out.println("Counting sort (" + tamanhoLista
					+ " elementos - " + tipo + ") ");
			countingSort(v, true);
			System.out.println("Tempo: "
					+ (System.currentTimeMillis() - inicio) + " ms");
		} else if (metodo == 2) {
			System.out.println("Bucket sort (" + tamanhoLista + " elementos - "
					+ tipo + ") ");
			//bucketSort(v, true);
			System.out.println("Tempo: "
					+ (System.currentTimeMillis() - inicio) + " ms");
		} else if (metodo == 3) {
			System.out.println("Heapsort (" + tamanhoLista + " elementos - "
					+ tipo + ") ");
			//heapSort(v);
			System.out.println("Tempo: "
					+ (System.currentTimeMillis() - inicio) + " ms");
		} else if (metodo == 4) {
			System.out.println("Radixsort (" + tamanhoLista + " elementos - "
					+ tipo + ") ");
			radixSort(v, true);
			System.out.println("Tempo: "
					+ (System.currentTimeMillis() - inicio) + " ms");
		}
		imprimeLista(v);
	}

	/**
	 * Cria um arquivo contendo a lista ordenada no diretório raiz com o nome de
	 * saida.txt
	 * 
	 * @param v
	 * @param tipoLista
	 */
	public static void criaArquivo(int v[], int tipoLista) {
		try {
			 File file = new File("/home/diegocmsantos/saida.txt");
			//File file = new File("c:\\saida.txt");
			file.createNewFile();
			FileWriter writer = new FileWriter(file);
			PrintWriter saida = new PrintWriter(writer);

			StringBuffer buffer = imprimeListaNoArquivo(v);

			/*
			 * Counting sort
			 */
			saida.println("Counting sort");
			saida.println("Antes: " + buffer);
			countingSort(v, false);
			saida.println("Depois: " + imprimeListaNoArquivo(v));
			saida.println("***************************************");
			saida.println();

			/*
			 * Bucket sort
			 */
			saida.println("Bucket sort");
			saida.println("Antes: " + buffer);
			//bucketSort(v, false);
			saida.println("Depois: " + imprimeListaNoArquivo(v));
			saida.println("***************************************");
			saida.println();

			/*
			 * Radix sort
			 */
			saida.println("Radix sort");
			saida.println("Antes: " + buffer);
			radixSort(v, true);
			saida.println("Depois: " + imprimeListaNoArquivo(v));
			saida.println("***************************************");
			saida.println();

			saida.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Define o tipo de lista a ser formada, 1 = Aleatória 2 = Decrescente 3 =
	 * Crescente
	 * 
	 * @param tipo
	 * @return
	 */
	public static String tipoLista(int tipo) {
		String tipoStr;
		if (tipo == 1) {
			tipoStr = "Aleatória";
		} else if (tipo == 2) {
			tipoStr = "Decrescente";
		} else {
			tipoStr = "Crescente";
		}
		return tipoStr;
	}

	/**
	 * Método para gerar lista
	 * 
	 * @param v
	 * @param tipoLista
	 */
	public static void geraLista(int v[], int tipoLista) {
		if (tipoLista == 1) {
			Random r = new Random();
			for (int i = 0; i < v.length; i++) {
				v[i] = r.nextInt(10 * v.length);
			}
		} else if (tipoLista == 2) {
			int tam = v.length - 1;
			for (int i = 0; i < v.length; i++) {
				v[tam] = i;
				tam--;
			}
		} else {
			for (int i = 0; i < v.length; i++) {
				v[i] = i;
			}
		}
	}

	/**
	 * Método para imprimir as listas
	 * 
	 * @param v
	 */
	public static void imprimeLista(int v[]) {
		for (int i = 0; i < v.length; i++) {
			System.out.print(v[i] + " ");
		}
		System.out.println();
	}

	/**
	 * Prepara um objeto StringBuffer para ser escrito no arquivo saida.txt
	 * 
	 * @param v
	 * @return
	 */
	public static StringBuffer imprimeListaNoArquivo(int v[]) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < v.length; i++) {
			buffer.append(v[i] + " ");
		}
		return buffer;
	}

	/**
	 * Método Counting Sort
	 * 
	 * 
	 * @param vetor
	 */
	public static void countingSort(int vetor[], boolean arquivo) {
		int countMov = 0;
		
		if (vetor.length == 0) {
			return;
		}
		int max = vetor[0];
		int min = vetor[0];

		for (int i = 1; i < vetor.length; i++) {
			if (vetor[i] > max)
				max = vetor[i];
			else if (vetor[i] < min)
				min = vetor[i];
			countMov++;
		}
		int numValues = max - min + 1;
		int[] counts = new int[numValues];
		for (int i = 0; i < vetor.length; i++) {
			counts[vetor[i] - min]++;
		}

		int outPos = 0;
		for (int i = 0; i < numValues; i++) {
			for (int j = 0; j < counts[i]; j++) {
				vetor[outPos] = i + min;
				outPos++;
			}
		}
		if (arquivo) {
			System.out.println("Movimentações: shell " + countMov);
		}
	}

	/**
	 * Método Bucket Sort
	 * 
	 * 
	 * @param vetor
	 * @return 
	 */
	public static int[] bucketSort(int vetor[], boolean arquivo) {
		int countMov = 0;
		
		int[] vetor2 = new int[vetor.length];

		for (int i = 0; i < vetor2.length; ++i) {
			++vetor2[vetor[i]];
		}
		for (int i = 0, j = 0; j < vetor.length; ++j) {
			for (int k = vetor2[j]; k > 0; --k) {
				vetor[i++] = j;
				countMov++;
			}
		}
		if (arquivo) {
			System.out.println("Movimentações: shell " + countMov);
		}
		
		return vetor;
	}

	/**
	 * Método seleção
	 * 
	 * @param vetor
	 */
	public static void radixSort(int[] vetor, boolean arquivo){
		
		int countMov = 0;
		
        if(vetor.length == 0)
            return;
        int[][] novoVetor = new int[vetor.length][2];
        int[] q = new int[0x100];
        int i,j,k,l,f = 0;
        for(k=0;k<4;k++){
            for(i=0;i<(novoVetor.length-1);i++)
                novoVetor[i][1] = i+1;
            novoVetor[i][1] = -1;
            for(i=0;i<q.length;i++)
                q[i] = -1;
            for(f=i=0;i<vetor.length;i++){
                j = ((0xFF<<(k<<3))&vetor[i])>>(k<<3);
                if(q[j] == -1)
                    l = q[j] = f;
                else{
                    l = q[j];
                    while(novoVetor[l][1] != -1)
                        l = novoVetor[l][1];
                    novoVetor[l][1] = f;
                    l = novoVetor[l][1];
                }
                f = novoVetor[f][1];
                novoVetor[l][0] = vetor[i];
                novoVetor[l][1] = -1;
                countMov++;
            }
            for(l=q[i=j=0];i<0x100;i++)
                for(l=q[i];l!=-1;l=novoVetor[l][1])
                        vetor[j++] = novoVetor[l][0];
        }
        if (arquivo) {
			System.out.println("Movimentações: shell " + countMov);
		}
    }
}
